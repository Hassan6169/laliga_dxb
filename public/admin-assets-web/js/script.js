// JavaScript Document
$(function(){

    //*****************************
    // Reset Href
    //*****************************
    $('[href="#"]').attr("href","javascript:;");

    //*****************************
    // Smooth Scroll
    //*****************************
    function goToScroll(e){
        $('html, body').animate({
            scrollTop: $("."+e).offset().top
        }, 1000);
    }

    //*****************************
    // Lazy Load
    //*****************************
    $(window).scroll(function(){
        lazzyload();
    });
    
    //*****************************
    // Mobile Navigation
    //*****************************
    $('.mobile-nav-btn').click(function() {
        $('.mobile-nav-btn, .primary-navigation, .mobile-nav').toggleClass('nav-active');
        //$('.overlay-bg').fadeIn();
    });
    
    // $('.overlay-bg').click(function(){
    //     $('.mobile-nav-btn, .mobile-nav, .app-container').removeClass('active');
    //     $(this).fadeOut();
    // });


    //*****************************
    // Close Funtion
    //*****************************
    $('.close-this').click(function(){
        $(this).parents().hide();
    });


    //*****************************
    // Scroll Funtion
    //*****************************

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

        if (scroll >= 130) {
            $('.mobile-contact').addClass('active');
        } else {
            $('.mobile-contact').removeClass('active');
        }

        if (scroll >= 100) {
            $('header.ph').addClass('fixed');
        } else {
            $('header.ph').removeClass('fixed');
        }
    });


    //*****************************
    // Slick Slider
    //*****************************
	var respsliders = {
      1: {slider : '.slider1'},
      2: {slider : '.slider2'}
    };

    $.each(respsliders, function() {

        $(this.slider).slick({

            arrows: true,
            dots: false,
            infinite: true,
            autoplay:true,
            speed: 300,
            responsive: [
                {
                    breakpoint: 2000,
                    settings: "unslick"
                },
                {
                    breakpoint: 768,
                    settings: {
                        unslick: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
    

    //*****************************
    // Accordian
    //*****************************
    $('.accordian').click(function() {
        $(this).parent().children('.accordian-inner').slideToggle();
    });

    //*****************************
    // Fancybox
    //*****************************
    $('[data-fancybox="gallery"]').fancybox({
        // Options will go here
    });

    //*****************************
    // Form Animation
    //*****************************
    $('.form-field').on('focus blur',function(i){
        $(this).parents('.control-group').toggleClass('focused','focus'===i.type||this.value.length>0)
    }).trigger('blur');

    //*****************************
    // Tabbing
    //*****************************
    $('[data-targetit]').on('click',function () {
        $(this).siblings().removeClass('current');
        $(this).addClass('current');
        var target = $(this).data('targetit');
        $('.'+target).siblings('[class^="tabs"]').removeClass('show');
        $('.'+target).addClass('show');
        //$('.slick-slider').slick('setPosition', 0);
    });

    //*****************************
    // Modal
    //*****************************
    $('[data-targetit]').on('click',function () {
        $('body').addClass('modal-open');
        var target = $(this).data('targetit');
        $('.'+target).addClass('show');
    });

    $('.modal-close').click(function() {
        $('.x-modal').removeClass('show');
        $('body').removeClass('modal-open');
    });

    //*****************************
    // Copyright Year
    //*****************************
    now=new Date;thecopyrightYear=now.getYear();if(thecopyrightYear<1900)thecopyrightYear=thecopyrightYear+1900;$("#cur-year").html(thecopyrightYear);

    //*****************************
    // Set Map
    //*****************************
    $("address.setmap").each(function(){
        var embed ="<iframe frameborder='0' scrolling='no' marginheight='0' height='100%' width='100%' marginwidth='0' src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( $(this).text() ) +"&amp;output=embed'></iframe>";
        $(this).html(embed);
    });

});