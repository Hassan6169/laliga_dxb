<?php



use Illuminate\Support\Facades\Route;



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/



Auth::routes();



//Route::get('upass', function(){

//    $user = \App\User::where('id', 1)->first();

//    $user->password = bcrypt('Laliga148#');

//    $user->save();

//    $user = \App\User::where('id', 2)->first();

//    $user->password = bcrypt('Parent123#');

//    $user->save();

//});

Route::get('/linking', function () {
  Artisan::call('storage:link');
});

Route::get('/payment', function()
{
    include public_path().'/payment.php';
});


Route::get('/redirectToUserHome', 'Auth\LoginController@redirectToUserHome');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/', 'Auth\LoginController@showLoginForm')->name('home');
Route::post('/sign-up/store', 'SignUpController@store')->name('sign.up.store');
Route::get('/terms-condition', 'HomeController@laligaTerms')->name('terms.conditions');


/* About Pages */

Route::any('/returnpage2', 'HomeController@returnpage');

Route::get('/about', 'HomeController@about')->name('pages.about');

Route::get('/coaches', 'HomeController@coaches')->name('pages.coaches');

Route::get('/photos', 'HomeController@photos')->name('pages.photos');

Route::get('/videos', 'HomeController@videos')->name('pages.videos');



/* Programs Pages */

Route::get('/methodology', 'HomeController@methodologyProgram')->name('pages.methodology_program');

Route::get('/development-program', 'HomeController@developmentProgram')->name('pages.development_program');

Route::get('/advanced-development-program', 'HomeController@advancedDevelopmentProgram')->name('pages.advanced_development_program');

Route::get('/laliga-hpc-program', 'HomeController@laligaHpcProgram')->name('pages.laliga_hpc_program');

Route::get('/summer-program', 'HomeController@summerProgram')->name('pages.summer_program');



/* Scouting Pages */

Route::get('/laliga-trials', 'HomeController@laligaTrials')->name('pages.laliga_trials');

Route::get('/contract-awards', 'HomeController@contractAwards')->name('pages.contract_awards');

Route::get('/soccer-scholarship', 'HomeController@soccerScholarship')->name('pages.soccer_scholarship');



/* Schedule Pages */

Route::get('/development-schedule', 'HomeController@developmentSchedule')->name('pages.development_schedule');

Route::get('/advanced-schedule', 'HomeController@advancedSchedule')->name('pages.advanced_schedule');

Route::get('/training-locations', 'HomeController@trainingLocations')->name('pages.training_locations');



/* Extra Pages */

Route::get('/fees', 'HomeController@fees')->name('pages.fees');

Route::get('/faq', 'HomeController@faq')->name('pages.faq');

Route::get('/privacy-policy', 'HomeController@policy')->name('pages.privacy_policy');

Route::get('/terms-conditions', 'HomeController@terms')->name('pages.terms_conditions');

Route::get('/register-terms-conditions', 'HomeController@registerTermsConditions')->name('pages.register_terms_condtions');




/* Admin Area */

Route::name('admin.')->prefix('admin/')->middleware(['admin'])->group(function(){



    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::get('dashboard', 'HomeController@adminDashboard')->name('_dashboard');

     Route::group(['prefix' => 'custom-invoices'], function () {

     Route::get('/player/{player}/term/{term}', 'InvoiceController@customInvoice')->name('player.term.custom.invoice');

    Route::post('/player/{player}/term/{term}/store', 'InvoiceController@customInvoiceStore')->name('player.term.custom.invoice.store');

     Route::get('/{team}/edit', 'TeamController@edit')->name('teams.edit');

     Route::put('/{team}/update','TeamController@update')->name('teams.update');

     Route::delete('/{team}', 'TeamController@destroy')->name('teams.destroy');

    });



    Route::group(['prefix' => 'seasons'],function () {

    Route::get('/', 'Admin\SeasonController@index')->name('seasons.index');

    Route::get('/create', 'Admin\SeasonController@create')->name('seasons.create');

    Route::post('/', 'Admin\SeasonController@store')->name('seasons.store');

    Route::get('/{season}/edit', 'Admin\SeasonController@edit')->name('seasons.edit');

    Route::put('/{season}/update', 'Admin\SeasonController@update')->name('seasons.update');

    Route::delete('/{season}', 'Admin\SeasonController@destroy')->name('seasons.destroy');

    // Terms



    Route::get('/{season}/Terms', 'Admin\TermsController@index')->name('season.terms.index');

    Route::get('/{season}/Terms/create', 'Admin\TermsController@create')->name('season.terms.create');

    Route::post('/Terms', 'Admin\TermsController@store')->name('season.terms.store');

    Route::get('/{season}/Terms/{term}/edit', 'Admin\TermsController@edit')->name('season.terms.edit');

    Route::put('/Terms/{term}/update', 'Admin\TermsController@update')->name('season.terms.update');

    Route::delete('/{season}/Term/{term}/delete', 'Admin\TermsController@destroy')->name('season.terms.destroy');
  


        Route::get('/', 'Admin\SeasonController@index')->name('seasons.index');

        Route::get('/create', 'Admin\SeasonController@create')->name('seasons.create');

        Route::post('/', 'Admin\SeasonController@store')->name('seasons.store');

        Route::get('/{season}/edit', 'Admin\SeasonController@edit')->name('seasons.edit');

        Route::put('/{season}/update', 'Admin\SeasonController@update')->name('seasons.update');

        Route::delete('/{season}', 'Admin\SeasonController@destroy')->name('seasons.destroy');

        // Terms



        Route::get('/{season}/Terms', 'Admin\TermsController@index')->name('season.terms.index');

        Route::get('/{season}/Terms/create', 'Admin\TermsController@create')->name('season.terms.create');

        Route::post('/terms', 'Admin\TermsController@store')->name('season.terms.store');

        Route::get('/{season}/Terms/{term}/edit', 'Admin\TermsController@edit')->name('season.terms.edit');

        Route::put('/terms/{term}/update', 'Admin\TermsController@update')->name('season.terms.update');

        Route::delete('/{season}/Terms/{term}', 'Admin\TermsController@destroy')->name('season.terms.destroy');
        
        Route::get('/{season}/Terms/{term}/break', 'Admin\TermsController@createTermbreak')->name('season.terms.break');
       
        Route::post('/{season}/Terms/{term}/break/store', 'Admin\TermsController@storeTermbreak')->name('season.term.break.store');

        Route::delete('/{season}/Terms/{term}/break/{break}', 'Admin\TermsController@destroyTermBreak')->name('season.terms.break.destroy');
       // Training Sessions
       

        Route::get('/training-session', 'Admin\TrainingSessionController@index')->name('training.sessions.index');

        Route::get('/term/{term}/training-session/create', 'Admin\TrainingSessionController@create')->name('training.sessions.create');

        Route::post('/term/{term}/training-session/store', 'Admin\TrainingSessionController@store')->name('season.term.training.sessions.store');

        

        Route::get('term/{term}/training-sessions/edit', 'Admin\TrainingSessionController@edit')->name('training.sessions.edit');

         

         Route::get('/training-session/{session}/edit/details', 'Admin\TrainingSessionController@editDetails')->name('training.sessions.edit.details');



        Route::put('training-session/{session}/update', 'Admin\TrainingSessionController@update')->name('training.sessions.update');

        Route::delete('term/{term}/training-sessions/edit/{session}','Admin\TrainingSessionController@destroy')->name('season.term.training.sessions.destroy');

         Route::get('/training-session/search', 'Admin\TrainingSessionController@search')->name('training.sessions.serach');
          

    });



    Route::group(['prefix' => 'emirates'], function () {

        Route::get('/', 'Admin\EmirateController@index')->name('emirates.index');

        Route::get('/create', 'Admin\EmirateController@create')->name('emirates.create');

        Route::post('/', 'Admin\EmirateController@store')->name('emirates.store');

        Route::get('/{emirate}/edit', 'Admin\EmirateController@edit')->name('emirates.edit');

        Route::put('/{emirate}/update', 'Admin\EmirateController@update')->name('emirates.update');

        Route::delete('/{emirate}', 'Admin\EmirateController@destroy')->name('emirates.destroy');

    });

       Route::group(['prefix' => 'user-accounts'], function () {

        Route::get('/', 'UserController@accounts')->name('accounts.index');

        Route::get('/create', 'UserController@createAccount')->name('accounts.create');

        Route::post('/', 'UserController@storeAccount')->name('accounts.store');

        Route::get('/{user}/edit', 'UserController@editAccount')->name('accounts.edit');

        Route::put('/{user}/update', 'UserController@updateAccount')->name('accounts.update');

        Route::delete('/{user}', 'UserController@destroy')->name('accounts.destroy');

    });



    Route::group(['prefix' => 'age-categories'], function () {

        Route::get('/', 'Admin\AgeCategoryController@index')->name('age.categories.index');

        Route::get('/create', 'Admin\AgeCategoryController@create')->name('age.categories.create');

        Route::post('/', 'Admin\AgeCategoryController@store')->name('age.categories.store');

        Route::get('/{category}/edit', 'Admin\AgeCategoryController@edit')->name('age.categories.edit');

        Route::put('/{category}/update', 'Admin\AgeCategoryController@update')->name('age.categories.update');

        Route::delete('/{category}', 'Admin\AgeCategoryController@destroy')->name('age.categories.destroy');

    });



    Route::group(['prefix' => 'locations'], function () {

        Route::get('/', 'Admin\LocationController@index')->name('locations.index');

        Route::get('/create', 'Admin\LocationController@create')->name('locations.create');

        Route::post('/', 'Admin\LocationController@store')->name('locations.store');

        Route::get('/{location}/edit', 'Admin\LocationController@edit')->name('locations.edit');

        Route::put('/{location}/update','Admin\LocationController@update')->name('locations.update');

        Route::delete('/{location}', 'Admin\LocationController@destroy')->name('locations.destroy');

    });



     Route::group(['prefix' => 'discounts'], function () {

        Route::get('/', 'DiscountController@index')->name('discounts.index');

        Route::get('/create', 'DiscountController@create')->name('discounts.create');

        Route::post('/', 'DiscountController@store')->name('discounts.store');

        Route::get('/{discount}/edit', 'DiscountController@edit')->name('discounts.edit');

        Route::put('/{discount}/update','DiscountController@update')->name('discounts.update');

        Route::delete('/{discount}','DiscountController@destroy')->name('discounts.destroy');

        Route::get('/player/{player}/term/{term}', 'DiscountController@playerDiscount')->name('players.discount.index');

        Route::post('/player/{player}/term/{term}', 'DiscountController@playerDiscountupdate')->name('players.discount.update');

    });



      Route::group(['prefix' => 'players'], function () {

      Route::get('/', 'PlayersController@index')->name('players.index');

      Route::get('/{player}/terms', 'PlayersController@playerTerm')->name('players.term');

      Route::get('/search', 'PlayersController@playersSearch')->name('players.serach');

      Route::get('/{player}/edit', 'PlayersController@edit')->name('players.edit');
      Route::get('/create', 'PlayersController@create')->name('players.create');

      Route::put('/{player}/update','PlayersController@updatePlayerAdmin')->name('players.update');

      Route::delete('/{player}', 'PlayersController@destroy')->name('players.destroy');

      Route::get('/{player}/term/{term}/invoices', 'InvoiceController@playerTermInvoice')->name('player.term.invoices');

      //player terms operation in admin
      
       Route::get('{player}/term/create', 'PlayersController@playerTermCreate')->name('players.term.create');
       
       Route::post('{player}/term/store', 'PlayersController@storePlayerTerm')->name('player.term.store');

      Route::get('/{playerId}/playerTerm/{id}/edit', 'PlayersController@playerTermEdit')->name('players.term.edit');

      Route::put('/{player}/playerTerm/{id}/update','PlayersController@playerTermUpdate')->name('players.term.update');

       Route::get('/{player}/term/{term}/id/{id}', 'DashboardController@destroyPlayerTerm')->name('players.term.destroy');
      
      Route::get('/{player}/term/{term}/emirate/{emirate}/create', 'DashboardController@createTrainingSession')->name('player.training.session.create');

      Route::get('/{player}/term/{term}/emirate/{emirate}/edit', 'DashboardController@editTrainingSession')->name('player.training.session.edit');

      Route::post('/update/player/{player}/term/{term}/emirate/{emirate}', 'DashboardController@updatePlayerTrainingSession')->name('player.training.session.update');
       Route::post('/getCategory', 'PlayersController@getPlayerCategory')->name('get.category');

    });



     Route::group(['prefix' => 'teams'], function () {

     Route::get('/', 'TeamController@index')->name('teams.index');

     Route::get('/create', 'TeamController@create')->name('teams.create');

     Route::post('/store', 'TeamController@store')->name('teams.store');

     Route::get('/{team}/edit', 'TeamController@edit')->name('teams.edit');

     Route::put('/{team}/update','TeamController@update')->name('teams.update');

     Route::delete('/{team}', 'TeamController@destroy')->name('teams.destroy');

    });

    
     Route::group(['prefix' => 'parents'], function () {
      Route::get('/', 'UserController@index')->name('parents.index');
      Route::get('/{user}/edit', 'UserController@edit')->name('parents.edit');
      Route::put('/{user}/update','UserController@update')->name('parents.update');
      Route::delete('/{user}', 'UserController@destroy')->name('parents.destroy');
       Route::get('/searchUser', 'UserController@searchUser')->name('parents.search');
    });

    Route::group(['prefix' => 'invoice'], function () {

    Route::get('/', 'InvoiceController@index')->name('invoices.index');
    Route::get('/player/{player}', 'InvoiceController@indexPlayerInvoice')->name('players.invoices.index');
    Route::delete('/{invoice}', 'InvoiceController@destroy')->name('players.invoices.destroy');
   
    });



      Route::group(['prefix' => 'sign_up'], function () {

      Route::get('/{id}/index', 'SignUpController@index')->name('signups.index');

      Route::get('/{id}/search', 'SignUpController@search')->name('signups.search');

      Route::get('/{signUp}/edit', 'SignUpController@edit')->name('signups.edit');

      Route::post('/{id}/store', 'SignUpController@store')->name('signups.store');

      Route::get('/{signUp}/register', 'SignUpController@register')->name('signups.register');
      Route::post('/{signUp}/user/register', 'UserController@store')->name('signups.user.register');

      Route::put('/{signUp}/update','SignUpController@update')->name('signups.update');

      Route::put('/{signUp}/updates','SignUpController@updateSignup')->name('signups.updates');
 
      Route::delete('/{id}/index/{signUp}', 'SignUpController@destroy')->name('signups.destroy');

    });

      Route::group(['prefix' => 'dailyreport'], function () {
      Route::get('/', 'DailyReportController@index')->name('daily.report.index');
      Route::get('/search', 'DailyReportController@reportSearch')->name('daily.report.search');
     });

    Route::group(['prefix' => 'audit'], function () {
      Route::get('/', 'AuditController@index')->name('audit.index');
     });


       Route::group(['prefix' => 'attendance'], function () {

      Route::get('/', 'AttendanceController@index')->name('attendance.index');

      Route::get('/search', 'AttendanceController@search')->name('attendance.search');

      Route::post('/store', 'AttendanceController@store')->name('attendance.store');

    });



      Route::group(['prefix' => 'news'], function () {

      Route::get('/', 'NewsController@index')->name('news.index');

      Route::get('/create', 'NewsController@create')->name('news.create');

      Route::post('/store', 'NewsController@store')->name('news.store');

      Route::get('/{news}/edit', 'NewsController@edit')->name('news.edit');

      Route::put('/{news}/update','NewsController@update')->name('news.update');

      Route::delete('/{news}', 'NewsController@destroy')->name('news.destroy');

    });



     Route::group(['prefix' => 'gallery-categories'], function () {

      Route::get('/', 'GalleryController@indexGalleryCategories')->name('gallery.categories.index');

      Route::get('/create', 'GalleryController@createGalleryCategories')->name('gallery.categories.create');

      Route::post('/store', 'GalleryController@storeGalleryCategories')->name('gallery.categories.store');

      Route::get('/{gallery_category}/edit', 'GalleryController@editGalleryCategories')->name('gallery.categories.edit');

      Route::put('/{gallery_category}/update','GalleryController@updateGalleryCategories')->name('gallery.categories.update');

      Route::delete('/{gallery_category}', 'GalleryController@destroyGalleryCategories')->name('gallery.categories.destroy');

    });



     Route::group(['prefix' => 'galleries'], function () {

      Route::get('/', 'GalleryController@index')->name('galleries.index');

      Route::get('/create', 'GalleryController@create')->name('galleries.create');

      Route::post('/store', 'GalleryController@store')->name('galleries.store');

      Route::get('/{gallery}/edit', 'GalleryController@edit')->name('galleries.edit');

      Route::put('/{gallery}/update','GalleryController@update')->name('galleries.update');

      Route::delete('/{gallery}', 'GalleryController@destroy')->name('galleries.destroy');

    });



    Route::group(['prefix' => 'evaluations'], function () {

      Route::get('{player}/index', 'EvaluationController@index')->name('player.evaluations.index');

      Route::get('/{player}/create', 'EvaluationController@create')->name('player.evaluations.create');

      Route::post('/{player}/{evaluation}/store', 'EvaluationController@store')->name('player.evaluations.store');

      Route::get('/{player}/evaluations/{evaluation}/edit', 'EvaluationController@edit')->name('player.evaluations.edit');

        Route::get('/{player}/evaluations/{evaluation}/send_player_evaluation', 'EvaluationController@sendPlayerEvaluation')->name('player.evaluations.send');

        Route::get('players/{player}/evaluations/{evaluation}/generate_evaluation_pdf_report', 'EvaluationController@generateEvaluationPdfReport')->name('player.generate_evaluation_pdf_report');

       Route::get('players/{player}/evaluations/{evaluation}/get_evaluation_report', 'PlayerController@getEvaluationReport')->name('player.evaluations.report');

    });





    Route::group(['prefix' => 'fees'], function () {

      Route::get('/', 'FeeController@index')->name('fee.sessions.index');

      Route::get('/{type}/create', 'FeeController@create')->name('fee.sessions.create');

      Route::post('/store', 'FeeController@store')->name('fee.sessions.store');
       Route::get('/{fee}/{term}/edit', 'FeeController@edit')->name('fee.sessions.edit');
      Route::put('/{fee}/update', 'FeeController@update')->name('fee.sessions.update');
       Route::delete('/{fee}','FeeController@destroy')->name('fee.destroy');


    });



    Route::group(['prefix' => 'scholarships'], function () {

    Route::get('/index','ScholarshipsController@index')->name('scholarships.index');

    });



    Route::group(['prefix' => 'invoice'], function () {

    Route::get('/', 'InvoiceController@index')->name('invoices.index');

    Route::get('/{invoice}/writeOff', 'InvoiceController@writeOff')->name('invoices.write.off.index');

    Route::put('writeOff/{invoice}/update', 'InvoiceController@writeOffUpdate')->name('invoices.write.off.update');

   Route::get('player/{invoice}/preview','InvoiceController@generatedReport')->name('player.invoices.show');

    Route::get('/{invoice}/partialRefund', 'InvoiceController@partialRefund')->name('invoices.partial.refund.index');

    Route::put('/{invoice}/partialRefund/update', 'InvoiceController@partialRefundUpdate')->name('invoices.partial.refund.update');

    Route::get('player/{invoiceNo}/report','InvoiceController@playerInvoiceReport')->name('player.invoices.report');

    Route::get('/payment/{invoice}/edit','InvoiceController@editPaymentMethod')->name('invoices.payments.method.edit');

    Route::put('/payment/{invoice}/update','InvoiceController@updatePaymentMethod')->name('invoices.payment.method.update');

    Route::get('/payment/{invoice}/installments','InvoiceController@paymentInstallment')->name('invoices.payments.installments');

    Route::put('/installments/{invoice}/update','InvoiceController@updateInstallments')->name('invoices.installments.update');

      Route::get('/search', 'InvoiceController@invoiceSearch')->name('invoice.serach');

    });



    Route::get('/{user}/account', 'InvoiceController@switchToUser')->name('account.switch');

   

   Route::group(['prefix' => 'wallets'], function () {

    Route::get('/{parent}', 'Admin\WalletController@index')->name('wallets.index');

    Route::get('/{parent}/create', 'Admin\WalletController@create')->name('wallets.create');

    Route::post('/store','Admin\WalletController@store')->name('wallets.store');

    Route::get('{wallet}/user/{user}/edit', 'Admin\WalletController@edit')->name('wallets.edit');

    Route::put('/{wallet}/update','Admin\WalletController@update')->name('wallets.update');
    Route::delete('/{user}/{wallet}','Admin\WalletController@destroy')->name('wallets.destroy');
   });

});

Route::name('manager.')->prefix('manager/')->middleware(['manager'])->group(function(){



    Route::get('/', 'DashboardController@index')->name('dashboard');

    
    
    Route::group(['prefix' => 'seasons'],function () {

    Route::get('/', 'Admin\SeasonController@index')->name('seasons.index');

    Route::get('/create', 'Admin\SeasonController@create')->name('seasons.create');

    Route::post('/', 'Admin\SeasonController@store')->name('seasons.store');

    Route::get('/{season}/edit', 'Admin\SeasonController@edit')->name('seasons.edit');

    Route::put('/{season}/update', 'Admin\SeasonController@update')->name('seasons.update');

    Route::delete('/{season}', 'Admin\SeasonController@destroy')->name('seasons.destroy');

    // Terms



    Route::get('/{season}/Terms', 'Admin\TermsController@index')->name('season.terms.index');

    Route::get('/{season}/Terms/create', 'Admin\TermsController@create')->name('season.terms.create');

    Route::post('/Terms', 'Admin\TermsController@store')->name('season.terms.store');

    Route::get('/{season}/Terms/{term}/edit', 'Admin\TermsController@edit')->name('season.terms.edit');

    Route::put('/Terms/{term}/update', 'Admin\TermsController@update')->name('season.terms.update');

    Route::delete('/{season}/Term/{term}/delete', 'Admin\TermsController@destroy')->name('season.terms.destroy');



        Route::get('/', 'Admin\SeasonController@index')->name('seasons.index');

        Route::get('/create', 'Admin\SeasonController@create')->name('seasons.create');

        Route::post('/', 'Admin\SeasonController@store')->name('seasons.store');

        Route::get('/{season}/edit', 'Admin\SeasonController@edit')->name('seasons.edit');

        Route::put('/{season}/update', 'Admin\SeasonController@update')->name('seasons.update');

        Route::delete('/{season}', 'Admin\SeasonController@destroy')->name('seasons.destroy');

        // Terms



        Route::get('/{season}/Terms', 'Admin\TermsController@index')->name('season.terms.index');

        Route::get('/{season}/Terms/create', 'Admin\TermsController@create')->name('season.terms.create');

        Route::post('/terms', 'Admin\TermsController@store')->name('season.terms.store');

        Route::get('/{season}/Terms/{term}/edit', 'Admin\TermsController@edit')->name('season.terms.edit');

        Route::put('/terms/{term}/update', 'Admin\TermsController@update')->name('season.terms.update');

        Route::delete('/{season}/Terms/{term}', 'Admin\TermsController@destroy')->name('season.terms.destroy');
        
        Route::get('/{season}/Terms/{term}/break', 'Admin\TermsController@createTermbreak')->name('season.terms.break');
       
        Route::post('/{season}/Terms/{term}/break/store', 'Admin\TermsController@storeTermbreak')->name('season.term.break.store');

        Route::delete('/{season}/Term/{term}/break/{break}', 'Admin\TermsController@destroyTermBreak')->name('season.terms.break.destroy');
       // Training Sessions

        Route::get('/traningsession', 'Admin\TrainingSessionController@index')->name('traning.sessions.index');

        Route::get('/term/{term}/traningsession/create', 'Admin\TrainingSessionController@create')->name('traning.sessions.create');

        Route::post('/term/{term}/trainingSession/store', 'Admin\TrainingSessionController@store')->name('season.term.traning.sessions.store');

        

        Route::get('term/{term}/traningsessions/edit', 'Admin\TrainingSessionController@edit')->name('training.sessions.edit');

         

         Route::get('/traningsession/{session}/edit/details', 'Admin\TrainingSessionController@editDetails')->name('training.sessions.edit.details');



        Route::put('traningSession/{session}/update', 'Admin\TrainingSessionController@update')->name('traning.sessions.update');

        Route::delete('term/{term}/traningsessions/edit/{session}','Admin\TrainingSessionController@destroy')->name('season.term.traning.sessions.destroy');

    });



    Route::group(['prefix' => 'emirates'], function () {

        Route::get('/', 'Admin\EmirateController@index')->name('emirates.index');

        Route::get('/create', 'Admin\EmirateController@create')->name('emirates.create');

        Route::post('/', 'Admin\EmirateController@store')->name('emirates.store');

        Route::get('/{emirate}/edit', 'Admin\EmirateController@edit')->name('emirates.edit');

        Route::put('/{emirate}/update', 'Admin\EmirateController@update')->name('emirates.update');

        Route::delete('/{emirate}', 'Admin\EmirateController@destroy')->name('emirates.destroy');

    });

       Route::group(['prefix' => 'coaches'], function () {

        Route::get('/', 'UserController@indexCoach')->name('coaches.index');

        Route::get('/create', 'UserController@createCoach')->name('coaches.create');

        Route::post('/', 'UserController@storeCoach')->name('coaches.store');

        Route::get('/{user}/edit', 'UserController@editCoach')->name('coaches.edit');

        Route::put('/{user}/update', 'UserController@updateCoach')->name('coaches.update');

        Route::delete('/{user}', 'UserController@destroyCoach')->name('coaches.destroy');

    });



    Route::group(['prefix' => 'age-categories'], function () {

        Route::get('/', 'Admin\AgeCategoryController@index')->name('age.categories.index');

        Route::get('/create', 'Admin\AgeCategoryController@create')->name('age.categories.create');

        Route::post('/', 'Admin\AgeCategoryController@store')->name('age.categories.store');

        Route::get('/{category}/edit', 'Admin\AgeCategoryController@edit')->name('age.categories.edit');

        Route::put('/{category}/update', 'Admin\AgeCategoryController@update')->name('age.categories.update');

        Route::delete('/{category}', 'Admin\AgeCategoryController@destroy')->name('age.categories.destroy');

    });



    Route::group(['prefix' => 'locations'], function () {

        Route::get('/', 'Admin\LocationController@index')->name('locations.index');

        Route::get('/create', 'Admin\LocationController@create')->name('locations.create');

        Route::post('/', 'Admin\LocationController@store')->name('locations.store');

        Route::get('/{location}/edit', 'Admin\LocationController@edit')->name('locations.edit');

        Route::put('/{location}/update','Admin\LocationController@update')->name('locations.update');

        Route::delete('/{location}', 'Admin\LocationController@destroy')->name('locations.destroy');

    });



     Route::group(['prefix' => 'discounts'], function () {

        Route::get('/', 'DiscountController@index')->name('discounts.index');

        Route::get('/create', 'DiscountController@create')->name('discounts.create');

        Route::post('/', 'DiscountController@store')->name('discounts.store');

        Route::get('/{discount}/edit', 'DiscountController@edit')->name('discounts.edit');

        Route::put('/{discount}/update','DiscountController@update')->name('discounts.update');

        Route::delete('/{discount}','DiscountController@destroy')->name('discounts.destroy');

        Route::get('/player/{player}/term/{term}', 'DiscountController@playerDiscount')->name('players.discount.index');

        Route::post('/player/{player}/term/{term}', 'DiscountController@playerDiscountupdate')->name('players.discount.update');

    });



      Route::group(['prefix' => 'players'], function () {

      Route::get('/', 'PlayersController@index')->name('players.index');

      Route::get('/{player}/terms', 'PlayersController@playerTerm')->name('players.term');

      Route::get('/search', 'PlayersController@playersSearch')->name('players.serach');

      Route::get('/{player}/edit', 'PlayersController@edit')->name('players.edit');

      Route::put('/{player}/update','PlayersController@updatePlayerAdmin')->name('players.update');

      Route::delete('/{player}', 'PlayersController@destroy')->name('players.destroy');



      //player terms operation in manager
      
       Route::get('{player}/term/create', 'PlayersController@playerTermCreate')->name('players.term.create');
       
       Route::post('{player}/term/store', 'PlayersController@storePlayerTerm')->name('player.term.store');

      Route::get('/{playerId}/playerTerm/{id}/edit', 'PlayersController@playerTermEdit')->name('players.term.edit');

      Route::put('/{player}/playerTerm/{id}/update','PlayersController@playerTermUpdate')->name('players.term.update');

       Route::get('/{player}/terms/{term}/', 'DashboardController@destroyPlayerTerm')->name('players.term.destroy');

      Route::get('/{player}/term/{term}/emirate/{emirate}/create', 'DashboardController@createTrainingSession')->name('player.training.session.create');

       Route::get('/{player}/term/{term}/emirate/{emirate}/edit', 'DashboardController@editTrainingSession')->name('player.training.session.edit');
      
      Route::post('/update/player/{player}/term/{term}/emirate/{emirate}', 'DashboardController@updatePlayerTrainingSession')->name('player.training.session.update');
       Route::post('/getCategory', 'PlayersController@getPlayerCategory')->name('get.category');

    });



    
     Route::group(['prefix' => 'parents'], function () {
      Route::get('/', 'UserController@index')->name('parents.index');
      Route::get('/{user}/edit', 'UserController@edit')->name('parents.edit');
      Route::put('/{user}/update','UserController@update')->name('parents.update');
      Route::delete('/{user}', 'UserController@destroy')->name('parents.destroy');
       Route::get('/searchUser', 'UserController@searchUser')->name('parents.search');
    });

    Route::group(['prefix' => 'invoice'], function () {

    Route::get('/', 'InvoiceController@index')->name('invoices.index');
    Route::get('/player/{player}', 'InvoiceController@indexPlayerInvoice')->name('players.invoices.index');
    Route::delete('/{invoice}', 'InvoiceController@destroy')->name('players.invoices.destroy');
   
    });



  Route::group(['prefix' => 'coach_assignments'], function () {

      Route::get('/', 'UserController@coachAssignment')->name('coach.assignments.index');

      Route::get('/create', 'UserController@coachAssignmentCreate')->name('coach.assignments.create');

      Route::post('/post', 'UserController@coachAssignmentStore')->name('coach.assignments.store');

      Route::get('/user/{userId}/{id}/edit', 'UserController@coachAssignmentEdit')->name('coach.assignments.edit');

      Route::put('/{id}/update','UserController@coachAssignmentUpdate')->name('coach.assignments.update');

      Route::get('/user/{userId}/{id}/delete', 'UserController@coachAssignmentDelete')->name('coach.assignments.destroy');

    });



      Route::group(['prefix' => 'sign_up'], function () {

      Route::get('/{id}/index', 'SignUpController@index')->name('signups.index');

      Route::get('/{id}/search', 'SignUpController@search')->name('signups.search');

      Route::get('/{id}/create', 'SignUpController@create')->name('signups.create');

      Route::post('/{id}/store', 'SignUpController@store')->name('signups.store');

      Route::get('/{signUp}/edit', 'SignUpController@edit')->name('signups.edit');

      Route::put('/{signUp}/update','SignUpController@update')->name('signups.update');

      Route::delete('/{id}/index/{signUp}', 'SignUpController@destroy')->name('signups.destroy');

    });

      Route::group(['prefix' => 'dailyreport'], function () {
      Route::get('/', 'DailyReportController@index')->name('daily.report.index');
      Route::get('/search', 'DailyReportController@reportSearch')->name('daily.report.search');
     });

      Route::group(['prefix' => 'fees'], function () {

      Route::get('/', 'FeeController@index')->name('fee.sessions.index');

      Route::get('/{type}/create', 'FeeController@create')->name('fee.sessions.create');

      Route::post('/store', 'FeeController@store')->name('fee.sessions.store');
       Route::get('/{fee}/{term}/edit', 'FeeController@edit')->name('fee.sessions.edit');
      Route::put('/{fee}/update', 'FeeController@update')->name('fee.sessions.update');
       Route::delete('/{fee}','FeeController@destroy')->name('fee.destroy');


    });



    Route::group(['prefix' => 'invoice'], function () {

    Route::get('/', 'InvoiceController@index')->name('invoices.index');

    Route::get('/{invoice}/writeOff', 'InvoiceController@writeOff')->name('invoices.write.off.index');

    Route::put('writeOff/{invoice}/update', 'InvoiceController@writeOffUpdate')->name('invoices.write.off.update');

   Route::get('player/{invoice}/preview','InvoiceController@generatedReport')->name('player.invoices.show');

    Route::get('/{invoice}/partialRefund', 'InvoiceController@partialRefund')->name('invoices.partial.refund.index');

    Route::put('/{invoice}/partialRefund/update', 'InvoiceController@partialRefundUpdate')->name('invoices.partial.refund.update');

    Route::get('player/{invoiceNo}/report','InvoiceController@playerInvoiceReport')->name('player.invoices.report');

    Route::get('/payment/{invoice}/edit','InvoiceController@editPaymentMethod')->name('invoices.payments.method.edit');

    Route::put('/payment/{invoice}/update','InvoiceController@updatePaymentMethod')->name('invoices.payment.method.update');

    Route::get('/payment/{invoice}/installments','InvoiceController@paymentInstallment')->name('invoices.payments.installments');

    Route::put('/installments/{invoice}/update','InvoiceController@updateInstallments')->name('invoices.installments.update');

    });



    Route::get('/{user}/account', 'InvoiceController@switchToUser')->name('account.switch');

   

   Route::group(['prefix' => 'wallets'], function () {

    Route::get('/{parent}', 'Admin\WalletController@index')->name('wallets.index');

    Route::get('/{parent}/create', 'Admin\WalletController@create')->name('wallets.create');

    Route::post('/store','Admin\WalletController@store')->name('wallets.store');

    Route::get('{wallet}/user/{user}edit', 'Admin\WalletController@edit')->name('wallets.edit');

    Route::put('/{wallet}/update','Admin\WalletController@update')->name('wallets.update');
    Route::delete('/{user}/{wallet}','Admin\WalletController@destroy')->name('wallets.destroy');
   });

});



Route::name('account.')->prefix('account/')->middleware(['user'])->group(function(){


 
   Route::post('/getCategory', 'PlayersController@getPlayerCategory')->name('player.get.category');
    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::get('/profile', 'HomeController@parentDashboard')->name('profile');

    Route::get('/profile/edit', 'HomeController@parentProfileEdit')->name('profile.edit');

    Route::post('/profile/update', 'HomeController@parentProfileUpdate')->name('profile.update');

    Route::get('/change-password', 'HomeController@changePassword')->name('password.index');
    Route::post('/update-password', 'HomeController@updatePassword')->name('password.update');

    Route::group(['prefix' => 'players'], function () {

    Route::get('/', 'PlayersController@accountIndex')->name('players.index');
    Route::get('/create', 'PlayersController@create')->name('players.create');
    Route::post('/store', 'PlayersController@store')->name('players.store');
    Route::get('/{player}/edit', 'PlayersController@edit')->name('players.edit');
    Route::put('/{player}/update', 'PlayersController@update')->name('players.update');

    });

    //display terms
   Route::group(['prefix' => 'terms'], function () {
     
    Route::get('/', 'DashboardController@allTerms')->name('terms');
    Route::post('/store', 'DashboardController@storePlayerTerm')->name('player.term.store');
    Route::post('/player/details', 'DashboardController@getPlayerDetails')->name('player.details');
    Route::get('session/player/{player}/term/{term}/emirate/{emirate}/edit', 'DashboardController@editTrainingSession')->name('player.training.session.edit');
    Route::post('/player/{player}/term/{term}/emirate/{emirate}/update', 'DashboardController@updatePlayerTrainingSession')->name('player.training.session.update');
    

    });
     Route::get('player/{player}/term/{term}/id/{id}', 'DashboardController@destroyPlayerTerm')->name('player.term.delete');
    Route::get('/switch/admin', 'InvoiceController@switchToAdmin')->name('switch.admin');

      Route::group(['prefix' => 'invoices-request'], function () {
      Route::get('/', 'InvoiceController@invoicesRequest')->name('invoices.request');
      Route::post('/review','InvoiceController@reviewInvoice')->name('player.review.invoice');
      Route::post('/request/invoices','InvoiceController@storeInvoice')->name('submit.invoice.request');


     });

     Route::group(['prefix' => 'invoices'], function () {

     Route::get('/', 'InvoiceController@index')->name('request.invoices.index');
     Route::get('/invoice-report/{invoice}', 'InvoiceController@generatedReport')->name('invoices.report');
     Route::get('player/{invoiceNo}/checkout','InvoiceController@checkout')->name('player.invoices.checkout');
     Route::get('/redirectTo/{action}', 'InvoiceController@redirectRoutes');
     Route::post('/redirectTo/{action}', 'PaymentsController@redirectRoutes')->name('players.payments.redirect_routes');


    });

    Route::group(['prefix' => 'installments'], function () {

     Route::get('/', 'InvoiceController@installments')->name('installments.index');
     Route::get('/invoice-report/{invoice}', 'InvoiceController@generatedReport')->name('installments.report');
     Route::get('player/{invoiceNo}/checkout','InvoiceController@checkout')->name('player.invoices.checkout');
     Route::get('/redirectTo/{action}', 'InvoiceController@redirectRoutes');
     Route::post('/redirectTo/{action}', 'PaymentsController@redirectRoutes')->name('players.payments.redirect_routes');


    });


     Route::group(['prefix' => 'discount'], function () {

        Route::get('/', 'DiscountController@index')->name('discounts.index');
        Route::get('/player/{player}/term/{term}', 'DiscountController@playerDiscount')->name('players.discount.index');
        Route::post('/player/{player}/term/{term}', 'DiscountController@playerDiscountupdate')->name('players.discount.update');

    });

});



 Route::name('coach.')->prefix('coach/')->middleware(['coach'])->group(function(){

  Route::get('/', 'DashboardController@index')->name('dashboard');



    Route::group(['prefix' => 'players'], function () {

      Route::get('/', 'PlayersController@index')->name('players.index');

      Route::get('/{player}/terms', 'PlayersController@playerTerm')->name('players.term');

      Route::get('/search', 'PlayersController@playersSearch')->name('players.serach');

      Route::get('/{player}/edit', 'PlayersController@edit')->name('players.edit');

      Route::put('/{player}/update','PlayersController@update')->name('players.update');

      Route::delete('/{player}/delete', 'PlayersController@destroy')->name('players.destroy');

  }); 



    Route::group(['prefix' => 'evaluations'], function () {

      Route::get('{player}/index', 'EvaluationController@index')->name('player.evaluations.index');

      Route::get('/{player}/create', 'EvaluationController@create')->name('player.evaluations.create');

      Route::post('/store', 'EvaluationController@store')->name('player.evaluations.store');

      Route::get('/{evaluation}/edit', 'EvaluationController@edit')->name('player.evaluations.edit');

      Route::put('/{evaluation}/update','EvaluationController@update')

      ->name('player.evaluations.update');

      Route::delete('/{evaluation}/delete', 'EvaluationController@destroy')->name('player.evaluations.destroy');

    });
    
       Route::group(['prefix' => 'attendance'], function () {

      Route::get('/', 'AttendanceController@index')->name('attendance.index');

      Route::get('/search', 'AttendanceController@search')->name('attendance.search');

      Route::post('/store', 'AttendanceController@store')->name('attendance.store');

    });

});

