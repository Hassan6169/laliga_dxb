
<ul class="unstyled sidebar-navigation">
    
    <li>
        <a href="{{ route('account.dashboard') }}"  class="{{(Request::segment(2) == '') ? 'active' : 'disable'}}"><i class="xxicon icon-home"></i> Dashboard</a>
    </li>
    <li>
        <a href="{{ route('account.players.index') }}" class="{{(Request::segment(2) == 'players') ? 'active' : 'disable'}}"><i class="xxicon icon-users"></i> Players</a>
    </li>
    <li>
        <a href="{{route('account.terms')}}" class="{{(Request::segment(2) == 'terms') ? 'active' : 'disable'}}"><i class="xxicon icon-layers"></i> Terms</a>
    </li>
    
    <li>
        <a href="{{route('account.request.invoices.index')}}" class="{{(Request::segment(2) == 'invoices') ? 'active' : 'disable'}}"><i class="xxicon icon-credit-card"></i>Invoices</a>
    </li>

       <li>
        <a href="{{route('account.installments.index')}}" class="{{(Request::segment(2) == 'installments') ? 'active' : 'disable'}}"><i class="xxicon icon-credit-card"></i>Installment Invoices</a>
    </li>

     </li>

       <li>
        <a href="{{route('account.password.index')}}" class="{{(Request::segment(2) == 'change-password') ? 'active' : 'disable'}}"><i class="xxicon icon-credit-card"></i>Change Password</a>
    </li>
    
    @if(!empty(session()->get('admin_id')))
    <li>
        <a href="{{route('account.switch.admin')}}"><i class="xxicon icon-log-out"></i>Switch to Admin</a>
    </li>
    <li>
        
        <a href="{{route('account.invoices.request')}}" class="{{(Request::segment(2) == 'invoices-request') ? 'active' : 'disable'}}"><i class="xxicon icon-log-out"></i>Invoice Request</a>
    </li>
    @endif
    <li>
        <a href="{{route('logout')}}"><i class="xxicon icon-log-out"></i> Logout</a>
    </li>
</ul>