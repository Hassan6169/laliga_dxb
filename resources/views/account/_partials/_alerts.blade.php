@if (session('success'))
<div class="alert --success" role="alert" id="successMessage">
	{{ session('success') }}
	<a href="javascript:;" class="xicon xicon-close close-alert close-this"></a>
</div>
@elseif (session('error'))
<div class="alert --danger" role="alert" id="errorMessage">
	{{ session('error') }}
	<a href="javascript:;" class="xicon xicon-close close-alert close-this"></a>
</div>
@endif