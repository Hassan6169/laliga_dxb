<div class="control-group">
	<label for="name" class="form-label">{{ __('Player Name') }}</label>
	{{ Form::text('name', null, ['class' => 'form-field', 'required', 'autocomplete'=>'name','maxlength'=>'100', 'autofocus']) }}
	@error('name')
	<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
	</span>
	@enderror
</div>
<div class="control-group">
	<label for="gender" class="form-label">{{ __('Gender') }}</label>
	<div class="radiobox ml-0">
		{{ Form::radio('gender', GENDER_MALE, true) }}
		<span class="label"></span>
		<span class="text">Male</span>
	</div>
	<div class="radiobox">
		{{ Form::radio('gender', GENDER_FEMALE) }}
		<span class="label"></span>
		<span class="text">Female</span>
	</div>
	
</div>
<div class="control-group">
	<label for="dob" class="form-label">{{ __('Date Of Birth') }}</label>
	{{ Form::text('dob', null, ['class' => 'form-field','id'=>'dob','autocomplete'=>"off",'required'=>'required']) }}
	@error('dob')
	<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
	</span>
	@enderror
</div>

@if(Route::currentRouteName() == 'account.players.edit')
<div class="control-group">
	<label for="mobile" class="form-label">{{ __('Age Category') }}</label>
	{{ Form::text('category_id', $player->category->name, ['class' => 'form-field','id'=>'age_category', 'readonly','required'=>'required']) }}
</div>
@else
<div class="control-group">
	<label for="mobile" class="form-label">{{ __('Age Category') }}</label>
	{{ Form::text('category_id', null, ['class' => 'form-field','id'=>'age_category', 'readonly','required'=>'required']) }}
</div>
@endif
<!-- <div class="control-group">
	<label for="school" class="form-label">{{ __('School') }}</label>
	{{ Form::text('school', null, ['class' => 'form-field']) }}
	
</div> -->
<div class="control-group">
	<label for="previous_football_academy" class="form-label">{{ __('Current Football Academy') }}</label>
	{{ Form::text('previous_football_academy', null, ['class' => 'form-field']) }}
	
</div>
<!-- <div class="control-group">
	<label for="nationality" class="form-label">{{ __('Nationality') }}</label>
	{{ Form::select('nationality', $arrays::countries(), null, ['class' => 'form-field', 'placeholder' => 'Select country']) }}
	
</div> -->
@if(Route::currentRouteName() == 'account.players.edit')
<div class="control-group">
	<label for="photo" class="form-label">{{ __('Photo') }}</label>
	@if($player->photo)
	<a href="{{ Storage::url($player->photo) }}" target="_blank">View Photo</a>
	@endif
	{{ Form::file('photo', ['class' => 'form-field'] ) }}
	@error('photo')
	<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
	</span>
	@enderror
</div>
<div class="control-group">
	<label for="medical_insurance" class="form-label">{{ __('Medical Insurance') }}</label>
	@if($player->medical_insurance)
	<a href="{{ Storage::url($player->medical_insurance) }}" target="_blank">View Medical Insurance</a>
	@endif
	{{ Form::file('medical_insurance', ['class' => 'form-field'] ) }}
	@error('medical_insurance')
	<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
	</span>
	@enderror
</div>
<div class="control-group">
	<label for="emirates_id_front" class="form-label">{{ __('Emirates ID Front') }}</label>
	@if($player->emirates_id_front)
	<a href="{{ Storage::url($player->emirates_id_front) }}" target="_blank">View Emirates ID Front</a>
	@endif
	{{ Form::file('emirates_id_front', ['class' => 'form-field'] ) }}
	@error('emirates_id_front')
	<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
	</span>
	@enderror
</div>
<div class="control-group">
	<label for="emirates_id_back" class="form-label">{{ __('Emirates ID Back') }}</label>
	@if($player->emirates_id_back)
	<a href="{{ Storage::url($player->emirates_id_back) }}" target="_blank">View Emirates ID Back</a>
	@endif
	{{ Form::file('emirates_id_back', ['class' => 'form-field'] ) }}
	@error('emirates_id_back')
	<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
	</span>
	@enderror
</div>
@endif
<script type="text/javascript">
$(document).ready(function(){
	$('#dob').datepicker({
		changeMonth: true,
        changeYear: true,
        dateFormat: 'mm/dd/yy',
        yearRange: "2003:2018",
        defaultDate: "1/1/2003"
	});

	$('#dob').change(function(){
		var gender=$("input[name=gender]:checked").val();
		var date=$('#dob').val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: 'POST',
			url: '/account/getCategory',
			data: {date:date,gender:gender},
			dataType: 'json',
			success: function (data) {
				$('#age_category').val(data.dataResult);
			},
			error: function (data) {
				console.log(data);
			}
		});
	});
});
</script>