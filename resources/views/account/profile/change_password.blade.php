@extends('layouts.parent')
@section('content')
<section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
    <div class="container-wrapper">
        <article class="inner-content">
            <h2 class="maintitle --black --small">
            {{ __('Edit Player') }}
            </h2>
            <div class="row">
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                        <h3 class="maintitle --white --small text-center mb-0">Wallet:{{ $wallet }} AED</h3>
                    </div>
                    <section class="box --shadow-box dashboard-content-detail">
                    <h4 class="title">{{ __('Change Password') }}</span></h4>
                    {{ Form::open(['route' => 'account.password.update', 'class'=>'default-form']) }}
                    <div class="control-group">
                        <label class="form-label">{{ __('Password*') }}</label>
                        <input id="password" type="password" class="form-field" name="password" required autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        
                    </div>
                    <div class="control-group">
                        <label class="form-label">{{ __('Confirm Password*') }}</label>
                        <input id="password-confirm" type="password" class="form-field" name="password_confirmation" required autocomplete="new-password">
                        @error('password-confirm')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn --btn-primary">
                            {{ __('Save') }}
                            </button>
                    {{ Form::close() }}
                </section>
            </div>
        </div>
    </article>
</div>
</section>
@endsection