@extends('layouts.parent')
@section('content')
<section class="sec-padding bg-spgrey">
    <div class="container-wrapper">
        <article class="inner-content">
            <h2 class="maintitle --black --small">
            {{ __('Training Session ') }}
            </h2>
            
            <div class="row">
                
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                        <center> <h3 class="maintitle --white --small">Wallet:{{ $wallet }} Dirham</h3></center>
                    </div>
                    <section class="box --shadow-box dashboard-content-detail">
                        <h4 class="title">{{ __('Invoice') }}</h4>
                        <div class="text-right">
                            <a href="{{ route('account.dashboard') }}" class="btn --btn-small --btn-secondary">Back</a>
                        </div>
                        @include('account._partials._alerts')
                        @php $date=\Carbon\Carbon::today()->format('m/d/Y'); @endphp
                        {{ Form::open(['route'=>['account.player.review.invoice'], 'class'=>'default-form','method'=>'POST']) }}
                        <div class="Rtable --collapse --2cols">
                            <div class="Rtable-cell --head">
                                Select Start Date
                            </div>
                            <div class="Rtable-cell --head">
                                Select End Date
                            </div>
                            
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        Select Start Date
                                    </div>
                                    <div class="control-group">
                                        
                                        {{ Form::text('start_date',$date, ['class' => 'form-field dob','autocomplete'=>"off"]) }}
                                        @error('start_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        Select Term
                                    </div>
                                    <div class="content">
                                        <div class="control-group">
                                            {{ Form::text('end_date', null, ['class' => 'form-field dob','autocomplete'=>"off"]) }}
                                            @error('end_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="Rtable --collapse --5cols">
                            
                            <div class="Rtable-cell --head">
                                
                            </div>
                            <div class="Rtable-cell --head">
                                Player Name
                            </div>
                            <div class="Rtable-cell --head">
                                Term  Details
                            </div>
                            <div class="Rtable-cell --head">
                                Days
                            </div>
                            
                            <div class="Rtable-cell --head">
                                Discounts On Player Terms
                            </div>
                            @foreach($players as $player)
                            @foreach($player->terms as $term)
                            @if($term->pivot->deleted_at==null && $term->type_id==3 && !in_array($term->pivot->serial_no,$checkInvoiceNo))
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header"></div>
                                    <div class="content">
                                        <div class="checkbox">
                                            <input type="checkbox" name="players[]" value="{{$player->id }}-{{$term->id}}" id="players">
                                            <span class="label"></span>
                                            
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Player Details</div>
                                    
                                    <div class="content">
                                        <strong>Name:</strong> {{$player->name}} <br />
                                        <strong>Guardian Name:</strong> {{$player->user->name}} <br />
                                        <strong>DOB:</strong> {{$player->dob}} <br />
                                        <strong>Category:</strong> {{ $player->orginalCategory->name }} <br />
                                        <strong>HPC:</strong> @if($player->is_hpc_player==HPC_PLAYER){{ "YES" }}
                                        @else {{"NO" }}@endif <br />
                                        <strong>Goal Keeper:</strong> @if($player->is_goal_keeper==GOAL_KEEPER){{ "YES" }}
                                        @else {{"NO" }}@endif
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Term Details</div>
                                    
                                    <div class="content">
                                        <div class="content">
                                            
                                            {{$term->name}} <br />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Days</div>
                                    <div class="content">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Discounts On Player Terms</div>
                                    <div class="content">
                                        <a href="{{route('account.players.discount.index',[$player->id,$term->id])}}" class="btn --btn-small bg-warning fc-white">Apply Discount</a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @foreach($player->trainingSessions as $session)
                            
                            @if(!in_array($session->pivot->serial_no,$data) && !in_array($session->pivot->serial_no,$checkInvoiceNo))
                            
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header"></div>
                                    <div class="content">
                                        <div class="checkbox">
                                            <input type="checkbox" name="players[]" value="{{$session->pivot->player_id }}-{{$session->term->id}}" id="players">
                                            <span class="label"></span>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Player Details</div>
                                    
                                    <div class="content">
                                        <strong>Name:</strong> {{$player->name}} <br />
                                        <strong>Guardian Name:</strong> {{$player->user->name}} <br />
                                        <strong>DOB:</strong> {{$player->dob}} <br />
                                        <strong>Category:</strong> {{ $player->orginalCategory->name }} <br />
                                        <strong>HPC:</strong> @if($player->is_hpc_player==HPC_PLAYER){{ "YES" }}
                                        @else {{"NO" }}@endif <br />
                                        <strong>Goal Keeper:</strong> @if($player->is_goal_keeper==GOAL_KEEPER){{ "YES" }}
                                        @else {{"NO" }}@endif
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Term Details</div>
                                    
                                    <div class="content">
                                        <div class="content">
                                            {{$session->term->name}} <br />
                                            <strong>Location:</strong> {{$session->getLocation($session->pivot->location_id)}}<br />
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Days</div>
                                    <div class="content">
                                        @foreach($player->trainingSessions as $player_day)
                                        
                                        @if($session->pivot->serial_no==$player_day->pivot->serial_no)
                                        
                                        {{$session->getDayLiteralAttribute($player_day->pivot->day_id)}}: {{date('H:i',strtotime($player_day->start_time))}}-{{date('H:i',strtotime($player_day->end_time))}}
                                        <br />
                                        
                                        @endif
                                        
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Discounts On Player Terms</div>
                                    <div class="content">
                                        <a href="{{route('account.players.discount.index',[$player->id,$session->term->id])}}" class="btn --btn-small bg-warning fc-white">Apply Discount</a>
                                    </div>
                                </div>
                            </div>
                            
                            
                            @endif
                            
                            @php
                            $data[]=$session->pivot->serial_no; @endphp
                            @endforeach
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <input type="checkbox" name="is_not_vat">
                                    <span class="label"></span>
                                    <span class="text">Remove the automatic 5% Tax</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <input type="checkbox" name="remove_kit_fee">
                                    <span class="label"></span>
                                    <span class="text">Remove the Kit Fees</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <input type="checkbox" name="use_wallet" id="is_wallet_amount">
                                    <span class="label"></span>
                                    <span class="text">Use wallet Credits/Debits</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="wallet_details">
                            <div class="col-md-4">
                                <div class="control-group">
                                    <label for="wallet_amount" class="form-label">{{ __('Amount') }}</label>
                                    <input type="text" name="wallet_amount" class="form-field" id="wallet_amount" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="control-group">
                                    <label for="remarks" class="form-label">{{ __('Remarks') }}</label>
                                    <input type="text" name="remarks" class="form-field" id="remarks">
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn --btn-primary add mt-40">Continue</button>
                        </div>
                    </section>
                </div>
            </div>
        </article>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function () {
$('#wallet_details').hide();
// $('.add').click(function () {
// var checked2 = $("input[name=players]:checked").length;
// if (!checked2) {
// alert("You must check at least one.");
// return false;
// }
// });
$('.dob').datepicker({
dateFormat: 'mm/dd/yy',
});
});
$('#is_wallet_amount').click(function () {
var checked = $("input[name=use_wallet]:checked").length;
if (checked) {
$('#wallet_details').show();
$("#wallet_amount").attr("required", true);
$("#remarks").attr("required", true);
}
else{
$('#wallet_details').hide();
$("#wallet_amount").attr("required", false);
$("#remarks").attr("required", false);
}
});
</script>
@endsection