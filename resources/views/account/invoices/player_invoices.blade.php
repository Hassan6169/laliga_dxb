@extends('layouts.app')
@section('content')
<section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/coach-banner.jpg') }}')">
  <div class="inner-wrapper">
    <h1 class="title">LaLiga Academy</h1>
  </div>
</section>
<section class="sec-padding bg-spgrey">
  <div class="container-wrapper">
    <article class="inner-content">
      <h2 class="maintitle --black --small">
      {{ __('Invoice List') }}
      </h2>
      <div class="row">
        <div class="col-lg-3">
          @include('account._partials.sidebar_menu')
        </div>
        <div class="col-lg-9">
          <div class="alert --warning" role="alert">
            <h3 class="maintitle --white --small text-center mb-0">Wallet: {{ $wallet }} AED</h3>
          </div>
          <section class="box --shadow-box dashboard-content-detail">
            <h4 class="title">{{ __('Invoice List') }}</h4>
            

            @include('account._partials._alerts')
            @if(count($invoice_request)>0)
            <div class="Rtable --collapse --6cols">
              <div class="Rtable-cell --head">
                Invoice No
              </div>
              <div class="Rtable-cell --head">
                Player Name
              </div>
              <div class="Rtable-cell --head">
                Term Details
              </div>
              <div class="Rtable-cell --head">
                Status
              </div>
              <div class="Rtable-cell --head">
                Amount
              </div>
              <div class="Rtable-cell --head">
                Action
              </div>
              
              @foreach($invoice_request as $invoice)
              @php $details=json_decode($invoice->session_details,true); @endphp
              
              <div class="Rtable-cell">
                <div class="body">
                  <div class="header">
                    Invoice No
                  </div>
                  <div class="content">
                    <strong>LA-00{{$invoice->id}}</strong>
                    <?php
                    $invoiceno=$invoice->id;
                    ?>
                  </div>
                </div>
              </div>
              
              <div class="Rtable-cell">
                <div class="body">
                  <div class="header">
                    Player Details
                  </div>
                  <div class="content">
                    
                 
                  @if($invoice->player_id==null)
                  @foreach($details as $key => $detail)
                  @php $player=$invoice->getPlayer($detail['player_id']); @endphp
                  <strong>PL: </strong>{{$player->id}}<br /> 
                  <strong>Name: </strong> {{$player->name}}<br />
                  @endforeach
                  @else
                  <strong>PL: </strong>{{$invoice->player->id}}<br /> 
                  <?php
                    $playerid=$invoice->player->id;
                    ?>
                  <strong>Name: </strong> {{$invoice->player->name}}<br />
                  @endif
                  </div>
                </div>
              </div>
              <div class="Rtable-cell">
                <div class="body">
                  <div class="header"></div>
                  <div class="content">
                 
                  
                  @if($invoice->term_id==null)
                  @foreach($details as $key => $detail)
                  @php $term=$invoice->getTerm($detail['term_id']); @endphp
                  {{$term->name}}
                  @endforeach
                  @else
                  {{$invoice->term->name}}<br/>
                  @endif
                
                  </div>
                </div>
              </div>
              <div class="Rtable-cell">
                <div class="body">
                  <div class="header"></div>
                  <div class="content">
                    @if($invoice->payment_status==STATUS_PAID)
                    {{"PAID"}}
                    @elseif($invoice->payment_status==STATUS_READY_TO_PAY)
                    {{" Ready to Pay"}}
                    @elseif($invoice->payment_status==STATUS_PENDING)
                    {{"Pending"}}
                    @elseif($invoice->payment_status==STATUS_NOT_PAID)
                    {{"NOT PAID"}}
                    @endif
                  </div>
                </div>
              </div>
              <div class="Rtable-cell text-lg-right">
                <div class="body">
                  <div class="header">Amount</div>
                  <div class="content">
                    {{ $invoice->total_amount }} Dirham
                    <?php
                    $invoiceamount=($invoice->total_amount)*100;
                    ?>
                  </div>
                </div>
              </div>
              <div class="Rtable-cell">
                <div class="body">
                  <div class="header"></div>
                  <div class="content">
<?php
if($invoice->payment_status!=STATUS_PAID){

$randomnum=rand();

$returnurl= URL::current();
// echo $returnurl;
$returnurl2= URL::to('/');

$shaString  = '';

$arrData    = array(
'command'            =>'AUTHORIZATION',
'access_code'        =>'IjeYi9rRDhUDY43jAIGc',
'merchant_identifier'=>'LmIAIBAK',
'merchant_reference' =>$randomnum.'LA-00'.$invoiceno,
'amount'             =>$invoiceamount,
'currency'           =>'AED',
'language'           =>'en',
'merchant_extra'     =>$playerid,
'merchant_extra2'    =>$invoiceno,
'customer_email'     =>'test@payfort.com',
'order_description'  =>'Payment Of Invoice Number:'.$invoiceno,
'return_url'         =>$returnurl2.'/returnpage2',
);

ksort($arrData);
foreach ($arrData as $key => $value) {
    $shaString .= "$key=$value";
}

$shaString = "Request123@" . $shaString . "Request123@";
$signature = hash("sha256", $shaString);

$requestParams = array(
'command'            =>'AUTHORIZATION',
'access_code'        =>'IjeYi9rRDhUDY43jAIGc',
'merchant_identifier'=>'LmIAIBAK',
'merchant_reference' =>$randomnum.'LA-00'.$invoiceno,
'amount'             =>$invoiceamount,
'currency'           =>'AED',
'language'           =>'en',
'merchant_extra'     =>$playerid,
'merchant_extra2'    =>$invoiceno,
'customer_email'     =>'test@payfort.com',
'signature'          =>$signature,
'order_description'  =>'Payment Of Invoice Number:'.$invoiceno,
'return_url'         =>$returnurl2.'/returnpage2',
);


// $redirectUrl = 'https://checkout.payfort.com/FortAPI/paymentPage';
$redirectUrl = 'https://sbcheckout.payfort.com/FortAPI/paymentPage';

echo "<form action='$redirectUrl' method='post' name='frm'>\n";
foreach ($requestParams as $a => $b) {

  echo "\t<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>\n";
}
echo "\t<button type='submit' class='btn --btn-small --btn-primary'>Pay Now</button>\n";
echo "\t</form>\n";

}
if($invoice->payment_status==STATUS_PAID){
  echo "\t<button class='btn --btn-small --btn-primary' value='Paid'>Paid</button>\n";
}
?>                      
                    <!-- <a href="{{route('account.player.invoices.checkout',[$invoice->id])}}"><button type="button" class="btn --btn-small --btn-primary">Pay Now</button></a> -->
                    <a href="{{route('account.invoices.report',[$invoice->id])}}" class="btn --btn-small --btn-primary" target="_blank">
                      View 
                    </a>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
            @endif
          </section>
          
        </div>
      </div>
    </article>
  </div>
  @endsection