<div class="box-body">
    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <div class="col-sm-3">
        {!! Form::label('name', 'Name', ['class'=>'control-label']) !!}
      </div>
      <div class="col-sm-9">
        {!! Form::text('name', null, ['class'=>'form-field','required' => 'required']) !!}
      </div>
      @if ($errors->has('name'))
      <span class="error-msg">
        <strong>{{ $errors->first('name') }}</strong>
      </span>
      @endif
    </div>
    <div class="control-group{{ $errors->has('emirate_id') ? ' has-error' : '' }}">
      <div class="col-sm-3">
        {!! Form::label('emirate', 'Select Emirate', ['class' => 'control-label']) !!}
      </div>
      <div class="col-sm-9">
        <select class="form-field{{ $errors->has('emirate_id') ? ' is-invalid' : '' }}" name="emirate_id" required>
          @foreach($emirates as $emirate)
          <option value="{{$emirate->id }}" {{(!empty($location)
          && $location->emirate_id==$emirate->id) ? 'selected':''}}>{{$emirate->name }}</option>
          @endforeach
        </select>
      </div>
      @if ($errors->has('emirate_id'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('emirate_id') }}</strong>
      </span>
      @endif
    </div>
    <div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
      <div class="col-sm-3">
        {!! Form::checkbox('is_active', 1, isset($location) ? $location->is_active : true) !!}
        {!! Form::label('is_active', 'Activate', ['class' => 'control-label']) !!}
        @if ($errors->has('is_active'))
        <span class="error-msg">
          <strong>{{ $errors->first('is_active') }}</strong>
        </span>
        @endif
      </div>
    </div>
  <div class="control-group">
    <center>
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
    
  </div>
</div>
</div>