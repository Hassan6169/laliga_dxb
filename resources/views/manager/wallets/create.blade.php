@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Add Wallet Amount</h3>

<section class="box">

	<div class="box-body">

		{!! Form::open(['route' => 'manager.wallets.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

	  		@include('manager.wallets._form',['submitBtnText' => 'Add Wallet'])

	  	{!! Form::close() !!}

	</div>

</section>

@endsection