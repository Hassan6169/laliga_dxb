@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Edit Season</h3>

<section class="box">

<div class="box-body">

    {!! Form::model($season, ['route' => ['manager.seasons.update', $season->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('manager.seasons._form',['submitBtnText' => 'Update'])

    {!! Form::close() !!}

  </div>

</section>

@endsection