@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Edit Age Category</h3>

<section class="box">

  <div class="box-body">

    {!! Form::model($signUp, ['route' => ['manager.signups.update',$signUp->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"form-horizontal"]) !!}

    @include('manager.sign_ups._form',['submitBtnText' => 'Update'])

    {!! Form::close() !!}

  </div>

</section>

@endsection