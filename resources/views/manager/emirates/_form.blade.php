
<div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">

    {!! Form::label('name', 'Name', ['class'=>'control-label']) !!}

    {!! Form::text('name', null, ['class'=>'form-field inputTextBox','required' => 'required']) !!}

    @if ($errors->has('name'))
    <span class="error-msg">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
    @endif

</div>

<div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">

    <div class="checkbox">
        {!! Form::checkbox('is_active', 1, isset($emirate) ? $emirate->is_active : true) !!}

        <span class="label"></span>
        <span class="text">
            {!! Form::label('is_active', 'Activate', ['class' => 'control-label']) !!}
        </span>
    </div>

    @if ($errors->has('is_active'))
    <span class="error-msg">
        <strong>{{ $errors->first('is_active') }}</strong>
    </span>
    @endif

</div>

<div class="control-group">

    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">

    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
        
</div>