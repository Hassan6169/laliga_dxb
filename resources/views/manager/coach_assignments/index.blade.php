@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Coach Assignments</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-6">
        <a href="{{route('manager.coach.assignments.create')}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Assign Coach</button></a>
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-primary fc-white">Back</a>
      </div>
      <div class="col-md-6">
        <div class="default-form">
          <div class="control-group --search-group mb-0 float-right">
            <input type="text" placeholder="Search" class="search-field">
            <div class="search-button">
              <button type="submit" class="form-button">
              <i class="xxicon icon-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <tr>
        <th><center>S NO</center></th>
        <th><center>Category</center></th>
        <th><center>Location</center></th>
        <th><center>Coach Name</center></th>
        <th><center>Team Name</center></th>
        <th><center>Action</center></th>
      </tr>
      @foreach ($users as $user)
      @foreach ($user->teams as $team)
      @if($team->pivot->is_active==STATUS_ENABLED)
      <tr>
        <td><center>{{ $no++ }}</center></td>
        <td><center>{{$user->getCategory($team->pivot->category_id)}}</center></td>
        <td><center>{{$user->getLocation($team->pivot->location_id)}}</center></td>
        <td><center>{{$user->name}}</center></td>
        <td><center>{{$team->name}}</center></td>
        <td><center><a href="{{ route('manager.coach.assignments.edit',[$user->id,$team->pivot->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Edit</button></a>
        <a href="{{ route('manager.coach.assignments.destroy',[$user->id,$team->pivot->id]) }}"><button type="button" class="btn --btn-small bg-danger fc-white">Delete</button></a></center></td>
      </tr>
      @endif
      @endforeach
      @endforeach
    </table>
    <ul class="pagination">
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-left"></i>
        </a>
      </li>
      <li>
        <a href="#">1</a>
      </li>
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-right"></i>
        </a>
      </li>
    </ul>
  </div>
</section>

@endsection