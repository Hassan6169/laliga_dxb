@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Assign Coach</h3>
<section class="box">
	<div class="box-body">
		{!! Form::open(['route' => 'manager.coach.assignments.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('manager.coach_assignments._form',['submitBtnText' => 'Assign Coach'])
		{!! Form::close() !!}
	</div>
</section>
@endsection