<div class="box-body">
        <div class="control-group{{ $errors->has('team_id') ? ' has-error' : '' }}">
            <div class="col-sm-3">
                {!! Form::label('team', 'Team', ['class'=>'control-label']) !!}
            </div>
            <div class="col-sm-9">
                <select class="form-field" name="team_id" required>
                    <option value="">Select Team</option>
                    @foreach ($teams as $team)
                    <option value="{{$team->id}}" {{ isset($data) && $data->pivot->team_id==$team->id ? 'selected':''}}>{{$team->name}}</option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('team_id'))
            <span class="error-msg">
                <strong>{{ $errors->first('team_id') }}</strong>
            </span>
            @endif
        </div>
        <div class="control-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            <div class="col-sm-3">
                {!! Form::label('category_id', 'Category', ['class'=>'control-label']) !!}
            </div>
            <div class="col-sm-9">
                <select class="form-field" name="category_id" required>
                    <option value="">Select Category</option>
                    @foreach ($categories as $category)
                    <option value="{{$category->id}}" {{isset($data) && $data->pivot->category_id==$category->id ? 'selected':''}}>{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('category_id'))
            <span class="error-msg">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
            @endif
        </div>
        <div class="control-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
            <div class="col-sm-3">
                {!! Form::label('location_id', 'Location', ['class'=>'control-label']) !!}
            </div>
            <div class="col-sm-9">
                <select class="form-field" name="location_id" required>
                    <option value="">Select Location</option>
                    @foreach ($locations as $location)
                    <option value="{{$location->id}}" {{isset($data) && $data->pivot->location_id==$category->id ? 'selected':''}}>{{$location->name}}</option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('location_id'))
            <span class="error-msg">
                <strong>{{ $errors->first('location_id') }}</strong>
            </span>
            @endif
        </div>
        <div class="control-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
            <div class="col-sm-3">
                {!! Form::label('user_id', 'Coach', ['class'=>'control-label']) !!}
            </div>
            <div class="col-sm-9">
                <select class="form-field" name="user_id" required>
                    <option value="">Select User</option>
                    @foreach ($users as $user)
                    <option value="{{$user->id}}" {{isset($data) && $data->pivot->user_id==$user->id ? 'selected':''}}>{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('user_id'))
            <span class="error-msg">
                <strong>{{ $errors->first('user_id') }}</strong>
            </span>
            @endif
        </div>
    <div class="control-group">
        <center>
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
        
    </div>
</div>
</div>