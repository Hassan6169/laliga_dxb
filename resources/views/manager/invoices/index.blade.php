@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Invoice</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <h4 class="box-title">
        Invoice Details
        </h4>
    </div>
    <div class="box-body">
           {!! Form::open(['route' => 'manager.players.serach', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="control-group">
                    {!! Form::text('keyword', $keyword ?? null, ['class'=>'form-field','placeholder' => 'Search By Keyword']) !!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('parent_no', $parent_no ?? null, ['class'=>'form-field','placeholder' => 'Parent No']) !!}
                </div>

                <div class="control-group">
                    {!! Form::text('player_mobile',$player_mobile ?? null, ['class'=>'form-field','placeholder' => 'Search By Mobile']) !!}
                </div>

            </div>

            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('player_no',$player_no ?? null, ['class'=>'form-field','placeholder' => 'Player No']) !!}
                </div>

                <div class="control-group">
                    {!! Form::select('gender', array(''=>'Select Gender','1' => 'Male', '2' => 'Female'),$gender ?? null,['class' => 'form-field']); !!}
                </div>

                
            </div>

            <div class="col-md-3">

                <div class="control-group">
                    {!! Form::text('invoice_no',$invoice_no ?? null, ['class'=>'form-field','placeholder' => 'LA  No']) !!}
                </div>
                <div class="control-group">
                    {!! Form::text('player_name', $player_name ?? null, ['class'=>'form-field','placeholder' => 'Player Name']) !!}
                </div>

                <div class="control-group">
                    <select class="form-field" name="category_id">
                        <option value="">Select Age Category</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}" @if(!empty($category_id) && $category_id == $category->id) selected="selected" @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="control-group">
                    {!! Form::select('is_hpc_player', array(''=>'Select HPC Player','1' => 'Yes', '2' => 'No'),$is_hpc_player ?? null,['class' => 'form-field']); !!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('player_email',$email ?? null, ['class'=>'form-field','placeholder' => 'Search By Email']) !!}
                </div>

                <div class="control-group">
                    <select class="form-field" name="admin_category_id">
                        <option value="">Management Category System</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}" @if(!empty($admin_category_id) && $admin_category_id == $category->id) selected="selected" @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="control-group">
                    {!! Form::select('refund_amount', array(''=>'Refund Amount','1' => 'Yes', '2' => 'No'),$is_goal_keeper ?? null,['class' => 'form-field']); !!}
                </div>
                
            </div>
        </div>

        <button type="submit" class="btn --btn-primary">Search</button>
        {!! Form::close() !!}
        <table class="table --bordered --hover">
            <tr>
                <th>Invoice No</th>
                <th>Player Details</th>
                <th>Training Details</th>
                <th>Payment Details</th>
                <th>Action</th>
            </tr>
            
            @foreach ($invoices as $invoice)
            @php $session_details=json_decode($invoice->session_details,true);
            $player=array();
            $data=array();
            @endphp
            
            <tr>
                <td>
                    <strong>LA-{{$invoice->invoice_no}}</strong>
                </td>
                
                <td>
                    @foreach($session_details as $key => $detail)
                    @if(!in_array($detail['player_id'],$player))
                    <strong>Name: </strong> {{$invoice->player($detail['player_id'])->name}}<br />
                    <strong>Email: </strong>{{$invoice->player($detail['player_id'])->user->email}}<br />
                    <strong>Mobile: </strong>{{$invoice->player($detail['player_id'])->user->mobile}}<br />
                    <br>
                    @php $player[]=$detail['player_id']; @endphp
                    @endif
                    @endforeach
                </td>
                
                <td>
                    @foreach($session_details as $key => $detail)
                    @if(!in_array($detail['player_id'],$data))
                    <strong>Term Name:</strong>{{$invoice->trainingSession($detail['session_id'])->term->name}} <br>  <strong>Location:</strong>{{$invoice->getLocation($detail['location_id'])}} <br />
                    @php $data[]=$detail['player_id'];
                    @endphp
                    @endif
                    <br>
                    <!-- <strong>Day: {{$invoice->day($detail['day_id'])}} </strong><br />
                    {{date('H:i',strtotime($invoice->trainingSession($detail['session_id'])->start_time))}}-{{date('H:i',strtotime($invoice->trainingSession($detail['session_id'])->end_time))}} <br /> -->
                    @endforeach
                </td>
                
                <td>
                    <strong>Kit Amount: </strong>{{$invoice->kit_amount}} <br />
                    
                    
                    <strong>Tax: </strong>{{$invoice->tax_amount}}<br />
                    @if(empty($invoice->total_amount))
                    <strong>Total Amount: </strong>{{$invoice->amount}}<br />
                    @else
                    <strong>Total Amount: </strong>{{$invoice->total_amount}}<br />
                    @endif
                    @if($invoice->payment_status==STATUS_PAID)
                    <strong>Status: </strong>PAID
                    @elseif($invoice->payment_status==STATUS_READY_TO_PAY)
                    <strong>Status: </strong>Ready to Pay
                    @elseif($invoice->payment_status==STATUS_PENDING)
                    <strong>Status: </strong>Pending
                    @endif
                </td>
                
                <td>
                    <a target="_blank" href="{{route('manager.player.invoices.show',[$invoice->invoice_no])}}">
                        <button class="btn --btn-small bg-secondary fc-white btn-sm">Preview Invoice</button>
                    </a>
                    
                    <a href="{{route('manager.player.invoices.report',[$invoice->invoice_no])}}" class="btn --btn-small bg-secondary fc-white btn-sm">Download
                    </a>
                    <br />
                    @if($invoice->partial_payment_type==STATUS_PARTIAL_PAYMENT)
                    <a href="{{route('manager.invoices.payments.installments',[$invoice->id])}}" class="btn --btn-small bg-warning fc-white btn-sm">Installments</a>
                    <br />
                    @endif
                    <a href="{{route('manager.invoices.partial.refund.index',[$invoice->id])}}" class="btn --btn-small bg-secondary fc-white btn-sm">Partial Refund</a>
                    
                    <br />
                    
                    <a href="{{route('manager.invoices.write.off.index',[$invoice->id])}}" class="btn --btn-small bg-info fc-white btn-sm">Write Off</a>
                    
                    <br />
                    <a href="{{route('manager.invoices.payments.method.edit',[$invoice->id])}}" class="btn --btn-small bg-success fc-white btn-sm">Edit Payment Method</a>
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $invoice->id }}" id="deleteItem">Delete</button>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection