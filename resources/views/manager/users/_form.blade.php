@inject('arrays','App\Http\Utilities\Arrays')
<div class="box-body">
    
    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">

        {!! Form::label('name', 'Guardian’s Full Name', ['class' => 'form-label']) !!}

        {!! Form::text('name', null, ['class'=>'form-field','placeholder' => 'Guardian’s Full Name']) !!}

        @if ($errors->has('name'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Guardian’s Email', ['class' => 'form-label']) !!}

        {!! Form::text('email', null, ['class'=>'form-field','placeholder' => 'Guardian’s Email', 'readonly']) !!}

        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>

     <div class="control-group{{ $errors->has('mobile') ? ' has-error' : '' }}">

        {!! Form::label('mobile', 'Guardian’s Mobile', ['class' => 'form-label']) !!}

        {!! Form::text('mobile', null, ['class'=>'form-field','placeholder' => 'Guardian’s Mobile']) !!}

        @if ($errors->has('mobile'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('mobile') }}</strong>
        </span>
        @endif
    </div>

     <div class="control-group{{ $errors->has('parent_1_relationship') ? ' has-error' : '' }}">

        {!! Form::label('parent_1_relationship', 'Guardian’s 1 Relation', ['class' => 'form-label']) !!}
        
        {{ Form::text('parent_1_relationship',null,['class' => 'form-field','placeholder'=>"Guardian’s 1 Relation"]) }}
        

        @if ($errors->has('parent_1_relationship'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('parent_1_relationship') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('parent_2_name') ? ' has-error' : '' }}">

        {!! Form::label('parent_2_name', 'Guardian’s 2 Full Name', ['class' => 'form-label']) !!}

        {!! Form::text('parent_2_name', null, ['class'=>'form-field','placeholder' => 'Guardian’s 2 Full Name']) !!}

        @if ($errors->has('parent_2_name'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('parent_2_name') }}</strong>
        </span>
        @endif
    </div>
   
    <div class="control-group{{ $errors->has('parent_2_email') ? ' has-error' : '' }}">

        {!! Form::label('parent_2_email', 'Guardian’s 2 Email', ['class' => 'form-label']) !!}

        {!! Form::text('parent_2_email', null, ['class'=>'form-field','placeholder' => 'Guardian’s Secondary Email']) !!}

        @if ($errors->has('parent_2_email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('parent_2_email') }}</strong>
        </span>
        @endif
    </div>
   
    
    <div class="control-group{{ $errors->has('parent_2_mobile') ? ' has-error' : '' }}">

        {!! Form::label('parent_2_mobile', 'Guardian’s 2 Contact Number', ['class' => 'form-label']) !!}

        {!! Form::text('parent_2_mobile', null, ['class'=>'form-field', 'placeholder' => 'Guardian’s 2 Contact Number']) !!}

        @if ($errors->has('parent_2_mobile'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('parent_2_mobile') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('parent_2_relationship') ? ' has-error' : '' }}">

        {!! Form::label('parent_2_relationship', 'Guardian’s 2 Relation', ['class' => 'form-label']) !!}
        
        {{ Form::text('parent_2_relationship',null,['class' => 'form-field','placeholder'=>"Guardian’s 2 Relation"]) }}
        

        @if ($errors->has('parent_2_relationship'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('parent_2_relationship') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('nationality') ? ' has-error' : '' }}">

        {!! Form::label('nationality', 'Nationality', ['class' => 'form-label']) !!}

         {{ Form::select('nationality', $arrays::countries(),$user->nationality, ['class' => 'form-field']) }}

        @if ($errors->has('nationality'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('nationality') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('change_password') ? ' has-error' : '' }}">

        {!! Form::label('change_password', 'Account Password', ['class' => 'form-label']) !!}

         <input id="password" type="password" class="form-field" name="change_password" required autocomplete="current-password">

        @if ($errors->has('change_password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('change_password') }}</strong>
        </span>
        @endif
    </div>
  
    <div class="control-group{{ $errors->has('hear_about_us') ? ' has-error' : '' }}">

        {!! Form::label('hear_about_us', 'How did you hear about us?', ['class' => 'form-label']) !!}
        
        {{ Form::select('hear_about_us',$hear_about_us,$user->hear_about_us, ['class' => 'form-field','placeholder'=>"Select Option"]) }}
        

        @if ($errors->has('hear_about_us'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('hear_about_us') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group mb-0">
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white" id="submit">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
    </div>
</div>