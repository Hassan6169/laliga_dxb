@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Add Fee</h3>
<section class="box">
	<div class="box-body">
		<a href="{{ url()->previous() }}" class="btn --btn-small bg-primary fc-white">Back</a>
		{!! Form::open(['route' =>['manager.fee.sessions.store'], 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('manager.fees._form',['submitBtnText' => 'Add Fees'])
		{!! Form::close() !!}
	</div>
	<!-- Modal -->
</section>
@endsection