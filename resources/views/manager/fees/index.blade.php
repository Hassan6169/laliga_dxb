@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Fees</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-6">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-primary fc-white">Back</a>
        <a href="{{ route('manager.fee.sessions.create',[SEASON_TERM]) }}" class="btn --btn-small bg-primary fc-white" >Add Fee For Season Term</a>
        <a href="{{ route('manager.fee.sessions.create',[CAMP_WITHDAYS]) }}" class="btn --btn-small bg-primary fc-white" value="2">Add Fee For Camp</a>
      </div>
      <div class="col-md-6">
        <div class="default-form">
          <div class="control-group --search-group mb-0 float-right">
            <input type="text" placeholder="Search" class="search-field">
            <div class="search-button">
              <button type="submit" class="form-button">
              <i class="xxicon icon-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <tr>
        <th><center>S NO</center></th>
        <th><center>Term Name</center></th>
        <th><center>Categories</center></th>
        <th><center>Location</center></th>
        <th><center>Action</center></th>
      </tr>
      <tr>
        @foreach ($fees as $key=>$fee)
        <tr>
          
          <td>
            <center>{{$fees->firstItem() + $key }}</center>
          </td>
          <td>
            <center>{{ $fee->term->name }}</center>
          </td>
          <td>
            @if(!empty($fee->fee_details))
            @php $details=json_decode($fee->fee_details,true); @endphp
            
            @php $categories=array();@endphp
            @foreach($details as $key => $detail)
            @if(!in_array($detail['category_id'],$categories))
            <center>Category:{{$fee->getCategory($detail['category_id'])}}</center><br>
            @php $categories[]=$detail['category_id']; @endphp
            @endif
            @endforeach
            
            @endif
            
          </td>
          <td><center>
            {{$fee->location->name}}
            </center>
          </td>
          
          <td>
            <center>
            <a href="{{ route('manager.fee.sessions.edit',[$fee->id,$fee->term_id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white btn-sm">Edit Fees
            </button></a>
            <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $fee->id }}" id="deleteItem">Delete</button>
            </center>
          </td>
        </tr>
        @endforeach
      </table>
      <ul class="pagination">
        <li>
          {{$fees->links()}}
        </li>
      </div>
    </section>
    {!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
    {!! Form::close() !!}
    <script>
    var deleteResourceUrl = '{{ url()->current() }}';
    $(document).ready(function () {
    $('.deleteItem').click(function (e) {
    e.preventDefault();
    var deleteId = parseInt($(this).data('deleteId'));
    $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
    if (confirm('Are you sure?')) {
    $('#deleteForm').submit();
    }
    });
    });
    </script>
    @endsection