@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Add Training Session</h3>
<section class="box">
	{!! Form::open(['route' =>  ['manager.season.term.traning.sessions.store', $term], 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
	<div class="box-body">
		<h4 class="pagetitle">Select Category</h4>
		@foreach($categories as $category)
		<input type="checkbox" id="category" name="categories[]" value="{{$category->id}}">
		<label for="category">{{$category->name}}</label>
		@endforeach
		<br>
		<h4 class="pagetitle">Select Location</h4>
		@foreach($locations as $location)
		<input type="checkbox" id="location" name="locations[]" value="{{$location->id}}">
		<label for="location">{{$location->name}}</label>
		@endforeach
		<br>
		<h4 class="pagetitle">Select Days</h4>
		@foreach($days as $day)
		<input type="checkbox" id="day" name="days[]" value="{{$day->id}}">
		<label for="day">{{$day->name}}</label>
		@endforeach
		@include('manager.training_session._form',['submitBtnText' => 'Add Training Session'])
		{!! Form::close() !!}
	</div>
</section>
@endsection