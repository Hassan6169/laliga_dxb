@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Edit Training Session</h3>
<section class="box">
	<div class="box-body">
		
		{!! Form::model($session, ['route' => ['manager.traning.sessions.update', $session->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('manager.training_session._form',['submitBtnText' => 'Update'])
		{!! Form::close() !!}
	</div>
</section>
@endsection