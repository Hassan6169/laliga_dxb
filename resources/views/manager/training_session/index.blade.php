@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Training Session</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-6">
      </div>
      <div class="col-md-6">
        <div class="default-form">
          <div class="control-group --search-group mb-0 float-right">
            <input type="text" placeholder="Search" class="search-field">
            <div class="search-button">
              <button type="submit" class="form-button">
              <i class="xxicon icon-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <thead>
        <tr>
          <th><center>S NO</center></th>
          <th><center>Term Name</center></th>
          <th><center>Action</center></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($terms as $key =>$term)
        <tr>
          <td><center>{{ $terms->firstItem() + $key  }}</center></td>
          <td><center>{{ $term->name}}</center></td>
          <td><center><a href="{{ route('manager.training.sessions.edit',[$term->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Add Timings</button></a>
        </tr>
        @endforeach
      </table>
      <ul class="pagination">
        {{$terms->links()}}
      </ul>
    </div>
  </section>
  <!-- /.content-wrapper -->
  {!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
  {!! Form::close() !!}
  <script>
  var deleteResourceUrl = '{{ url()->current() }}';
  $(document).ready(function () {
  $('.deleteItem').click(function (e) {
  e.preventDefault();
  var deleteId = parseInt($(this).data('deleteId'));
  $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
  if (confirm('Are you sure?')) {
  $('#deleteForm').submit();
  }
  });
  });
  </script>
  @endsection