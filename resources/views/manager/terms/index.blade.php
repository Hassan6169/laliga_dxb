@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Terms</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row align-items-center">
            <div class="col-8">
                <a href="{{route('manager.season.terms.create',[$season])}}" class="btn --btn-small --btn-primary mb-0">Add Term</a>
            </div>
            
            <div class="col-4">
                <a href="{{route('manager.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
        </div>
    </div>
    <div class="box-body">
        <table class="table --bordered --hover">
            <tr>
                <th><center>S NO</center></th>
                <th>Name</th>
                <th><center>Type</center></th>
                <th><center>Start Date</center></th>
                <th><center>End Date</center></th>
                <th><center>Is Active</center></th>
                <th><center>Action</center></th>
            </tr>
            
            @foreach ($terms as $key=>$term)
            <tr>
                <td>
                    <center>{{ $terms->firstItem() + $key }}</center>
                </td>
                <td>
                    {{ $term->name }}
                </td>
                <td>
                    <center>
                    @foreach($types as $key => $type)
                    @if($term->type_id==$key)
                    <center>{{ $type }}</center>
                    @endif
                    @endforeach
                    </center>
                </td>
                <td>
                    <center>@if(!empty($term->start_date)){{ date('Y-m-d',strtotime($term->start_date))}}@endif</center>
                </td>
                <td>
                    <center>@if(!empty($term->end_date)){{ date('Y-m-d',strtotime($term->end_date))}}@endif</center>
                </td>
                <td>
                    <center>
                    @if($term->is_active)
                    <p>Active</p>
                    @else
                    <p>InActive</p>
                    @endif
                    </center>
                </td>
                <td>
                    <center>
                    <a href="{{ route('manager.season.terms.edit',[$season,$term->id]) }}" class="btn --btn-small bg-secondary fc-white btn-sm">Edit</a>
                    <a href="{{ route('manager.season.terms.break',[$season,$term->id]) }}" class="btn --btn-small bg-primary fc-white btn-sm">Add Term break</a>
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white btn-sm" data-delete-id="{{ $term->id }}" id="deleteItem">Delete </button>
                    </center>
                </td>
            </tr>
            @endforeach
        </table>
        
        <div class="">
            {{$terms->links()}}
        </div>
    </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection