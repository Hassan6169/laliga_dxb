@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Add Term</h3>
<section class="box">
	{!! Form::open(['route' => 'manager.season.terms.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
	@include('manager.terms._form',['submitBtnText' => 'Add Terms'])
	{!! Form::close() !!}
</div>
</section>
@endsection