@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add Emirate</h3>
<section class="box">
	<div class="box-body">
		{!! Form::open(['route' => 'admin.emirates.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('admin.emirates._form',['submitBtnText' => 'Add Emirate'])
		{!! Form::close() !!}
	</div>
</section>
@endsection