@extends('layouts.admin')

@section('content')

<h3 class="pagetitle">Edit Emirate</h3>

<section class="box">

  <div class="box-body">

    {!! Form::model($emirate, ['route' => ['admin.emirates.update', $emirate->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('admin.emirates._form',['submitBtnText' => 'Update'])

    {!! Form::close() !!}

  </div>

</section>

@endsection