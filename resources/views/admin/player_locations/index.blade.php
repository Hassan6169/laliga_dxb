@php
if(!empty($sessions)){
foreach($sessions as $session){
foreach($session->players as $player)
{
$playerdata[]=$player->pivot->training_session_id.'-'.$player->pivot->day_id.'-'.$player->pivot->location_id.'-'.$player->pivot->weeks;
}
}
}

@endphp
@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Player: {{$player->name}}</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn --btn-small bg-secondary fc-white mb-0">Back</a>
        </div>
    </div>
    <div class="box-body">
        {{ Form::open(['route'=>['admin.player.training.session.update',$player->id,$term,$emirate]]) }}
        @if($term->type_id==CAMP_WITHDAYS)
        <table class="table --bordered --hover">
            <tr>
                <th>Location</th>
                @foreach($days as $day)
                <th>{{$day->name}}</th>
                @endforeach
                <th>Full Week</th>
            </tr>
            @for($i=1; $i<=$term->weeks;$i++)
            @foreach($locations as $location)
            <tr>
                <td>
                    {{$location->name}}
                </td>
                @foreach($days as $day)
                <td>
                    @if(!empty($sessions))

                    @foreach($sessions as $session)
                    @if($session->day_id==$day->id && $session->location_id==$location->id)
                    @php $data=$session->id.'-'.$day->id.'-'.$location->id.'-'.$i; @endphp
                    <center><input type="checkbox" name="days[]" class="day-{{$i}}" 
                        value="{{$session->id}}-{{$day->id}}-{{$location->id}}-{{$i}}" {{!empty($playerdata) && in_array($data,$playerdata) ? 'checked':''}}></center>
                    <br>
                    <center>{{date('H:i',strtotime($session->start_time))}}-{{date('H:i',strtotime($session->end_time))}}<br>
                    </center>

                    @endif
                    @endforeach
                    @endif

                </td>
                @endforeach
                <td>
                   <input type="hidden" name="data[]" value="{{$session->id}}-{{$location->id}}-{{$i}}">
                  <center><input type="checkbox" name="is_full_week[]" class="full" data-id={{$i}}></center> 
                </td>
            </tr>
            @endforeach
            @endfor
        </table>
        @elseif($term->type_id==SEASON_TERM)
        @php $weeks=0; @endphp
        <table class="table --bordered --hover">
            <tr>
                <th>Location</th>
                @foreach($days as $day)
                <th>{{$day->name}}</th>
                @endforeach
            </tr>
            
            @foreach($locations as $location)
            <tr>
                <td>
                    {{$location->name}}
                </td>
                @foreach($days as $day)
                <td>
                    @if(!empty($sessions))
                    @foreach($sessions as $session)
                    @if($session->day_id==$day->id && $session->location_id==$location->id)
                    @php $data=$session->id.'-'.$day->id.'-'.$location->id.'-'.$weeks; @endphp
                    <input type="checkbox" name="days[]" value="{{$session->id}}-{{$day->id}}-{{$location->id}}-{{$weeks}}" {{!empty($playerdata) && in_array($data,$playerdata) ? 'checked':''}}>
                    
                    {{date('H:i',strtotime($session->start_time))}}-{{date('H:i',strtotime($session->end_time))}}
                    @endif
                    @endforeach
                    @endif
                </td>
                @endforeach
            </tr>
            @endforeach
        </table>
        @endif
        <button type="submit" class="btn --btn-small bg-secondary fc-white">Submit</button>
        {!! Form::close() !!}
    </div>
</section>
<script>
$('.full').click(function () {

var $this = $(this);
if ($this.is(':checked')) {
 var week=$(this).attr("data-id");
 $(".day-"+week).attr("checked",true);
}
else{
  $(':checkbox').prop('checked', false).removeAttr('checked');
    
}


});    
</script>

@endsection