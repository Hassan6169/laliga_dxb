@extends('layouts.admin')

@section('content')

<h3 class="pagetitle">Add Season</h3>

<section class="box">

<div class="box-body">

    {!! Form::open(['route' => 'admin.seasons.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('admin.seasons._form',['submitBtnText' => 'Add Season'])

    {!! Form::close() !!}

  </div>

</section>

@endsection