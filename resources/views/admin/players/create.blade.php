@extends('layouts.admin')

@section('content')

<h3 class="pagetitle">Add Player</h3>

<section class="box">

  <div class="box-body">

    <!-- /.box-header -->

    {!! Form::open(['route' => 'admin.players.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('admin.players._form',['submitBtnText' => 'Add Player'])

    {!! Form::close() !!}

  </div>

</section>

@endsection