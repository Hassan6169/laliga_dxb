<div class="control-group{{ $errors->has('status') ? ' has-error' : '' }}">
    {!! Form::label('status', 'Select Status', ['class'=>'form-label']) !!}
    {{ Form::select('status', $types,2, ['class' => 'form-field', 'placeholder' => 'Select Status','required' => 'required','id'=>'status']) }}
    @if ($errors->has('status'))
    <span class="error-msg">
        <strong>{{ $errors->first('status') }}</strong>
    </span>
    @endif
</div>
@if(empty($wallet->id))
<div class="control-group{{ $errors->has('amount_type') ? ' has-error' : '' }}">
    {!! Form::label('amount_type', 'Select Amount Type', ['class'=>'form-label']) !!}
    {{ Form::select('amount_type', $amount_type, null, ['class' => 'form-field', 'placeholder' => 'Select Amount Type','required' => 'required','id'=>'amount_type']) }}
    @if ($errors->has('amount_type'))
    <span class="error-msg">
        <strong>{{ $errors->first('amount_type') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('amount') ? ' has-error' : '' }}">
    {!! Form::label('amount', 'Amount', ['class'=>'form-label']) !!}
    {!! Form::text('amount','', ['class'=>'form-field','placeholder'=>'Amount']) !!}
    @if ($errors->has('amount'))
    <span class="error-msg">
        <strong>{{ $errors->first('amount') }}</strong>
    </span>
    @endif
</div>
@endif
<div class="control-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
    {!! Form::label('remarks', 'Remarks', ['class'=>'form-label']) !!}
    {!! Form::textarea('remarks',null, ['class'=>'form-field','placeholder'=>'Remarks','required' => 'required']) !!}
    @if ($errors->has('remarks'))
    <span class="error-msg">
        <strong>{{ $errors->first('remarks') }}</strong>
    </span>
    @endif
</div>
<input type="hidden" name="user_id" value="{{$parent}}">
<div class="control-group mb-0">
    <button type="submit" class="btn --btn-small bg-secondary fc-white" id="submit">{{ $submitBtnText }}</button>
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
    
</div>

<script>
$('#status').on('change', function (e) {

    var optionSelected = $("#status").val();
   
    if(optionSelected== 3 || optionSelected== 4){
        
       $('#amount_type').val('1');
    }
   
});
</script>