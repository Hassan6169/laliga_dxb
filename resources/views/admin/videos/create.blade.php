@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add Age Category</h3>
<section class="box">
  <div class="box-body">
    {!! Form::open(['route' => 'admin.age.categories.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"form-horizontal"]) !!}
    @include('admin.age_category._form',['submitBtnText' => 'Add Category'])
    {!! Form::close() !!}
  </div>
</section>
@endsection