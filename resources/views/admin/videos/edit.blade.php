@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit Age Category</h3>
<section class="box">
  <div class="box-body">
    {!! Form::model($category, ['route' => ['admin.age.categories.update', $category->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"form-horizontal"]) !!}
    @include('admin.age_category._form',['submitBtnText' => 'Update'])
    {!! Form::close() !!}
  </div>
</section>
@endsection