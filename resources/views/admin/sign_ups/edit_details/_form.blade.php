@inject('arrays','App\Http\Utilities\Arrays')
<div class="box-body">
    <div class="control-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
        {!! Form::label('status_id','Status', ['class' => 'form-label']) !!}
             {!! Form::select('status_id',$status,null,['class' => 'form-field']); !!}
        @if ($errors->has('status_id'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('status_id') }}</strong>
        </span>
        @endif
    </div>
      <div class="control-group{{ $errors->has('source_id') ? ' has-error' : '' }}">
        {!! Form::label('source_id','Sources', ['class' => 'form-label']) !!}
             {!! Form::select('source_id',$sources,null,['class' => 'form-field']); !!}
        @if ($errors->has('source_id'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('source_id') }}</strong>
        </span>
        @endif
    </div>
     <div class="control-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
        {!! Form::label('user_id', 'Team Member', ['class' => 'form-label']) !!}

        {!! Form::select('user_id',$users,null, ['class'=>'form-field','placeholder' => 'Select', ]) !!}

        @if ($errors->has('user_id'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('user_id') }}</strong>
        </span>
        @endif
    </div>
    <div class="control-group{{ $errors->has('comments') ? ' has-error' : '' }}">
    
    {!! Form::label('comments', 'Admin Comments', ['class'=>'form-label',]) !!}
    {!! Form::textarea('comment', null, ['class'=>'form-field']) !!}
    @if ($errors->has('comments'))
    <span class="error-msg">
        <strong>{{ $errors->first('comments') }}</strong>
    </span>
    @endif
</div>
   <div class="control-group">
    
    @if(!empty($signUp->comments))
    @php $comments=json_decode($signUp->comments,true); @endphp
    @foreach($comments as $key => $comment)
    @if($comment['id']==$signUp->id)
    {{ $comment['comments']}}
    <b>Posted By:</b>{{ $comment['admin_name']}}<br>
    <b>Updated At:</b>{{$comment['posted_on']}}<br>
    @endif
    @endforeach
    
    @endif
    </div>
    
    <div class="control-group mb-0">
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white" id="submit">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
    </div>
</div>
