@extends('layouts.admin')
@section('content')
<!---<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>--->
<h3 class="pagetitle">Training Session</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-8">
       {!! Form::open(['route' => ['admin.training.sessions.index'], 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form",'id' => 'data']) !!}
    
      <div class="row">
      <div class="col-md-6">
        <div class="control-group">
          
          {!! Form::label('season_id', 'Select Season', ['class' => 'form-label']) !!}
          {!! Form::select('season_id',$seasons,$request->season_id ?? null, ['class'=>'form-field','placeholder' => 'Select Season','id' => 'season']) !!}
    
          
         </div> 
        </div>
        <div class="col-md-6">
        <div class="control-group">
          
          {!! Form::label('term_id', 'Select Term', ['class' => 'form-label']) !!}
          {!! Form::select('term_id',$search_terms,$request->term_id ?? null, ['class'=>'form-field']) !!}
       </div>
     </div>
   
    </div>
    <button type="submit" class="btn --btn-small --btn-primary fc-white" name="is_submit" value="1">Search</button>
    
    <a href="{{route('admin.training.sessions.index')}}"><button type="button" class="btn --btn-small bg-danger fc-white clear">Clear</button></a>
    {!! Form::close() !!}
      
      </div>
      <div class="col-4">
         <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
      </div>
    </div>
  </div>
  <div class="box-body">
     
    <table class="table --bordered --hover">
      <thead>
        <tr>
          <th><center>S NO</center></th>
          <th><center>Term Name</center></th>
          <th><center>Action</center></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($terms as $key =>$term)
        <tr>
          <td><center>{{ $terms->firstItem() + $key  }}</center></td>
          <td><center>{{ $term->season->name}}-{{ $term->name}}</center></td>
          <td><center><a href="{{ route('admin.training.sessions.edit',[$term->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Add Timings</button></a>
        </tr>
        @endforeach
      </table>
      <ul class="pagination">
        {{$terms->links()}}
      </ul>
    </div>
  </section>
  <!-- /.content-wrapper -->
  {!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
  {!! Form::close() !!}
  <script>
  var deleteResourceUrl = '{{ url()->current() }}';
  $(document).ready(function () {
  $('.deleteItem').click(function (e) {
  e.preventDefault();
  var deleteId = parseInt($(this).data('deleteId'));
  $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
  if (confirm('Are you sure?')) {
  $('#deleteForm').submit();
  }
  });
  });
  </script>
   <script type="text/javascript">
    $(document).ready(function(){
    
    $('#season').on('change',function(){
    
    $('#data').submit();
    });
    });
    </script>
  @endsection