@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit Gallery</h3>
<section class="box">
  <div class="box-body">
    {!! Form::model($gallery, ['route' => ['admin.galleries.update', $gallery->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.galleries._form',['submitBtnText' => 'Update'])
    {!! Form::close() !!}
  </div>
</section>
@endsection