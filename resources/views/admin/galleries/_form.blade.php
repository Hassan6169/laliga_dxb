<div class="box-body">

    <div class="control-group{{ $errors->has('type') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('type','Type', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::select('type',$type,null,['class' => 'form-field']); !!}
           </div>
            @if ($errors->has('type'))
                <span class="error-msg">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
        </div>
   

    <div class="control-group{{ $errors->has('emirate_id') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('emirate_id','Emirates', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::select('emirate_id',$emirates,null,['class'=>'form-field']); !!}
           </div>
            @if ($errors->has('emirate_id'))
                <span class="error-msg">
                    <strong>{{ $errors->first('emirate_id') }}</strong>
                </span>
            @endif
        </div>
 
    <div class="control-group{{ $errors->has('gallery_category_id') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('gallery_category_id','Gallery Categories', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::select('gallery_category_id',$gallery_categories,null,['class' => 'form-field']); !!}
           </div>
            @if ($errors->has('gallery_category_id'))
                <span class="error-msg">
                    <strong>{{ $errors->first('gallery_category_id') }}</strong>
                </span>
            @endif
        </div>
 
    <div class="control-group{{ $errors->has('name_eng') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('name_eng', 'Name(Eng)', ['class'=>'control-label',]) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::text('name_eng', null, ['class'=>'form-field','maxlength'=>'100','required' => 'required']) !!}
           </div>
            @if ($errors->has('name_eng'))
                <span class="error-msg">
                    <strong>{{ $errors->first('name_eng') }}</strong>
                </span>
            @endif
        </div>
 
    <div class="control-group{{ $errors->has('video_url') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('video_url', 'Video Url', ['class'=>'control-label',]) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::text('video_url', null, ['class'=>'form-field','required' => 'required']) !!}
           </div>
            @if ($errors->has('video_url'))
                <span class="error-msg">
                    <strong>{{ $errors->first('video_url') }}</strong>
                </span>
            @endif
        </div>
  
    <div class="control-group{{ $errors->has('name_ar') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('name_ar', 'Title(Arabic)', ['class'=>'control-label',]) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::text('name_ar', null, ['class'=>'form-field','maxlength'=>'100','required' => 'required']) !!}
           </div>
            @if ($errors->has('name_ar'))
                <span class="error-msg">
                    <strong>{{ $errors->first('name_ar') }}</strong>
                </span>
            @endif
        </div>
 
     <div>
         @if(!empty($gallery->image))
         <img src="{{ asset('storage/galleries/'.$gallery->image) }}" style="width:400px;height:200px;">
         @endif
     </div>
  
    <div class="control-group{{ $errors->has('image') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('image', 'Thumb Image', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::file('image', null, ['class'=>'form-field','required' => 'required']) !!}
           </div>
            @if ($errors->has('image'))
                <span class="error-msg">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
            @endif
        </div>
  
        <div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
             <div class="col-sm-3">
                {!! Form::checkbox('is_active', 1, isset($news) ? $news->is_active : true) !!}
                {!! Form::label('is_active', 'Activate', ['class' => 'control-label']) !!}
                @if ($errors->has('is_active'))
                    <span class="error-msg">
                        <strong>{{ $errors->first('is_active') }}</strong>
                    </span>
                @endif
            </div>
        </div>
  
    <center>
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
       
   
</div>
</div>
