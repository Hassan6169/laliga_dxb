@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Fees</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-8">
        
        <a href="{{ route('admin.fee.sessions.create',[SEASON_TERM]) }}" class="btn --btn-small bg-primary fc-white" >Add Fee For Season Term</a>
        <a href="{{ route('admin.fee.sessions.create',[CAMP_WITHDAYS]) }}" class="btn --btn-small bg-primary fc-white" value="2">Add Fee For Camp</a>
        <a href="{{ route('admin.fee.sessions.create',[CAMP_WITHOUTDAYS]) }}" class="btn --btn-small bg-primary fc-white" value="2">Add Fee For Camp Without Days</a>
      </div>
      <div class="col-md-4">
      <a href="{{ url()->previous() }}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
      </div>
    </div>
  </div>
  <div class="box-body">
     {!! Form::open(['route' => 'admin.fee.sessions.index', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    <div class="row">
       <div class="col-md-3">
            <div class="control-group">
                    <select class="form-field" name="category_id">
                        <option value="">Select Age Category</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}" @if(!empty($category_id) && $category_id == $category->id) selected="selected" @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>

       </div>
       <div class="col-md-3">
            <div class="control-group">
                    <select class="form-field" name="term_id">
                        <option value="">Terms</option>
                        @foreach($terms as $term)
                        <option value="{{$term->id}}" @if(!empty($term_id) && $term_id == $term->id) selected="selected" @endif>{{$term->name}}</option>
                        @endforeach
                    </select>
                </div>

       </div>
       <div class="col-md-3">
            <div class="control-group">
                    <select class="form-field" name="location_id">
                        <option value="">Select Location</option>
                        @foreach($locations as $location)
                        <option value="{{$location->id}}" @if(!empty($location_id) && $location_id == $location->id) selected="selected" @endif>{{$location->name}}</option>
                        @endforeach
                    </select>
                </div>

       </div>

    </div>
      <button type="submit" name="is_submit" value="1" class="btn --btn-small --btn-primary fc-white">Search</button>
        <a href="{{route('admin.fee.sessions.index')}}"><button type="button" class="btn --btn-small bg-danger fc-white clear">Clear</button></a>
        {!! Form::close() !!}
    <table class="table --bordered --hover">
      <tr>
        <th><center>S NO</center></th>
        <th><center>Term Name</center></th>
        <th><center>Categories</center></th>
        <th><center>Location</center></th>
        <th><center>Action</center></th>
      </tr>
      <tr>
        @foreach ($fees as $key=>$fee)
        <tr>
          
          <td>
            <center>{{$fees->firstItem() + $key }}</center>
          </td>
          <td>
            <center>{{ $fee->term->name }}</center>
          </td>
          <td>
            @if(!empty($fee->fee_details))
            @php $details=json_decode($fee->fee_details,true); @endphp
            
            @php $categories=array();@endphp
            @foreach($details as $key => $detail)
            @if(!in_array($detail['category_id'],$categories))
            <center>Category:{{$fee->getCategory($detail['category_id'])}}</center><br>
            @php $categories[]=$detail['category_id']; @endphp
            @endif
            @endforeach
            
            @endif
            
          </td>
          <td><center>
           @if(!empty($fee->location->name)) 
           {{$fee->location->name}}
           @endif
            </center>
          </td>
          
          <td>
            <center>
            <a href="{{ route('admin.fee.sessions.edit',[$fee->id,$fee->term_id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white btn-sm" style="width: 150px;">Edit Fees
            </button></a>
            <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $fee->id }}" id="deleteItem" style="width: 150px;">Delete</button>
            </center>
          </td>
        </tr>
        @endforeach
      </table>
      <ul class="pagination">
        <li>
          {{$fees->links()}}
        </li>
      </div>
    </section>
    {!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
    {!! Form::close() !!}
    <script>
    var deleteResourceUrl = '{{ url()->current() }}';
    $(document).ready(function () {
    $('.deleteItem').click(function (e) {
    e.preventDefault();
    var deleteId = parseInt($(this).data('deleteId'));
    $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
    if (confirm('Are you sure?')) {
    $('#deleteForm').submit();
    }
    });
    });
    </script>
    @endsection