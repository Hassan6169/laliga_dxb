@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Discount</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-8">
            <a href="{{route('admin.discounts.create')}}">
                <button type="button" class="btn --btn-small --btn-primary mt-10">Add Discount</button>
            </a>
        </div>
        <div class="col-4">
                <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
    </div>
</div>
    <div class="box-body">
        <table class="table --bordered --hover">
            <tr>
                <th>S NO</th>
                <th>Name</th>
                <th>Amount</th>
                <th>Type</th>
                <th>Category</th>
                <th>Is Active</th>
                <th>Action</th>
            </tr>
            
            @foreach ($discounts as $key=>$discount)
            <tr>
                <td>{{ $discounts->firstItem() + $key }}</td>
                <td>{{ $discount->name }}</td>
                <td>{{ $discount->amount }}</td>
                <td>
                @foreach($types as $key=> $type)
                 
                    @if($key==$discount->type)
                    <p>{{$type}}</p>
                    @endif
                    @endforeach
                </td>
                <td>
                @foreach($categories as $key=> $category)
                 
                    @if($key==$discount->category)
                    <p>{{$category}}</p>
                    @endif
                    @endforeach
                </td>
                <td>
                    @if($discount->is_active)
                    {{"Active"}}
                    @else
                    {{"In Active"}}
                    @endif
                </td>
                
                <td><center>
                    <a href="{{ route('admin.discounts.edit',[$discount->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Edit</button></a>
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $discount->id }}" id="deleteItem">Delete</button></center>
                </td>
            </tr>
            @endforeach
        </table>
     <div class="">
    {{$discounts->links()}}
    </div>
    </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';

$(document).ready(function () {

$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection