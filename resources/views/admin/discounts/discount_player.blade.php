  @php 
  if(count($player_discounts)>0){
  foreach($player_discounts as $discount)
  {
  $discountdata[]=$discount->id;
  }
}
@endphp
@extends('layouts.admin')
@section('content')

<h3 class="pagetitle"> Discount For :{{$player->name}}</h3>
<h3 class="pagetitle"> Term :{{$term->name}}</h3>

@include('includes.message')

<section class="box">

    <div class="box-header">
        <div class="row align-items-center">
            <div class="col-8">
                <h4 class="box-title">
                    Discount
                </h4>
            </div>
            
            <div class="col-4">
                <a href="{{ url()->previous() }}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
        </div>
    </div>

    <div class="box-body">
        {{ Form::open(['route'=>['admin.players.discount.update',$player->id,$term], 'class'=>"default-form"]) }}
        
        @foreach($discounts as $discount)

        <div class="control-group">
            <div class="checkbox">
                <input type="checkbox" name="discounts[]" value="{{$discount->id}}" {{ isset($discountdata) && in_array($discount->id,$discountdata) ? 'checked':''}}>

                <span class="label"></span>
                <span class="text">
                    {{$discount->name}}
                </span>
            </div>
        </div>
            
        @endforeach

        <button type="submit" class="btn --btn-small bg-secondary fc-white" value="update" name="submit">Submit</button>
    
        <button type="submit" class="btn --btn-small bg-danger fc-white" value="delete" name="submit">Remove All</button>
        
        {!! Form::close() !!}
    </div>

</section>

@endsection