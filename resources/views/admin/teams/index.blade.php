@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Teams</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-md-8">
                <a href="{{route('admin.teams.create')}}" class="btn --btn-small --btn-primary mb-0">Add teams</a>
            </div>
            
            <div class="col-4">
                <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>

        </div>
    </div>

    <div class="box-body">
        <table class="table --bordered --hover" align="middle">
            <tr>
                <th>
                    <center>S NO</center>
                </th>
                <th>
                    <center>Name</center>
                </th>
                <th>
                    <center>Is Active</center>
                </th>
                <th>
                    <center>Action</center>
                </th>
            </tr>
            
            @foreach ($teams as $key=>$team)
            <tr>
                <td>
                    <center>{{$teams->firstItem() + $key }}</center>
                </td>
                <td>
                    <center>{{ $team->name }}</center>
                </td>
                <td>
                    <center>
                        @if($team->is_active)
                        <p>Active</p>
                        @else
                        <p>InActive</p>
                        @endif
                    </center>
                </td>
                <td>
                    <center>
                        <a href="{{ route('admin.teams.edit',[$team->id]) }}" class="btn --btn-small bg-secondary fc-white mb-0">Edit</a>
                        
                        <button type="button" class="deleteItem btn --btn-small bg-danger fc-white mb-0" data-delete-id="{{ $team->id }}" id="deleteItem">Delete</button>
                    </center>
                </td>
            </tr>
            @endforeach
        
        </table>
        
        <div class="">
            {{$teams->links()}}
        </div>
    </div>

</section>

{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}

<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {

$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection