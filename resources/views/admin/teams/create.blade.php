@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add Team</h3>
<section class="box">
	<div class="box-body">
		{!! Form::open(['route' => 'admin.teams.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('admin.teams._form',['submitBtnText' => 'Add Team'])
		{!! Form::close() !!}
	</div>
</section>
@endsection