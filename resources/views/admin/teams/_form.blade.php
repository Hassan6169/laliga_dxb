<div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Name', ['class'=>'form-label',]) !!}
    {!! Form::text('name', null, ['class'=>'form-field','required' => 'required']) !!}
    @if ($errors->has('name'))
    <span class="error-msg">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
    @endif
</div>
<div class="control-group">
    
    {!! Form::label('category_id', 'Select category', ['class' => 'form-label']) !!}
    {!! Form::select('category_id',$categories,null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select category']) !!}
    
</div>
<div class="control-group">
    {!! Form::label('user_id', 'Select Coach', ['class' => 'form-label']) !!}
    {!! Form::select('user_id',$users,null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select Coach']) !!}
    
</div>
<div class="control-group">
    {!! Form::label('term_id', 'Select Term', ['class' => 'form-label']) !!}
    {!! Form::select('term_id',$terms,null, ['class'=>'form-field','required' => 'required','placeholder' => 'Select Term']) !!}
</div>
<div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
    <div class="checkbox">
        {!! Form::checkbox('is_active', 1, isset($team) ? $team->is_active : true) !!}
        <span class="label"></span>
        <span class="text">
            {!! Form::label('is_active', 'Activate', ['class' => 'form-label']) !!}
        </span>
    </div>
    @if ($errors->has('is_active'))
    <span class="error-msg">
        <strong>{{ $errors->first('is_active') }}</strong>
    </span>
    @endif
</div>
<div class="control-group mb-0">
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
</div>