@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Installments
@if(!empty($invoice->total_amount))
Total Amount:{{$invoice->total_amount}}
@else
Total Amount:{{$invoice->amount}}
@endif
</h3>
<section class="box">
	@include('includes.message')
	<div class="box-body">
		{!! Form::model($invoice, ['route' => ['admin.invoices.installments.update', $invoice->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		<div class="row">
		@foreach($installments as $installment)
	
					<div class="col-sm-6">
						<div class="control-group">
						{!! Form::text('installments[]',$installment->amount, ['class'=>'form-field','required' => 'required']) !!}
					</div>
					</div>
					<div class="col-sm-6">
						<div class="control-group">
						{!! Form::select('status[]',$paymentStatus,$installment->status, ['class'=>'form-field','required' => 'required']) !!}
					</div>
					</div>
				
				@if ($errors->has('installments'))
				<span class="error-msg">
					<strong>{{ $errors->first('installments') }}</strong>
				</span>
				@endif
			
		@endforeach
		</div>
		<div class="form-group">
			<center>
			<input type="submit" value="Update" class="btn --btn-small bg-secondary fc-white">
			<a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
			
		</div>
	</div>
	{!! Form::close() !!}
</div>
</section>
@endsection