@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Invoice</h3>
@include('includes.message')
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<section class="box">
  <div class="box-header">
    <h4 class="box-title">
    Invoice Details
    </h4>
  </div>
  <div class="box-body">
    {!! Form::open(['route' => 'admin.invoice.serach', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    <div class="row">
      <div class="col-sm-3">
        <div class="control-group">
          {!! Form::text('keyword', $keyword ?? null, ['class'=>'form-field','placeholder' => 'Search By Keyword']) !!}
        </div>
      </div>
      <div class="col-sm-3">
        <div class="control-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">LA</div>
            </div>
            <input type="text" name="invoice_id" class="form-control" id="inlineFormInputGroupUsername" placeholder="xxxxxxxx">
          </div>
          
        </div>
      </div>
      <div class="col-sm-3">
        <div class="control-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">Player No:PL-</div>
            </div>
            <input type="text" name="player_id" class="form-control" id="inlineFormInputGroupUsername" placeholder="xxxxxxxx">
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="control-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">Parent No:PL-</div>
            </div>
            <input type="text" name="parent_id" class="form-control" id="inlineFormInputGroupUsername" placeholder="xxxxxxxx">
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="control-group">
          <select class="form-field" name="payment_status">
            <option value="">Payment Status Selection</option>
            @foreach($paymentStatus1 as $key=>$status)
            <option value="{{$key}}">{{$status}}</option>
            @endforeach
            
          </select>
        </div>
      </div>
        {{-- <div class="control-group">
          {!! Form::select('show_all_inv_additional_inv', array(''=>'Show all Invoices(For Additional invoices)','1' => 'Not show additional invoices', '2' => 'Only Additional invoices'),$additional ?? null,['class' => 'form-field']); !!}
        </div> --}}
        <div class="col-md-3">
        <div class="control-group">
                    <select class="form-field" name="term_id">
                        <option value="">Training Term Selection</option>
                        @foreach($terms as $term)
                        <option value="{{$term->id}}" @if(!empty($term_id) && $term_id == $term->id) selected="selected" @endif>{{$term->name}}</option>
                        @endforeach
                    </select>
        </div> 
      </div>
      <div class="col-md-3">
        <div class="control-group">
          <select class="form-field" name="payment_method">
            <option value="">Payment Method Selection</option>
            @foreach($paymentMethods as $key=>$method)
            <option value="{{$key}}">{{$method}}</option>
            @endforeach
          </select>
        </div>
        
      </div>
      <div class="col-md-3">
        
        {{-- <div class="control-group">
          {!! Form::select('show_all_inv_for_custom_inv', array(''=>'Show all Invoices(For Custom invoices)','2' => 'Not show Custom invoices', '1' => 'Only Custom invoices'),$custom_invoice ?? null,['class' => 'form-field']); !!}
        </div> --}}
        <div class="control-group">
          <select class="form-field" name="inv_installment">
            <option value="">Installment Selection</option>
            <option value="2" type="checkbox">2 Times</option>
            <option value="3" type="checkbox">3 Payments</option>
            <option value="4" type="checkbox">4 Payments</option>
            <option value="5" type="checkbox">5 Payments</option>
            <option value="6" type="checkbox">6 Payments</option>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="control-group">
          {!! Form::text('amount_from', $player_name ?? null, ['class'=>'form-field','placeholder' => 'Amount From']) !!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="control-group">
          {!! Form::text('amount_upto', $player_name ?? null, ['class'=>'form-field','placeholder' => 'Amount Upto']) !!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="control-group">
          <select class="form-field" name="category_id">
            <option value="">Category Selection</option>
           
            @foreach($categories as $category)
            <option value="{{$category->id}}" @if(!empty($category_id) && $category_id == $category->id) selected="selected" @endif>{{$category->name}}</option>
            @endforeach
          </select>
          
        </div>
      </div>
      
      <div class="col-md-3">
        
        <div class="control-group">
          <select class="form-field" name="paid_installments">
            <option value="">Paid Installments</option>
            <option value="1" type="checkbox"   class="form-check-label" id="defaultCheck1">1 Installment</option>
            <option value="2" type="checkbox"class="form-check-label" id="defaultCheck1" >2 Installment</option>
            <option value="3" type="checkbox" class="form-check-label" id="defaultCheck1" >3 Installment</option>
            <option value="4" type="checkbox"  class="form-check-label"id="defaultCheck1" >4 Installment</option>
          </select>
        </div>
      </div>
      
      <div class="col-md-3">
        <div class="control-group">
          <select class="form-field" name="location_id">
            <option value="">Select Location</option>
            @foreach($locations as $key=>$location)
            <option value="{{$location->id}}">{{$location->name}}</option>
            @endforeach
          </select>
          
        </div>
      </div>
      <div class="col-md-3">
        <div class="control-group">
          <select class="form-field" name="pending_installments">
            <option value="">Pending Installments</option>
            <option value="1" type="checkbox"   class="form-check-label" id="defaultCheck1">1 Installment</option>
            <option value="2" type="checkbox"class="form-check-label" id="defaultCheck1" >2 Installment</option>
            <option value="3" type="checkbox" class="form-check-label" id="defaultCheck1" >3 Installment</option>
            <option value="4" type="checkbox"  class="form-check-label"id="defaultCheck1" >4 Installment</option>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="control-group">
          {!! Form::text('created_at', $created_at ?? null, ['class'=>'form-field','placeholder' => 'Payment Date From ','id'=>'payment_date_from','autocomplete'=>'off']) !!}
          </div>
        </div>
        <div class="col-sm-3">
          <div class="control-group">
          {!! Form::text('created_at2', $created_at2 ?? null, ['class'=>'form-field','placeholder' => 'Paymnet Date To','id'=>'payment_date_to','autocomplete'=>'off']) !!}
          </div>
        </div>
        <div class="col-md-3">
          <div class="control-group">
            {!! Form::select('refund_amount', array(''=>'Refund Amount','1' => 'yes', '2' => 'No'),$refund ?? null,['class' => 'form-field']); !!}
            
          </div>
      </div>
      <div class="col-md-3">
        <div class="control-group">
          {!! Form::select('write_off_amount', array(''=>'Write off Amount','1' => 'Yes', '2' => 'No'),$write_off_amount ?? null,['class' => 'form-field']); !!}
        </div>
      </div>
      <div class="col-sm-3">
        <div class="control-group">
          {!! Form::text('start_date', $start_date ?? null, ['class'=>'form-field','placeholder' => 'Invoice Date From','id'=>'start_date' ,'autocomplete'=>"off"]) !!}
        </div>
      </div>
      <div class="col-sm-3">
        <div class="control-group">
          {!! Form::text('end_date', $end_date ?? null, ['class'=>'form-field','placeholder' => 'Invoice Date To','id'=>'end_date' ,'autocomplete'=>"off"]) !!}
        </div>
      </div>
      
    </div>
    <button type="submit" class="btn --btn-small --btn-primary fc-white">Search</button>
    <a href="{{route('admin.invoices.index')}}"><button type="button" class="btn --btn-small bg-danger fc-white clear">Clear</button></a>
    {!! Form::close() !!}
    <table class="table --bordered --hover">
      <tr>
        <th>Invoice No</th>
        <th>Player Details</th>
        <th>Training Details</th>
        <th>Payment Details</th>
        <th>Action</th>
      </tr>
      
      @foreach ($invoices as $invoice)
      @php $details=json_decode($invoice->session_details,true);@endphp
      
      <tr>
        <td>
          <strong>LA-{{$invoice->invoice_no}}</strong>
        </td>
        
        <td>
          @if($invoice->player_id==null)
          @foreach($details as $key => $detail)
          @php $player=$invoice->getPlayer($detail['player_id']); @endphp
          <strong>PL: </strong>{{$player->id}}<br />
          <strong>Name: </strong> {{$player->name}}<br />
          @endforeach
          @else
          <strong>PL: </strong>{{$invoice->player->id}}<br />
          <strong>Name: </strong> {{$invoice->player->name}}<br />
          @endif
        </td>
        
        <td>
          @if($invoice->term_id==null)
          @foreach($details as $key => $detail)
          @php $term=$invoice->getTerm($detail['term_id']); @endphp
                    
          @if($invoice->term!=null)
          {{ $invoice->term }}
          @endif

          @endforeach
          @else
          
          @if($invoice->term!=null)
          {{ $invoice->term->name }} <br/>
          @endif
          {{-- {{ $invoice->term->name }}<br/> --}}
          @endif
        </td>
        
        <td>
          <strong>Kit Amount: </strong>{{$invoice->kit_amount}} <br />
          
          
          <strong>Tax: </strong>{{$invoice->tax_amount}}<br />
          @if(empty($invoice->total_amount))
          <strong>Total Amount: </strong>{{$invoice->amount}}<br />
          @else
          <strong>Total Amount: </strong>{{$invoice->total_amount}}<br />
          @endif
          @if($invoice->payment_status==STATUS_PAID)
          <strong>Status: </strong>PAID
          @elseif($invoice->payment_status==STATUS_READY_TO_PAY)
          <strong>Status: </strong>Ready to Pay
          @elseif($invoice->payment_status==STATUS_PENDING)
          <strong>Status: </strong>Pending
          @endif
        </td>
        
        <td>
          <a target="_blank" href="{{route('admin.player.invoices.show',[$invoice->id])}}">
            <button class="btn --btn-small bg-secondary fc-white btn-sm">Preview Invoice</button>
          </a>
          
          <br />
          @if($invoice->partial_payment_type==STATUS_PARTIAL_PAYMENT)
          <a href="{{route('admin.invoices.payments.installments',[$invoice->id])}}" class="btn --btn-small bg-warning fc-white btn-sm">Installments</a>
          <br />
          @endif
          <a href="{{route('admin.invoices.partial.refund.index',[$invoice->id])}}" class="btn --btn-small bg-secondary fc-white btn-sm">Partial Refund</a>
          
          <br />
          
          <a href="{{route('admin.invoices.write.off.index',[$invoice->id])}}" class="btn --btn-small bg-info fc-white btn-sm">Write Off</a>
          
          <br />
          <a href="{{route('admin.invoices.payments.method.edit',[$invoice->id])}}" class="btn --btn-small bg-success fc-white btn-sm">Edit Payment Method</a>
          <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $invoice->id }}" id="deleteItem">Delete</button>
        </td>
      </tr>
      @endforeach
    </table>
  </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
  <script>
    var dateToday = new Date();
    $( "#payment_date_from" ).datepicker({
   
    });

    var dateToday = new Date();
    $( "#payment_date_to" ).datepicker({
   
    });
   </script>
   <script>
    var dateToday = new Date();
    $( "#start_date" ).datepicker({
   
    });

    var dateToday = new Date();
    $( "#end_date" ).datepicker({
   
    });
   </script>
@endsection