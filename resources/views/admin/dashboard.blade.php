@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Dashboard</h3>
<section class="mb-30">
    <div class="row">
        
        <div class="col-md-3">
            <div class="box --info-box">
                <span class="box-icon bg-warning">
                    <i class="xxicon icon-user"></i>
                </span>
                <div class="box-content">
                    <span class="box-text">Players</span>
                    <span class="box-number fc-warning">{{$players}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box --info-box">
                <span class="box-icon bg-info">
                    <i class="xxicon icon-user"></i>
                </span>
                <div class="box-content">
                    <span class="box-text">Total Invoices</span>
                    <span class="box-number fc-info">{{$total_invoices}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box --info-box">
                <span class="box-icon bg-success">
                    <i class="xxicon icon-user"></i>
                </span>
                <div class="box-content">
                    <span class="box-text">Paid</span>
                    <span class="box-number fc-success">{{$paid}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box --info-box">
                <span class="box-icon bg-danger">
                    <i class="xxicon icon-user"></i>
                </span>
                <div class="box-content">
                    <span class="box-text">Not Paid</span>
                    <span class="box-number fc-danger">{{$not_paid}}</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="box">
    @foreach($seasons as $season)
    <div class="alert --warning" role="alert">
        <h3><center>{{$season->name}}</center></h3>
    </div>
    <div class="box-body">
        <table class="table --bordered --hover">
            <tr>
                <th>Term</th>
                <th>Paid</th>
                <th>Partially Payment</th>
                <th>Total</th>
            </tr>
            @foreach($season->terms as $term)
              @foreach($data as $key=> $value)
               @if($key==$term->id)
            <tr>
                <td>
                   <centre> {{ $term->name}}</centre>
                </td>
                
                <td>
                    <centre>{{$value['paid']}}</centre> 
                </td>
                <td>
                  <centre> {{$value['partial_payment']}}</centre>   
                </td>
                <td>
                  <centre>{{$value['total']}}</centre>  
                </td>
                
            </tr>
             @endif
             @endforeach
            @endforeach
        </table>
        
    </div>
    @endforeach
</section>
@endsection