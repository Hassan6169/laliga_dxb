<div class="box-body">
        <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <div class="col-sm-3">
                {!! Form::label('name', 'Name', ['class'=>'control-label',]) !!}
            </div>
            <div class="col-sm-9">
                {!! Form::text('name', null, ['class'=>'form-field','required' => 'required']) !!}
            
            @if ($errors->has('name'))
            <span class="error-msg">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
            </div>
        </div>
        <div class="control-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
            <div class="col-sm-3">
                {!! Form::label('start_date', 'Start Date', ['class'=>'control-label']) !!}
            </div>
            <div class="col-sm-9">
                <div class='input-group'>
                    {!! Form::text('start_date',null,['class'=>'form-field','required' => 'required','id'=>'start_date','autocomplete'=>'off']) !!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
           
            @if ($errors->has('start_date'))
            <span class="error-msg">
                <strong>{{ $errors->first('start_date') }}</strong>
            </span>
            @endif
            </div>
        </div>
        <div class="control-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
            <div class="col-sm-3">
                {!! Form::label('end_date', 'End Date', ['class'=>'control-label']) !!}
            </div>
            <div class="col-sm-9">
                <div class='input-group'>
                    {!! Form::text('end_date',null,['class'=>'form-field','required' => 'required','id'=>'end_date','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off']) !!}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        
        @if ($errors->has('end_date'))
        <span class="error-msg">
            <strong>{{ $errors->first('end_date') }}</strong>
        </span>
        @endif
        </div>
    </div>
    <div class="control-group{{ $errors->has('gender') ? ' has-error' : '' }}">
        <div class="col-sm-3">
            {!! Form::label('gender','Gender', ['class'=>'control-label']) !!}
        </div>
        <div class="col-sm-9">
            {!! Form::select('gender', array('0'=>'None','1' => 'Male', '2' => 'Female'),null,['class' => 'form-field']); !!}
        
        @if ($errors->has('gender'))
        <span class="error-msg">
            <strong>{{ $errors->first('gender') }}</strong>
        </span>
        @endif
        </div>
    </div>
    <div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
        <div class="col-sm-3">
            {!! Form::checkbox('is_active', 1, isset($category) ? $category->is_active : true) !!}
            {!! Form::label('is_active', 'Activate', ['class' => 'control-label']) !!}
            @if ($errors->has('is_active'))
            <span class="error-msg">
                <strong>{{ $errors->first('is_active') }}</strong>
            </span>
            @endif
        </div>
    </div>
<div class="control-group">
    <center>
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
    
</div>
</div>
</div>
<script>
var dateToday = new Date();
$( "#start_date" ).datepicker({

});
$( "#end_date" ).datepicker({

});
</script>