@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Age Categories</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-8">
        <a href="{{route('admin.age.categories.create')}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Add Category</button></a>
      </div>
      <div class="col-md-4">
        
      <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>

      </div>
    </div>
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <tr>
        <th><center>S NO</center></th>
        <th><center>Name</center></th>
        <th><center>Is Active</center></th>
        <th><center>Action</center></th>
      </tr>
      @foreach ($age_categories as $key=>$categories)
      <tr>
        <td><center>{{ $age_categories->firstItem() + $key }}</center></td>
        <td><center>{{ $categories->name }}</center></td>
        <td><center>
          @if($categories->is_active)
          <p>Active</p>
          @else
          <p>InActive</p>
        @endif</center></td>
        <td><center><a href="{{ route('admin.age.categories.edit',[$categories->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Edit</button></a>
      <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $categories->id }}" id="deleteItem">Delete</button></center></td>
    </tr>
    @endforeach
  </table>
  <ul class="pagination">
   {{$age_categories->links()}}
  </ul>
</div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {

$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection