@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add Term</h3>
<section class="box">
	{!! Form::open(['route' => 'admin.season.terms.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
	@include('admin.terms._form',['submitBtnText' => 'Add Terms'])
	{!! Form::close() !!}
</div>
</section>
@endsection