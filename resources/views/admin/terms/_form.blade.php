<div class="box-body">

    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Name', ['class'=>'form-label']) !!}

        {!! Form::text('name', null, ['class'=>'form-field','required' => 'required','maxlength'=>'100','placeholder'=>'Name']) !!}

        @if ($errors->has('name'))
        <span class="error-msg">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('start_date') ? ' has-error' : '' }}">

        {!! Form::label('start_date', 'Start Date', ['class'=>'form-label']) !!}

        <div class='input-group'>
            {!! Form::text('start_date',null,['class'=>'form-field','id'=>'start_date','autocomplete'=>'off']) !!}
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>

        @if ($errors->has('start_date'))
        <span class="error-msg">
            <strong>{{ $errors->first('start_date') }}</strong>
        </span>
        @endif

    </div>

    <div class="control-group{{ $errors->has('end_date') ? ' has-error' : '' }}">

        {!! Form::label('end_date', 'End Date', ['class'=>'form-label']) !!}

        <div class='input-group'>
            {!! Form::text('end_date',null,['class'=>'form-field','id'=>'end_date','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off']) !!}
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>

        @if ($errors->has('end_date'))
        <span class="error-msg">
            <strong>{{ $errors->first('end_date') }}</strong>
        </span>
        @endif
    </div>

     <div class="control-group{{ $errors->has('type_id') ? ' has-error' : '' }}">

        {!! Form::label('type', 'Select Type', ['class' => 'form-label']) !!}

        {!! Form::select('type_id',$types,null, ['class'=>'form-field','required' => 'required']) !!}

        @if ($errors->has('type_id'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('type_id') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('level') ? ' has-error' : '' }}">
        
        {!! Form::label('level', 'Select Level', ['class' => 'form-label']) !!}
        

        {!! Form::select('level',$levels,null, ['class'=>'form-field','required' => 'required']) !!}
         
        @if ($errors->has('level'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('level') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('kit_amount') ? ' has-error' : '' }}">

        {!! Form::label('kit_amount', 'Kit Amount', ['class'=>'form-label']) !!}

        {!! Form::text('kit_amount', null, ['class'=>'form-field','placeholder'=>'Kit Amount']) !!}

        @if ($errors->has('kit_amount'))
        <span class="error-msg">
            <strong>{{ $errors->first('kit_amount') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('total_sessions') ? ' has-error' : '' }}">

        {!! Form::label('total_sessions', 'Total Sessions', ['class'=>'form-label']) !!}

        {!! Form::text('total_sessions', null, ['class'=>'form-field','placeholder'=>'Total Sessions']) !!}

        @if ($errors->has('total_sessions'))
        <span class="error-msg">
            <strong>{{ $errors->first('total_sessions') }}</strong>
        </span>
        @endif
    </div>

      <div class="control-group{{ $errors->has('weeks') ? ' has-error' : '' }}">

        {!! Form::label('weeks', 'Weeks', ['class'=>'form-label']) !!}

        {!! Form::text('weeks', null, ['class'=>'form-field','placeholder'=>'Weeks']) !!}

        @if ($errors->has('weeks'))
        <span class="error-msg">
            <strong>{{ $errors->first('weeks') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('is_show_to_admin') ? ' has-error' : '' }}">

        <div class="checkbox">
            {!! Form::checkbox('is_show_to_admin', 1, isset($term) ? $term->is_show_to_admin : true) !!}

            <span class="label"></span>
            <span class="text">
                {!! Form::label('is_show_to_admin', 'Show to Admin', ['class' => 'form-label']) !!}
            </span>
        </div>

        @if ($errors->has('is_show_to_admin'))
        <span class="error-msg">
            <strong>{{ $errors->first('is_show_to_admin') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">

        <div class="checkbox">
            {!! Form::checkbox('is_active', 1, isset($term) ? $term->is_active : true) !!}

            <span class="label"></span>
            <span class="text">
                {!! Form::label('is_active', 'Activate', ['class' => 'form-label']) !!}
            </span>
        </div>

        @if ($errors->has('is_active'))
        <span class="error-msg">
            <strong>{{ $errors->first('is_active') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('yearly') ? ' has-error' : '' }}">

        <div class="checkbox">
            {!! Form::checkbox('yearly', 1, isset($term) ? $term->yearly : true) !!}

            <span class="label"></span>
            <span class="text">
                {!! Form::label('yearly', 'Yearly', ['class' => 'form-label']) !!}
            </span>
        </div>

        @if ($errors->has('yearly'))
        <span class="error-msg">
            <strong>{{ $errors->first('yearly') }}</strong>
        </span>
        @endif
    </div>

    {!! Form::hidden('season_id',$season, ['class'=>'form-field']) !!}
    <div class="control-group mb-0">

        <button type="submit" class="btn --btn-small bg-secondary fc-white" id="submit">{{ $submitBtnText }}</button>
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
    </div>

</div>

<script>
    var dateToday = new Date();
    
    $( "#start_date" ).datepicker({
        //minDate: dateToday,
    });

    $( "#end_date" ).datepicker({
        //minDate: dateToday,
    });
</script>