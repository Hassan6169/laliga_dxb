@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Term Break-{{$term->name}}</h3>
<section class="box">
	@include('includes.message')
	<div class="box-body">
		{!! Form::open(['route' => ['admin.season.term.break.store',[$season,$term->id]], 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		
		
		<div class="row">
			<div class="col-sm-6">
				<div class="control-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
					{!! Form::label('start_date', 'Start Date', ['class'=>'form-label']) !!}
					<div class='input-group'>
						{!! Form::text('start_date',null,['class'=>'form-field','autocomplete'=>'off']) !!}
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					@if ($errors->has('start_date'))
					<span class="error-msg">
						<strong>{{ $errors->first('start_date') }}</strong>
					</span>
					@endif
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="control-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
					{!! Form::label('end_date', 'End Date', ['class'=>'form-label']) !!}
					<div class='input-group'>
						{!! Form::text('end_date',null,['class'=>'form-field','autocomplete'=>'off']) !!}
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					@if ($errors->has('end_date'))
					<span class="error-msg">
						<strong>{{ $errors->first('end_date') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
	    <div class="form-group">
				<center>
				<input type="submit" value="Update" class="btn --btn-small bg-secondary fc-white">
				<a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
				
			</div>
		</div>
		<table class="table --bordered --hover">
      <tr>
        <th><center>S NO</center></th>
        <th><center>Term Name</center></th>
        <th><center>Start Date</center></th>
        <th><center>End Date</center></th>
        <th><center>Action</center></th>
      </tr>
      <tr>
        @foreach ($term_break as $key=>$break)
        <tr>
          
          <td>
            <center>{{$term_break->firstItem() + $key }}</center>
          </td>
          <td>
            <center>{{ $break->term->name }}</center>
          </td>
          <td>
            {{$break->start_date}}
            
          </td>
          <td><center>
             {{$break->end_date}}
            </center>
          </td>
          
          <td>
            <center>
            <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $break->id }}" id="deleteItem">Delete</button>
            </center>
          </td>
        </tr>
        @endforeach
      </table>
		{!! Form::close() !!}
	</div>
</section>
<script>
var dateToday = new Date();
$( ".form-field" ).datepicker({
       
    });
</script>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
    {!! Form::close() !!}
    <script>
    var deleteResourceUrl = '{{ url()->current() }}';
    $(document).ready(function () {
    $('.deleteItem').click(function (e) {
    e.preventDefault();
    var deleteId = parseInt($(this).data('deleteId'));
    $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
    if (confirm('Are you sure?')) {
    $('#deleteForm').submit();
    }
    });
    });
    </script>
@endsection