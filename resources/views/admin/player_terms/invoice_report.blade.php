<!doctype html>
<html class="no-js" lang="en">
<head>
    <style>
        body {
            border: 0px solid;
            margin: 0;
            padding: 25px;
            font-family: "Arial";
        }
        table {
            border-collapse: collapse;
        }
        .border td {
            border: 1px solid black;
            padding: 4px;
        }
        .details .rowborder {
            border: 1px solid #000;
        }
        .details .rowspace {
            padding: 10px ;
        }
        .details {
            margin-top: 40px;
            border: 1px solid #000;
            width: 100%;
        }
        .details th {
            padding: 4px;
            text-align: center;
        }
        .details th, .details td {
            border-right: 1px solid #000;
        }
        .details td {
            padding-top:5px;
            padding-left: 4px;
            padding-right: 4px;
            padding-bottom:20px;
        }
        .details .player td {
            padding: 2px;
            border: 0px;
        }
        .details thead tr .description {
            width: 65%;
            text-align: left;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .amounts {
            width: 100%;
        }
        .amounts tr td {
            width: 50%;
            border-right:0px;
            border-left: 0px;
            border-top:0px;
            border-bottom: 1px solid #000;
            padding: 10px 4px 4px 4px;
            height: 40px;
            font-weight: bold;
        }
        .amounts tr:last-child td {
            border-bottom: 0px;
        }
        .no-padding {
            padding: 0px !important;
        }
        td {
            font-family: Arial;
        }

        .details thead { display: table-header-group }
        .details tfoot { display: table-row-group }
        .details tr { page-break-inside: avoid }
    </style>
</head>
<body>
<?php
// echo "<pre>";print_r($data);die;
$is_covid_credit = false;
?>
<table style="width: 100%;">
    <thead>
    <tr>
        <td style="width: 50%">
            <img src="{{ asset('assets-web/images/laliga-academy-logo-main.png') }}" alt="Laliga">
        </td>
        <td style="width: 50%;float:right;">
            LaLiga Academy UAE <br>
            Dubai Silicon Oasis <br>
            Suntech Tower, office 904 <br>
            +971 4 333 9130 <br>
            laligacademy@inspirat.us<br/>
            TRN#100268867700003
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <h2 style="text-align: center;font-size: 30px;font-weight: normal;">Tax Invoice</h2>
        </td>
    </tr>
    </thead>
</table>
<table style="width: 100%;">
    <tr>
        <td style="width: 50%">
            <table style="width: 90%" class="border">
                <tr>
                    <td colspan="">
                        Bill To :{{$invoices->user->name}}
                    </td>
                </tr>
                <tr>
                    <td colspan="">
                        <b>
                        <br>
                        For:
                        @foreach($data as $key=> $value)
                        {{$value['name']}}<br>
                        @endforeach
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 50%;float:right;">
            <table style="width: 100%" class="border">
                <tr>
                    <td style="text-align: left;">
                        Date
                    </td>
                    <td style="text-align: center;">
                     {{ \Carbon\Carbon::parse($invoices->created_at)->format('d-M-Y')}}
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        Invoice #
                    </td>
                    <td style="text-align: center;">
                        LA - 00{{$invoices->id}}
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        Project
                    </td>
                    <td style="text-align: center;">
                        LaLiga Academy
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        Terms
                    </td>
                    <td style="text-align: center;">
                        Immediate
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table class="details">
    <thead>
    <tr class="rowborder">
        <th class="description">Description</th>
        <th>Quantity</th>
        <th>Unit Price</th>
        <th>Total AED</th>
    </tr>
    </thead>
      <tbody>
        <tr class ="rowspace">
           <td>
            <br />
            <span style="font-size: 12px;"></span>
            </td>
              <td class="text-center">1</td>
              <td class="text-center">{{$invoices->amount}}</td>
              <td class="text-center">{{$invoices->amount}}</td>
            </tr>
           @foreach($data as $key=> $value)
          <tr class ="rowspace">
                <td>
                    <table class="player">
                        <tr>
                            <td>Player Name</td>
                            <td>:</td>
                            <td>{{$value['name']}}</td>
                        </tr>
                        <tr>
                            <td>Age category</td>
                            <td>:</td>
                            <td>{{$value['category']}}</td>
                        </tr>
                       
                            <tr>
                                <td valign="top">Selected Location</td>
                                <td valign="top">:</td>
                                <td>
                                  {{$value['location']}}
                                </td>
                            </tr>
                            <tr>
                                <td>Number of Training Days</td>
                                <td>:</td>
                                <td>
                                   {{$value['no_of_trainings']}}
                                </td>
                            </tr>
                   
                        <tr>
                            <td>Duration</td>
                            <td>:</td>
                            <td>
                                {{$value['term_name']}}
                            </td>
                        </tr>
                      
                      
                    
                    </table>
                </td>
                <td class="text-center">
                    1
                </td>
                <td class="text-right">
                   
                </td>
                <td class="text-right">
                  
                </td>
            </tr>
           
          @endforeach
<tr class="rowborder">
        <td class="">
            Cash / Check in favor of: Inspiratus Consulting Limited <br>
            Bank Details: Emirates NBD <br>
            Account Name: Inspiratus Consulting Limited <br>
            SWIFT Code: EBILAEAD <br>
            AED Account Number: 101-4845 4803-01 <br>
            AED IBAN Number: AE910260001014845480301

        </td>
        <td colspan="3" class="no-padding">
            <table style="" class="amounts">
               
                   
                <tr>
                    <td valign="bottom" style="text-align: left;">Total</td>
                    <td valign="bottom" style="text-align: right;">
                     {{$invoices->amount}}
                    </td>
                </tr>
             
                <tr>
                    <td valign="bottom" style="text-align: left;">Balance Due</td>
                    <td valign="bottom" style="text-align: right;"> {{$invoices->amount}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="rowborder">
        <td colspan="4">
          
                For du customer world cup discount invoices only upto 15 July.
                <br>
                To avail of the 15% discount the invoice must be paid in full by 15 July 2018, non payment will result in cancellation of the invoice.
                <br>
           

            <h4 style="margin-bottom: 0; text-decoration: underline;">Terms & Conditions</h4>
            <ul style="margin-top: 5px;">
              
                <li>This payment is non-refundable</li>
                <li>Places are limited and are only guaranteed upon payment of this invoice and based on space availability at the time of payment
                <li>Payment is required prior to commencement of each term</li>
                <li>Selection for Advanced Development and HPC Teams will incur additional costs which will be invoiced separately</li>
                <li>Participation in tournaments and leagues will incur additional participation and match kit fees which will be invoiced separately</li>
                <li>Missed classes cannot be re-scheduled. (No makeup sessions)</li>
                <li>If choosing to pay by bank transfer, please make sure to indicate under payment details: “LaLiga Academy – Name of Player” and share the transfer confirmation with us so we can track your payment</li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
