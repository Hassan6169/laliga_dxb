@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Player Terms: {{$player->name}}</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-md-6">
                <a href="{{route('admin.players.term.create',[$player->id])}}" class="btn --btn-small bg-primary fc-white mb-0">Add Term</a>
            </div>

            <div class="col-md-6">
                <div class="text-right">
                    <a href="{{ route('admin.players.index') }}" class="btn --btn-small bg-secondary fc-white mb-0">Back</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="box-body">
        <table class="table --bordered --hover">
            <tr>
                <th>S NO</th>
                <th>Player Details</th>
                <th>Kit Status</th>
                <th>Invoice</th>
                <th>Emirates</th>
                <th>Tranining Term</th>
                <th>Payment Status</th>
                <th>Action</th>
            </tr>
            
            @foreach($terms as $term)
            @if($term->pivot->deleted_at==null)
            <tr>
                <td>
                    {{ $no++ }}
                </td>
                
                <td>
                    <strong>Orginal Category:</strong>
                    @if(!empty($player->category_id))
                        {{$player->orginalCategory->name}}
                    @endif
                    <br />
                    <strong>Changed Category:</strong>
                    @if(!empty($player->admin_category_id))
                        {{$player->changeCategory->name}} @else {{"None"}}
                    @endif
                    <br />
                    <strong>Goal Keeping Training:</strong>
                    @if(!empty($player->is_goal_keeper==1))
                        {{"Yes"}}
                    @else
                        {{"No"}}
                    @endif
                    <br />
                    <strong>Is Sponsored :</strong>
                    @if(!empty($term->pivot->is_sponsored==1))
                        {{"Yes"}}
                    @else
                        {{"No"}}
                    @endif
                    <br />
                </td>
                
                <td>
                    <strong>kit Size:</strong>
                    @foreach($kit_size as $key => $size)
                    @if($term->pivot->kit_size==$key)
                        {{$size}}
                    @endif
                    @endforeach
                    <br />
                    <strong>kit Received:</strong>
                    @foreach($kit_status as $key => $status)
                    @if($term->pivot->kit_status==$key)
                        {{$status}}
                    @endif
                    @endforeach
                    <br />
                    <strong>Match Kit:</strong>
                    @if(!empty($term->pivot->match_kit==1))
                        {{"Applicable"}}
                    @else
                        {{"Not Applicable"}}
                    @endif
                </td>
                
                <td>
                    @if($term->pivot->payment_status==STATUS_READY_TO_PAY||($term->pivot->payment_status==STATUS_PAID))
                        <a href="{{ route('admin.player.term.invoices',[$player->id,$term->id]) }}" target="_blank" class="btn --btn-small bg-secondary fc-white">Invoice</a>
                    <br />
                    @endif
                  
                </td>
                
                <td>
                    {{$term->getEmirate($term->pivot->emirate_id)}}
                </td>
                
                <td>
                    {{$term->name}}
                </td>

                <td>
                    @foreach($payment_status as $key => $status)
                    @if($term->pivot->payment_status==$key)
                        {{$status}}
                    @endif
                    @endforeach
                </td>

                <td>

                    <a href="{{ route('admin.players.term.edit',[$player->id,$term->pivot->id]) }}" class="btn --btn-small bg-secondary fc-white"  style="width: 150px;">Edit</a>
                    <br />
                    
                     <a href="{{route('admin.player.training.session.edit',[$player->id,$term->id,$term->pivot->emirate_id]) }}" class="btn --btn-small bg-primary fc-white" style="width: 150px;">Locations</a>
                    
                    <br />
                    <a href="{{route('admin.player.term.custom.invoice',[$player->id,$term->id])}}" class="btn --btn-small bg-warning fc-white mb-0" style="width: 150px;">Custom Invoice</a>
                    <br />
                    <!-- <a href="{{route('admin.players.discount.index',[$player->id,$term->id])}}" class="btn --btn-small bg-warning fc-white" style="width: 150px;">Discount</a> -->
                    <br />
                   <button class="btn --btn-small bg-danger fc-white btn-sm" onclick="deleteTerm({{$player->id}},{{$term->id}},{{$term->pivot->id}})"  style="width: 150px;">Delete</button>
                   
            
                </td>          
            </tr>
         @endif
        @endforeach
        </table>
    </div>
    <script type="text/javascript">
    function deleteTerm(player,term,id)
    {
    if(confirm('Are you sure?')){
    location.href="{{url('/admin/players')}}"+"/"+player +"/term/"+term+"/id/"+id;
    }else{
    return false;
    }
        }
    </script>
</section>
@endsection