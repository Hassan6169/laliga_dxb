@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add News</h3>
<section class="box">
  <div class="box-body">
    {!! Form::open(['route' => 'admin.news.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.news._form',['submitBtnText' => 'Add New'])
    {!! Form::close() !!}
  </div>
</section>
@endsection