<div class="box-body">
<div class="form-group">
    <div class="control-group{{ $errors->has('gender') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('new_type','News Type', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::select('new_type',$new_type,null,['class' => 'form-field']); !!}
           </div>
            @if ($errors->has('new_type'))
                <span class="error-msg">
                    <strong>{{ $errors->first('new_type') }}</strong>
                </span>
            @endif
        </div>
    </div>
<div class="form-group">
    <div class="control-group{{ $errors->has('name_eng') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('name_eng', 'Name(Eng)', ['class'=>'control-label',]) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::text('name_eng', null, ['class'=>'form-field','maxlength'=>'100','required' => 'required']) !!}
           </div>
            @if ($errors->has('name_eng'))
                <span class="error-msg">
                    <strong>{{ $errors->first('name_eng') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group">
    <div class="control-group{{ $errors->has('news_source_eng') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('news_source_eng','News Source Eng', ['class'=>'control-label',]) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::text('news_source_eng',null,['class'=>'form-field','required' => 'required']) !!}
           </div>
            @if ($errors->has('news_source_eng'))
                <span class="error-msg">
                    <strong>{{ $errors->first('news_source_eng') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group">
    <div class="control-group{{ $errors->has('session_id') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('session_id','Session', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::select('session_id',$sessions,null,['class' => 'form-field']); !!}
           </div>
            @if ($errors->has('session_id'))
                <span class="error-msg">
                    <strong>{{ $errors->first('session_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

   <div class="form-group">
    <div class="control-group{{ $errors->has('news_events_id') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('news_events_id','News Event', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::select('news_events_id',$news_events,null,['class' => 'form-field']); !!}
           </div>
            @if ($errors->has('news_events_id'))
                <span class="error-msg">
                    <strong>{{ $errors->first('news_events_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="form-group">
    <div class="control-group{{ $errors->has('details_eng') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('details_eng', 'details_eng', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::textarea('details_eng', null, ['class'=>'form-field','required' => 'required']) !!}
           </div>
            @if ($errors->has('details_eng'))
                <span class="error-msg">
                    <strong>{{ $errors->first('details_eng') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group">
    <div class="control-group{{ $errors->has('name_ar') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('name_ar', 'Title(Arabic)', ['class'=>'control-label',]) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::text('name_ar', null, ['class'=>'form-field','required' => 'required']) !!}
           </div>
            @if ($errors->has('name_ar'))
                <span class="error-msg">
                    <strong>{{ $errors->first('name_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
  <div class="form-group">
    <div class="control-group{{ $errors->has('details_ar') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('details_ar', 'details(Ar)', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::textarea('details_ar', null, ['class'=>'form-field','required' => 'required']) !!}
           </div>
            @if ($errors->has('details_ar'))
                <span class="error-msg">
                    <strong>{{ $errors->first('details_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="form-group">
     <div>
         @if(!empty($news->image))
         <img src="{{ asset('storage/news_images/'.$news->image) }}" alt="Girl in a jacket" style="width:400px;height:200px;">
         @endif
     </div>
    </div>

      <div class="form-group">
    <div class="control-group{{ $errors->has('image') ? ' has-error' : '' }}">
         <div class="col-sm-3">
            {!! Form::label('image', 'Thumb Image', ['class'=>'control-label']) !!}
        </div>
            <div class="col-sm-9">
            {!! Form::file('image', null, ['class'=>'form-field','required' => 'required']) !!}
           </div>
            @if ($errors->has('image'))
                <span class="error-msg">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
            @endif
        </div>
    </div>

       <div class="form-group">
        <div class="control-group{{ $errors->has('external_link') ? ' has-error' : '' }}">
            <div class="col-sm-3">
                {!! Form::label('external_link', 'External Link', ['class'=>'control-label']) !!}
            </div>
            <div class="col-sm-9">
                <div class='input-group'>
                    {!! Form::text('external_link',null,['class'=>'form-field','required' => 'required','id'=>'external_link','autocomplete'=>'off']) !!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            @if ($errors->has('external_link'))
            <span class="error-msg">
                <strong>{{ $errors->first('external_link') }}</strong>
            </span>
            @endif
        </div>
    </div>

     <div class="form-group">
        <div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
             <div class="col-sm-3">
                {!! Form::checkbox('is_active', 1, isset($news) ? $news->is_active : true) !!}
                {!! Form::label('is_active', 'Activate', ['class' => 'control-label']) !!}
                @if ($errors->has('is_active'))
                    <span class="error-msg">
                        <strong>{{ $errors->first('is_active') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
<div class="form-group">
    <center>
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
       
    </div>
</div>
</div>
<script> 
 var dateToday = new Date();
 $( "#published_date" ).datepicker({
    
 });
</script>