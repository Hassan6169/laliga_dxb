@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Gallery Categories</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-8">
        <a href="{{route('admin.gallery.categories.create')}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Add</button></a>
      </div>
      <div class="col-md-4">
         <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
      </div>
    </div>
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
       <tr>
          <th><center>S NO</center></th>
          <th><center>Title</center></th>
          <th><center>Session</center></th>
          <th><center>Status</center></th>
          <th><center>Action</center></th>
        </tr>
        @foreach ($galleries as $gallery)
        <tr>
          <td><center>{{ $no++ }}</center></td>
          <td><center>{{ $gallery->name_eng }}</center></td>
          <td><center>@foreach($sessions as $key => $session) 
           @if($gallery->session_id==$key){{ $session }} 
           @endif @endforeach</center></td>
          <td><center>
            @if($gallery->is_active)
            <p>Active</p>
            @else
            <p>InActive</p>
          @endif</center></td>
          <td><center><a href="{{ route('admin.gallery.categories.edit',[$gallery->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Edit</button></a>
        <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $gallery->id }}" id="deleteItem">Delete</button></center></td>
      </tr>
      @endforeach
    </table>
    <ul class="pagination">
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-left"></i>
        </a>
      </li>
      <li>
        <a href="#">1</a>
      </li>
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-right"></i>
        </a>
      </li>
    </ul>
  </div>
  </section>
  {!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
  {!! Form::close() !!}
  <script>
  var deleteResourceUrl = '{{ url()->current() }}';
  $(document).ready(function () {
  
  $('.deleteItem').click(function (e) {
  e.preventDefault();
  var deleteId = parseInt($(this).data('deleteId'));
  
  $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
  if (confirm('Are you sure?')) {
  $('#deleteForm').submit();
  }
  });
  });
  </script>
  @endsection