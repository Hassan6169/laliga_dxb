@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add</h3>
<section class="box">
  <div class="box-body">
    {!! Form::open(['route' => 'admin.gallery.categories.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.gallery_categories._form',['submitBtnText' => 'Add New'])
    {!! Form::close() !!}
  </div>
</section>
@endsection