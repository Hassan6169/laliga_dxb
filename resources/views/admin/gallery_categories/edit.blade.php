@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit New</h3>
<section class="box">
  <div class="box-body">
    {!! Form::model($gallery_category,['route' => ['admin.gallery.categories.update', $gallery_category->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.gallery_categories._form',['submitBtnText' => 'Update'])
    {!! Form::close() !!}
  </div>
</section>
@endsection