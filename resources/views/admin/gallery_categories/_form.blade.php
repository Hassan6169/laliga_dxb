<div class="box-body">
    <div class="control-group{{ $errors->has('name_eng') ? ' has-error' : '' }}">
        <div class="col-sm-3">
            {!! Form::label('name_eng', 'Name(Eng)', ['class'=>'form-label']) !!}
        </div>
        <div class="col-sm-9">
            {!! Form::text('name_eng', null, ['class'=>'form-field','required' => 'required']) !!}
            
            @if ($errors->has('name_eng'))
            <span class="error-msg">
                <strong>{{ $errors->first('name_eng') }}</strong>
            </span>
            @endif
        </div>
    </div>
<div class="control-group{{ $errors->has('name_ar') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::label('name_ar', 'Title(Arabic)', ['class'=>'form-label']) !!}
    </div>
    <div class="col-sm-9">
        {!! Form::text('name_ar', null, ['class'=>'form-field','required' => 'required']) !!}
        
        @if ($errors->has('name_ar'))
        <span class="error-msg">
            <strong>{{ $errors->first('name_ar') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group{{ $errors->has('session_id') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::label('session_id','Session', ['class'=>'form-label']) !!}
    </div>
    <div class="col-sm-9">
        {!! Form::select('session_id',$sessions,null,['class' => 'form-field']); !!}
    </div>
    @if ($errors->has('session_id'))
    <span class="error-msg">
        <strong>{{ $errors->first('session_id') }}</strong>
    </span>
    @endif
</div>



<div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::checkbox('is_active', 1, isset($gallery_category) ? $gallery_category->is_active : true) !!}
        {!! Form::label('is_active', 'Activate', ['class' => 'control-label']) !!}
        @if ($errors->has('is_active'))
        <span class="error-msg">
            <strong>{{ $errors->first('is_active') }}</strong>
        </span>
        @endif
    </div>
</div>
<center>
<input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
<a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>


</div>
</div>