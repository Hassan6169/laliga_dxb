@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/trials-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Trials</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">
                <h2 class="maintitle --black --small">
                    LaLiga Trials
                </h2>

                <p class="maindesc">Throughout the season, HPC players have opportunities to earn trials at LaLiga Clubs.  This trials represent unique experiences through which the players receive professional training alongside  their counterparts at top LaLiga Clubs and get seen by the club coaches and scouts. To date 20 players from the HPC have earned trials at LaLiga Clubs:</p>

                <ul class="unstyled table">
                    <li>
                        <img src="/assets-web/images/scouting/levante-UD.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/celta-vigo.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/al-meria-UD.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/cadiz-CF.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/real-betis.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/club-deportivo.png" alt="LaLiga Academy" class="m-auto">
                    </li>
                </ul>

                <ul class="unstyled check-list two-column-list">
                    <li>Mehdi El Kamlichi trial at Malaga CF</li>
                    <li>Mohammed Bouherafa trial at Almeria UD, El Ejido</li>
                    <li>Yahyia Iraqi trial at Malaga CF, Real Betis</li>
                    <li>Ahmed El Yamani selected by Celta Vigo, trial at Malaga </li>
                    <li>Mohamed Rajeh trial at Cadiz</li>
                    <li>Ahmed Salam trial at Malaga CF, El Ejido</li>
                    <li>Barbod Mazloumian trial at Malaga CF, Levante U.D.</li>
                    <li>Rajai Ardakani trial at Celta Vigo</li>
                    <li>Aidan Pinto trial at Celta Vigo</li>
                    <li>Isa Adam Aslangolu trial at Malaga CF</li>
                    <li>Jade Sultan trial at Levante U.D.</li>
                    <li>Lotfi Machou selected by Malaga</li>
                    <li>Laurent Hallal selected by Malaga</li>
                    <li>Adham Bayoumi selected by Malaga</li>
                    <li>Mohammed Ali Awadh selected by UD Leganes</li>
                </ul>

            </article>
        </div>
    </section>

@endsection
