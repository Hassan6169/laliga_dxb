@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/homepage-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Teams</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">
                <h2 class="maintitle --black --small">
                    Professional Teams
                </h2>

                <p class="maindesc">Players trained at the LaLiga HPC are recognized as the top youth footballers across the UAE and many have gone on to earn contracts with national clubs across the UAE.  To date more than 21 players trained at the LaLiga HPC are playing for professional clubs in the UAE.</p>

                <ul class="unstyled table">
                    <li>
                        <img src="/assets-web/images/scouting/al-nasr.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/shabab-alahli.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/alain-FC.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/aljazira-club.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/alwahda-FC.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/baniyas-club.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/sharjah-FC.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/emirates-club.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/nadi-hamriya.png" alt="LaLiga Academy" class="m-auto">
                    </li>

                    <li>
                        <img src="/assets-web/images/scouting/ajman-club.png" alt="LaLiga Academy" class="m-auto">
                    </li>
                </ul>

                <ul class="unstyled check-list two-column-list">
                    <li>Mehdi El Kamlichi at Al Nasr Club</li>
                    <li>Mohammed Bouherafa at Al Nasr Club</li>
                    <li>Aidan Pinto trial at Al Nasr Club</li>
                    <li>Lotfi Machou at Al Nasr Club</li>
                    <li>Mohammed Ehab at Al Nasr Club</li>
                    <li>Abhinov John at Al Nasr Club</li>
                    <li>Ahmed El Yamani at  Shabab Al Ahli Club</li>
                    <li>Hussein Hawi at Shabab Al Ahli Club</li>
                    <li>Mohammed Mahious at Sharjah Club</li>
                    <li>Yahyia Iraqi trial at Emirates Club</li>
                    <li>Mahmoud Tarmoum at Al Hamriya Club</li>
                    <li>Mohammed Sabri at Al Jazira Club</li>
                    <li>Ahmed Salam at Al Jazira Club</li>
                    <li>Youssef Mazen at Baniyas Club</li>
                    <li>Moustafa Mohamed at Al Wahda Club</li>
                    <li>Mahmoud Jarrar at Al Wahda Club</li>
                    <li>Abdelrahman Suhbi at Al Wahda Club </li>
                    <li>Ramy Hatem at Al Ain Club</li>
                    <li>Mohammed Eyad at Al Ain Club</li>
                    <li>Hazem Mohammad Abbaas at Al Ain Club</li>
                    <li>Rashid Al Hamadi at Ajman Club</li>
                </ul>

            </article>
        </div>
    </section>

@endsection
