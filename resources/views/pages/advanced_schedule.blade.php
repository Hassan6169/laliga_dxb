@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/schedule-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">ADVANCED</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <p class="fs-large">All LaLiga Academy Development squad players have the opportunity to be invited by their coaches to join the LaLiga Academy Advanced Development program. The Advanced Development programs may require them to train on additional days in alternate locations and participate in competitive leagues and tournaments. Participation in the LaLiga Advanced Development Program will be for an additional fee.</p>

            <article class="inner-content mt-40">
                <h2 class="maintitle --black">
                    <span>dubai</span>
                    TRAINING SCHEDULES
                </h2>
                
                <?php /* Gems World Academy Dubai */?>
                <h4 class="fc-primary">GEMS WORLD ACADEMY - AL BARSHA SOUTH</h4>

                <div class="Rtable --schedule-table --collapse --7cols text-center">
                    <div class="Rtable-cell --head">
                        Age Category
                    </div>

                    <div class="Rtable-cell --head">
                        Sunday
                    </div>

                    <div class="Rtable-cell --head">
                        Monday
                    </div>

                    <div class="Rtable-cell --head">
                        Tuesday
                    </div>

                    <div class="Rtable-cell --head">
                        Wednesday
                    </div>

                    <div class="Rtable-cell --head">
                        Thursday
                    </div>

                    <div class="Rtable-cell --head">
                        Friday
                    </div>
                    
                    <!-- U6 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U6
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U8 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U8
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U10 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U10
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                6:00 - 7:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                6:00 - 7:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U12 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U12
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U14 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U14
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U16 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U16
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U18 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U18
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U23 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U23
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>
                </div>


                <?php /* Al Barari Dubai */?>
                <h4 class="fc-primary">AL BARARI</h4>

                <div class="Rtable --schedule-table --collapse --7cols text-center">
                    <div class="Rtable-cell --head">
                        Age Category
                    </div>

                    <div class="Rtable-cell --head">
                        Sunday
                    </div>

                    <div class="Rtable-cell --head">
                        Monday
                    </div>

                    <div class="Rtable-cell --head">
                        Tuesday
                    </div>

                    <div class="Rtable-cell --head">
                        Wednesday
                    </div>

                    <div class="Rtable-cell --head">
                        Thursday
                    </div>

                    <div class="Rtable-cell --head">
                        Friday
                    </div>
                    
                    
                    <!-- U8 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U8
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U10 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U10
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U12 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U12
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U14 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U14
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U16 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U16
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:15 - 8:30
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:15 - 8:30
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U18 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U18
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:15 - 8:30
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:15 - 8:30
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- Girls U14 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                Girls U14
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>
                </div>

            </article>

            <article class="inner-content mt-40">
                <h2 class="maintitle --black">
                    <span>abu dhabi</span>
                    TRAINING SCHEDULES
                </h2>
                
                <?php /* Raha School Abu Dhabi */?>
                <h4 class="fc-primary">Raha School</h4>

                <div class="Rtable --schedule-table --collapse --7cols text-center">
                    <div class="Rtable-cell --head">
                        Age Category
                    </div>

                    <div class="Rtable-cell --head">
                        Sunday
                    </div>

                    <div class="Rtable-cell --head">
                        Monday
                    </div>

                    <div class="Rtable-cell --head">
                        Tuesday
                    </div>

                    <div class="Rtable-cell --head">
                        Wednesday
                    </div>

                    <div class="Rtable-cell --head">
                        Thursday
                    </div>

                    <div class="Rtable-cell --head">
                        Friday
                    </div>
                    
                    <!-- U6 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U6
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U8 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U8
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U10 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U10
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U12 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U12
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U14 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U14
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:15 - 8:30
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:15 - 8:30
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U16 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U16
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:15 - 8:30
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:15 - 8:30
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                </div>


                <?php /* BSAK  Abu Dhabi*/?>
                <h4 class="fc-primary">BSAK</h4>

                <div class="Rtable --schedule-table --collapse --7cols text-center">
                    <div class="Rtable-cell --head">
                        Age Category
                    </div>

                    <div class="Rtable-cell --head">
                        Sunday
                    </div>

                    <div class="Rtable-cell --head">
                        Monday
                    </div>

                    <div class="Rtable-cell --head">
                        Tuesday
                    </div>

                    <div class="Rtable-cell --head">
                        Wednesday
                    </div>

                    <div class="Rtable-cell --head">
                        Thursday
                    </div>

                    <div class="Rtable-cell --head">
                        Friday
                    </div>
                    
                    <!-- U6 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U6
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U8 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U8
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                5:00 - 6:00
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U10 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U10
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U12 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U12
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                6:00 - 7:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U14 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U14
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <!-- U16 Category -->
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Age Category
                            </div>

                            <div class="content">
                                U16
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Sunday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Monday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Tuesday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Wednesday
                            </div>

                            <div class="content">
                                7:00 - 8:15
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Thursday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Saturday
                            </div>

                            <div class="content">
                                -
                            </div>
                        </div>
                    </div>

                </div>

            </article>
        </div>
    </section>


@endsection
