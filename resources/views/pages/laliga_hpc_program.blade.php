@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/hpc-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">High Performance</h1>
        </div>
    </section>

    <section class="half-beats sec-padding bg-blue" data-img="url({{ asset('assets-web/images/half-beat.png') }})">
        <div class="container-wrapper">
            <div class="row">
                <div class="col-lg-7">
                    <article class="inner-content">
                        <h2 class="maintitle fc-yellow --small">
                            LaLiga HPC
                        </h2>

                        <p class="maindesc --white">
                            The first ever global initiative for LaLiga enabling the youth across the UAE to train yearlong at the highest standards to become future national, regional and international pro-footballers. At the LaLiga HPC, youth in the UAE are trained at the same standards of youth footballers in Spain, providing them with a solid foundation to springboard into college football or pro-football. <br><br>

                            Training at the LaLiga HPC is focused on high sports performance driven by game strategy.  Players are exposed to intense tactical drills to increase endurance, improve reaction speed, strength and conditioning, prevention and rehabilitation, recovery all with the aim of boosting their athleticism and get them game-ready. <br><br>

                            High Performance teams train 3-4 days per week and compete in leagues throughout the season. In addition, matches are set up against local clubs and international tournaments throughout the season to give the players exposure to highest level of competition though which they can improve. <br><br>

                            Every summer top players are selected for a 15-day scouting camp in Spain where they have exposure to an intensified professional training program and play competitive matches against top LaLiga youth clubs.  Through this experience they have the opportunity to be selected by LaLiga and invited for trials which can lead to contract awards. </p>
                    </article>
                </div>

                <div class="col-lg-4 offset-lg-1">
                    <figure>
                        <img data-src="{{ asset('assets-web/images/hpc/about-img.png') }}" alt="LaLiga Academy" class="lazy">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <div class="row">
                <div class="col-lg-5">
                    <p class="lh-medium h2">The LaLiga HPC is by invitation only and runs with <br> <strong>5 categories:</strong></p>
                </div>

                <div class="col-lg-3 offset-lg-1">
                    <figure>
                        <img data-src="{{ asset('assets-web/images/hpc/male-icon.png') }}" alt="LaLiga Academy" class="lazy m-auto">
                    </figure>

                    <h3 class="text-center mt-20">Boys: <br> U18, U16, U14, U12</h3>
                </div>

                <div class="col-lg-3">
                    <figure>
                        <img data-src="{{ asset('assets-web/images/hpc/female-icon.png') }}" alt="LaLiga Academy" class="lazy m-auto">
                    </figure>

                    <h3 class="text-center mt-20">Girls: <br> U17</h3>
                </div>
            </div>

            <article class="mt-80">
                <h2 class="maintitle --small fc-blue mb-60">HPC PLAYERS ARE:</h2>

                <div class="row">
                    <div class="col-lg-3">
                        <figure>
                            <img data-src="{{ asset('assets-web/images/hpc/versatile-img.png') }}" alt="LaLiga Academy" class="lazy m-auto">
                        </figure>
                    </div>

                    <div class="col-lg-3">
                        <figure>
                            <img data-src="{{ asset('assets-web/images/hpc/talented-img.png') }}" alt="LaLiga Academy" class="lazy m-auto">
                        </figure>
                    </div>

                    <div class="col-lg-3">
                        <figure>
                            <img data-src="{{ asset('assets-web/images/hpc/committed-img.png') }}" alt="LaLiga Academy" class="lazy m-auto">
                        </figure>
                    </div>

                    <div class="col-lg-3">
                        <figure>
                            <img data-src="{{ asset('assets-web/images/hpc/consistent-img.png') }}" alt="LaLiga Academy" class="lazy m-auto">
                        </figure>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section class="half-beats sec-padding bg-blue" data-img="url({{ asset('assets-web/images/half-beat.png') }})">
        <div class="container-wrapper">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <h2 class="maintitle --white --small">hpc players have:</h2>
                </div>
                
                <div class="col-lg-7">
                    <figure class="float-right">
                        <img data-src="{{ asset('assets-web/images/hpc/hpc-structure.png') }}" alt="LaLiga Academy" class="lazy inherit">
                    </figure>
                </div>
            </div>
        </div>
    </section>


@endsection
