@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/methodology-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Methodology</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">
                <h2 class="maintitle --black --small">
                    Methodology Program
                </h2>

                <p class="maindesc">
                    Our approach to training is player-centric and we focus on the role of the player within the context of the team and the game, developing each players skills to their highest potential.  LaLiga Academy’s integrated approach combines the technical and tactical skills together with the conditional and cognitive, always driving the interconnectivity of these 4 key skills to simulate decision-making. <br><br>

                    We believe that every aspiring football player can become a competitive athlete.  We work on understanding each player to customize our approach to their development and encourage them to reach their highest potential:
                </p>
            </article>
        </div>
    </section>

    <section class="bg-spgrey sec-padding">
        <div class="container-wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <h4 class="maintitle --small text-center mb-40">THE PLAYER</h4>
                    <figure>
                        <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/methodology-img1.png') }}" alt="LaLiga Academy" class="lazy">
                    </figure>
                </div>

                <div class="col-lg-6">
                    <h4 class="maintitle --small text-center mb-40">THE PROGRAM</h4>
                    <figure>
                        <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/methodology-img2.png') }}" alt="LaLiga Academy" class="lazy">
                    </figure>
                </div>
            </div>
        </div>
    </section>


@endsection
