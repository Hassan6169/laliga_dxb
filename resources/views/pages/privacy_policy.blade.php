@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/homepage-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Privacy Policy</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">

                <p class="maindesc">The Companies in Inspiratus Group (together 'Inspiratus" “we" and "us") respect the privacy of the visitors to website of LaLiga Academy UAE (“Academy”): ae.laligacademy.com (“Site”) and the local websites connected with it, and take great care to protect your information. <br><br>

                This Privacy Policy describes what information we may collect from you, how we collect that information, how we use that information, and the choices you have regarding your information. By using our website, mobile applications, or providing information to us in other ways, you agree to be bound by this Privacy Policy. <br><br>

                This Privacy Policy may be updated from time to time. Please check back periodically for further updates and changes. The most updated version will be the one that is available on this website. <br><br>

                WHAT INFORMATION ABOUT ME IS COLLECTED AND STORED?</p>
                

                <h2 class="maintitle --black --small">
                    Information You Provide
                </h2>

                <p class="maindesc">We collect and store information that you provide to us through our websites or at events, through mobile applications, through marketing efforts, including contests, sweepstakes and events, and through third parties, or any other way we interact with you. <br><br>

                You may choose not to provide certain information, but then you might not be able to take advantage of certain capabilities of this website, our mobile applications or other services we may offer. For example, you must provide certain personal information in order to place an order or to register for an account on or through this website. <br><br>

                We may request, collect and store some or all of the following types of personal information: name, e-mail address, telephone number, gender, personal interests, birthday, information about a third party and similar. <br><br>

                If you access our website or mobile applications with your log-in credentials from a social networking site (e.g. Facebook, Pinterest, or Twitter) or if you otherwise agree to associate your account with us with a social networking site, we may receive personal information about you from such social networking site, in accordance with the terms of use and privacy policy of the social networking site. We may add this to the information we have already collected from you via other interactions. <br><br>

                When you download or use our mobile applications, we may receive information about your location and your mobile device. We may use this information to provide you with location-based services like local store information, search results, and other personalized content. Most mobile devices allow users to disable location services by using controls located in the device's settings menu. If you have questions about how to disable your device's location services, please contact your mobile service carrier or your device manufacturer.</p>


                <h2 class="maintitle --black --small">
                    Information from Other Sources
                </h2>

                <p class="maindesc">We collect and store certain other non-personally identifiable information automatically whenever you interact with us. For example, we may collect your Internet Protocol (IP) address and browser information and reference site domain name when you visit this website and analyze this data for preferences, trends, and site usage statistics. <br><br>

                We may also collect information by using cookies, web beacons, and related technologies in order to analyze and understand how the website is used and to enhance your visits to the website. For example, we may use cookies and related technologies to: (i) customize account pages for you, (ii) track user visits and account features, (iii) determine what features and pages are most frequently requested or visited, and (iv) save your password so you do not have to provide it each time you visit the website. <br><br>

                "Cookies" are small text files that a website sends to a user's hard drive through the browser for data storage. Most browsers accept cookies automatically but allow you to disable them. By disabling or otherwise rejecting cookies, you may not be able to take full advantage of all offerings on this website. We recommend that you leave cookies "turned on" so that we can offer you a better browsing experience on this website. <br><br>

                We use third-party companies to provide Internet data hosting and to help us measure and determine how visitors use our website and the effectiveness of our website and promotions/competitions in order to help us improve and optimize our website and the products and services we offer. We also use third-party companies to serve advertisements (banners or links) on our behalf across the Internet. These companies use tracking technologies (including cookies and pixel tags) to collect non-identifiable information about your visits to our website and your interactions with our products and services in order to provide tailored advertisements based on your interests and browsing of our website (remarketing) on our behalf and on behalf of other advertisers across the Internet. We use third-party audience data (such as age, gender, and interests) to better understand and advertise to audiences that visit our website. We do not use or associate personally identifiable information with remarketing lists, cookies, data feeds, or other anonymous identifiers. <br><br>

                Our websites currently do not have a mechanism to recognize "Do Not Track" signals, but several of the third-party companies who utilize these cookies or beacons enable you to opt out of this advertising. If you wish to opt out of receiving this third-party interest-based advertising, you may obtain more information here. Please note this does not opt you out of receiving marketing communications from us. <br><br>

                In addition, some of our e-mail communications to you may include a "click-through URL" linked to a particular page on the website. By clicking on one of these URLs, you will automatically enter our website and we may collect information related to the "click-through." You may avoid having this information collected by not clicking on URL links in our e-mail communications. <br><br>

                We also may use a variety of web analytics tools for operational and marketing purposes, such as measuring and analyzing traffic patterns. These tools may collect data from sensors, including video cameras, smartphone detectors, Wi-Fi networks, or other devices. <br><br>

                We may collect information about you from third-party sources and information that is publicly available. For example, information you submit in a public forum (e.g., a blog, chat room, or social network site) can be read, collected, or used by us and others, and could be used to personalize your experience. You are responsible for the information you choose to submit in these instances. Another example, is that we may receive demographic information about you from a third party to make our service and marketing efforts more efficient and personalized for you.</p>


                <h2 class="maintitle --black --small">
                    How Is Information Used?
                </h2>

                <p class="maindesc">We use the personal information we collect to communicate with you, and provide great services, and experiences. We or third-party service providers we engage may use the information to respond to your comments, questions or complaints, to personalize your experiences, to analyze your preferences and habits, to analyze trends and statistics, to administer and fulfill our contests and other promotions, to analyze and improve the website and our products and services, and to send you marketing communications and other information regarding our products, services, marketing, or special events. We also use your information for business purposes such as security, analytics, operations, fraud detection, reporting, making back-ups, and legal compliance. <br><br>

                We display personal testimonials/reviews of the Academy participants, parents, and other parties related to the Academy on our site in addition to other endorsements. With your consent we may post your testimonial along with your name. If you wish to update or delete your testimonial, you can contact us at +971 4 3926610. </p>


                <h2 class="maintitle --black --small">
                    With Whom Is Information Shared?
                </h2>

                <p class="maindesc">Except with your consent or as described in this Privacy Policy, we do not sell, rent, or share with third parties any of your personal information. We may share your personal information with other companies within the Inspiratus family of brands. <br><br>

                We may share personal information with certain third parties that perform services on our behalf. The services provided by those third parties may include: operating the website, hosting the website, providing the products and services you request, marketing, web site evaluation, data analysis and, where applicable, data cleansing. Those third parties are not authorized to use or disclose personal information you provide to us on or through this website for any purpose other than to perform the services designated by us. <br><br>

                We may share personal information with a third party if we are required by law to disclose that personal information. For example, we may be required to disclose personal information to respond to a subpoena, court order, or other legal process. We may also disclose this information in response to a law enforcement agency's request, or where we believe it is necessary to investigate or address unlawful acts or acts that may endanger the health or safety of a consumer or the general public, to verify or enforce compliance with the terms of use and policies that govern our website or as otherwise required or permitted by law. In addition, the personal information you provide to us may be transferred as an asset in connection with a merger or sale involving all or part of Inspiratus® or as part of a corporate reorganization, stock sale or other change of control.</p>


                <h2 class="maintitle --black --small">
                    What About The Privacy Of Children Under 13?
                </h2>

                <p class="maindesc">If you are under the age of 13 you should not provide any personal information to us and you should use this website only with the involvement of a parent or guardian. If you are a parent or guardian of a child under the age of 13 and suspect he/she has provided personal information on or through the website, please contact us as set forth below under "How Can I Contact You Regarding This Privacy Policy?"</p>

                <h2 class="maintitle --black --small">
                    What Steps Are Taken To Keep Personal Information Secure?
                </h2>

                <p class="maindesc">The security of your information is important to us. We have instituted security procedures and measures to protect our servers and networks that store personal information. Unfortunately, no transmission of information over the Internet can be guaranteed to be 100% secure and, therefore, we cannot ensure or warrant the security of any information. In order to help protect personal information you provide to us, you: (i) should not share your password or account information with others, (ii) should use a secure web browser, and (iii) should change your password frequently. If you wish to cancel a password, or if you become aware of any loss, theft, or unauthorized use of a password, please contact us as set forth below under "How Can I Contact You Regarding This Privacy Policy?"</p>

                <h2 class="maintitle --black --small">
                    How Can i Access, Correct And Update Personal Information?
                </h2>

                <p class="maindesc">We reserve the right to retain any personal information reasonably necessary to appropriately document our business activities and for archival and record retention purposes. We will store personal information for as long as reasonably necessary for the purposes described in this Privacy Policy. You may find out more details regarding this Privacy Policy by contacting us as provided below under "How Can I Contact You Regarding This Privacy Policy?"</p>

                <h2 class="maintitle --black --small">
                    What Happens To My Payment Information?
                </h2>

                <p class="maindesc">This website may contain links to or from websites to which this Privacy Policy does not apply ("Linked Websites"). Please be advised that the practices described in this Privacy Policy do not reflect the practices regarding information collection, use and disclosure of Linked Websites or the owners or operators of those Linked Websites. Your use of Linked Websites will be governed by the terms of use, privacy policies, and other terms, notices, and policies set forth on or referenced in those Linked Websites. We encourage you to be aware of when you leave this website and to read the privacy policies of each and every website that you visit. <br><br>

                Some of the advertisements you see on the Site are selected and delivered by third parties, such as ad networks, advertising agencies, advertisers, and audience segment providers. These third parties may collect information about you and your online activities, either on the Site or on other websites, through cookies, web beacons, and other technologies in an effort to understand your interests and deliver to you advertisements that are tailored to your interests. Please remember that we do not have access to, or control over, the information these third parties may collect. The information practices of these third parties are not covered by this privacy policy. </p>

                <h2 class="maintitle --black --small">
                    How Can I Contact You Regarding This Privacy Policy?
                </h2>

                <p class="maindesc">The Website Policies and Terms & Conditions may be changed or updated occasionally to meet the requirements and standards. Therefore the Customers are encouraged to frequently visit these sections in order to be updated about the changes on the website. Modifications will be effective on the day they are posted. <br><br>

                If you have any questions or concerns regarding your privacy or the Privacy Policy you can send a request with your concerns or any further questions to us by using the contact information provided below: <br><br>

                By Email: <a href="mailto:sportsmarketing@inspirat.us?subject=Privacy Policy Questions">sportsmarketing@inspirat.us</a> <br>
                Subject: Privacy Policy Questions <br>
                Attn: LaLiga Academy UAE <br><br>

                By Mail: <br>
                Inspiratus <br>
                P.O.Box 341312 <br>
                Suntech Tower, Office 905, Dubai Silicon Oasis <br>
                Dubai, U.A.E <br><br>

                Inspiratus Consulting Limited®</p>

            </article>
        </div>
    </section>

@endsection
