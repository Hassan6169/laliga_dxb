@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/registration-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Fees</h1>
        </div>
    </section>

@endsection
