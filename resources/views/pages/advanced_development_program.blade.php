@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/advance-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Advanced</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">
                <h2 class="maintitle --black --small">
                    Advanced Program (Girls &amp; Boys)
                </h2>

                <p class="maindesc">
                    The LaLiga Academy Advanced program has been developed to provide aspiring football players with a training platform in line with programs delivered to LaLiga youth players in Spain.  Advanced teams are by invitation only and all advanced players participate in competitive leagues throughout the season. <br><br>

                    The Advanced Squad program focuses on the LaLiga style of play, techniques, and philosophy and is delivered by LaLiga’s UEFA Pro certified coaches. With an integrated approach the program combines technical, tactical, physical, cognitive, emotional and social aspects of the game to give each player the skills and understanding to excel within the game context. Players receive the building blocks that will enable them to pursue football as student athletes. <br><br>

                    Training is 3 times per week plus a match day and includes:
                </p>

                <ul class="unstyled check-list mt-24">
                    <li>Physical, technical, and tactical drills on the field</li>
                    <li>Analytical game strategy</li>
                    <li>Activities to foster respect between players, to promote the awareness and acceptance of cultural diversity, and to inspire harmony between players on and off the pitch.</li>
                </ul>

                <p class="maindesc">Top talents from the Advanced Development squad may be scouted and selected for trials at the exclusive LaLiga High Performance Centre (HPC) throughout the season. Those selected for the HPC will train on additional days in other locations and participate in competitive leagues and tournaments and may earn a spot on the annual 15 days LaLiga Scouting camp in Spain.</p>

                <div class="row">
                    <div class="col-md-3">
                        <figure>
                            <img src="/assets-web/images/programs/tactical-cognitive.png" alt="LaLiga Academy">
                        </figure>
                    </div>

                    <div class="col-md-3">
                        <figure>
                            <img src="/assets-web/images/programs/technical-coordinative.png" alt="LaLiga Academy">
                        </figure>
                    </div>

                    <div class="col-md-3">
                        <figure>
                            <img src="/assets-web/images/programs/physical-conditional.png" alt="LaLiga Academy">
                        </figure>
                    </div>

                    <div class="col-md-3">
                        <figure>
                            <img src="/assets-web/images/programs/phychological.png" alt="LaLiga Academy">
                        </figure>
                    </div>
                </div>

            </article>
        </div>
    </section>


@endsection
