@extends('layouts.app')

@section('content')

    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/development-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Development</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">
                <h2 class="maintitle --black --small">
                    Development Program  (Girls &amp; Boys)
                </h2>

                <p class="maindesc">
                    The LaLiga Academy Development program is for open for all players from the age of 4 – 17 who are interested to learn football and improve their skills and fitness.  The program focuses on improving the individual technique of new aspiring players in terms of their game tactics and strategy while also introducing positions and teamwork. <br><br>

                    One of the distinguished features of LaLiga Academy is the personalization of the training sessions as the coaches evaluate the players throughout the season to identify the strengths and areas of improvement for each and every player. <br><br>

                    Participants can train for upto 3 times per week and enjoy friendly competitive matches throughout the season. The training program includes:
                </p>

                <ul class="unstyled check-list mt-24">
                    <li>Development of individual skills and technique</li>
                    <li>1 vs. 1 practice</li>
                    <li>Teamwork</li>
                    <li>Tactical comprehension exercises</li>
                </ul>

                <p class="maindesc">All LaLiga Academy Development squad players have the opportunity to be invited by their coaches to join the LaLiga Academy Advanced Development program*. The Advanced Development programs may require them to train on additional days in alternate locations and participate in competitive leagues and tournaments.</p>

                <div class="row">
                    <div class="col-md-3">
                        <figure>
                            <img src="/assets-web/images/programs/discover-football.png" alt="LaLiga Academy">
                        </figure>
                    </div>

                    <div class="col-md-3">
                        <figure>
                            <img src="/assets-web/images/programs/improve-skills.png" alt="LaLiga Academy">
                        </figure>
                    </div>

                    <div class="col-md-3">
                        <figure>
                            <img src="/assets-web/images/programs/socialize.png" alt="LaLiga Academy">
                        </figure>
                    </div>

                    <div class="col-md-3">
                        <figure>
                            <img src="/assets-web/images/programs/learn-game.png" alt="LaLiga Academy">
                        </figure>
                    </div>
                </div>

                <div class="bg-black mt-20 pt-10 pb-10">
                    <h4 class="fc-white tt-uppercase text-center">all ages and abilities</h4>
                </div>

                <p class="maindesc mt-10">
                    <small>*Participation in the LaLiga Advanced Development Programs will incur additional fees.</small>
                </p>
            </article>
        </div>
    </section>

@endsection
