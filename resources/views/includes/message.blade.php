{{-- session message --}}
@if(Session::has('success') || Session::has('error') || Session::has('info'))
@if(Session::has('success'))
<div class="alert --success" role="alert" id="successMessage">
    <strong>Success!</strong> {{ Session::get('success') }}
</div>
@elseif(Session::has('error'))
<div class="alert --danger" role="alert" id="errorMessage">
    <strong>Error!</strong> {{ Session::get('error') }}
</div>
@elseif(Session::has('info'))
<div class="alert --info" role="alert">
    <strong>Info!</strong> {{ Session::get('info') }}
</div>
@endif
@endif
{{--  End of message --}}