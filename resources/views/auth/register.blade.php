@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.app')
@section('head-scripts')

@section('content')
<head>
	<script src="https://www.google.com/recaptcha/api.js"></script>
	
</head>
<section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/registration-banner.jpg') }}')">
	<div class="inner-wrapper">
		<h1 class="title" id="help">Registration</h1>
	</div>
</section>
<section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
	<div class="container-wrapper">
		<article class="inner-content">
			<div class="row">
				<section class="col-lg-6 offset-lg-3">
					
					<div class="box --shadow-box --padding-box --bg-yellow">
						<form method="POST" action="{{ route('register') }}" class="default-form" id="register_form">
							@csrf
							<div class="control-group">
								<label class="form-label">{{ __('Guardian’s Full Name*') }}</label>
								<input id="name" type="text" class="form-field @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus maxlength="50">
								@error('name')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
							<div class="control-group">
                                <label class="form-label">{{ __('Guardian’s Relation with Player*') }}</label>
                                <input id="parent_1_relationship" type="text" class="form-field @error('parent_1_relationship') is-invalid @enderror" name="parent_1_relationship" value="{{ old('parent_1_relationship') }}" required autocomplete="parent_1_relationship" autofocus>
                                @error('parent_1_relationship')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="control-group">
                                <label class="form-label">{{ __('Guardian’s Mobile Number*') }}</label>
                                <input id="mobile" type="text" pattern="\d*" class="form-field" name="mobile" value="{{ old('mobile') }}" maxlength="10" required>
                                @error('mobile')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
                            </div>

                            <div class="control-group">
                                <label class="form-label">{{ __('Guardian’s Other Contact Number') }}</label>
                                <input name="parent_2_mobile"
                                id="parent_2_mobile" type="text" pattern="\d*" class="form-field"value="{{ old('parent_2_mobile') }}" maxlength="10">
                                @error('parent_2_mobile')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
                            </div>
							
							<div class="control-group">
								<label class="form-label">{{ __('Guardian’s E-Mail Address*') }}</label>
								<input id="email" type="email" class="form-field" name="email" value="{{ old('email') }}" required autocomplete="email">
								@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
							<div class="control-group">
								<label class="form-label">{{ __('Guardian’s Secondary E-Mail Address') }}</label>
								<input id="parent_2_email" type="email" class="form-field" 
								name="parent_2_email" value="{{ old('parent_2_email') }}" autocomplete="email">
								@error('parent_2_email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
							
							<div class="control-group">
								<label class="form-label">{{ __('Nationality*') }}</label>
								<select name="nationality" class="form-field" required>
									<option value="">Select Country</option>
									@foreach($arrays::countries() as $country)
									<option value="{{ $country }}" @if(old('nationality') == $country) selected="selected" @endif>{{ $country }}</option>
									@endforeach
									
							    </select>
							    @error('nationality')
								     <span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
							<div class="control-group">
								<label class="form-label">{{ __('Password*') }}</label>
								<input id="password" type="password" class="form-field" name="password" required autocomplete="new-password">
								@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
								
							</div>
							<div class="control-group">
								<label class="form-label">{{ __('Confirm Password*') }}</label>
								<input id="password-confirm" type="password" class="form-field" name="password_confirmation" required autocomplete="new-password">
								@error('password-confirm')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
							
							<div class="control-group">
								<label class="form-label">{{ __('How Did You Hear About Us*') }}</label>
								<select name="hear_about_us" class="form-field" required>
									<option value="">Select</option>
									<option value="1" @if(old('hear_about_us') == 1) selected="selected" @endif>TV/Radio/Cinema</option>
									<option value="2"  @if(old('hear_about_us') == 2) selected="selected" @endif>Social Media(Facebook,Twitter,Instagram,etc.)</option>
									<option value="3"  @if(old('hear_about_us') == 3) selected="selected" @endif>Online Search</option>
									<option value="4"  @if(old('hear_about_us') == 4) selected="selected" @endif>Family or Friend</option>
									<option value="5"  @if(old('hear_about_us') ==5) selected="selected" @endif>School</option>
									<option value="6"  @if(old('hear_about_us') == 6) selected="selected" @endif>Email or SMS</option>
									<option value="7"  @if(old('nationality') == 7) selected="selected" @endif>Others</option>
									</select>
							   @error('hear_about_us')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							 @enderror
							</div>
							<div class="control-group">
                            <div class="checkbox">
                                <input type="checkbox" name="liga_tnc" id="liga_tnc" class="team-chkbox" required>
                                <span class="label"></span>
                                <span class="text">
                                    I as parent or guardian of the Player identified herein, give my consent for the Player, to participate in LaLiga Academy, and have read and accept these
									
									<a href="{{route('terms.conditions')}}" target="_blank" class="fc-red">Terms and Conditions.</a>
								</span>
                            </div>
                        </div>
							<div class="control-group">
								 <div class="g-recaptcha" data-sitekey="6LfTmdUbAAAAAKQFZNIMSRDcl1xBLK4KP3n_tG4m" data-callback="correctCaptcha"></div>
							</div>
							 @error('g-recaptcha-response')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							 @enderror

							
							<div class="control-group mb-10">
								<button type="button" class="btn --btn-primary" id="register">
								{{ __('Register') }}
								</button>
							</div>
							<p class="maindesc mb-0">Already have an account? <a href="/login" class="fc-primary td-underline">Login</a></p>
						</form>
					</div>
				</section>
			</div>
		</article>
	</div>
</section>
<script type="text/javascript">

 $('#register').on('click',function(){
    var response = grecaptcha.getResponse();
     if (response.length == 0) {
          alert("You can't leave Captcha empty")
          return false;
        }
          //recaptcha passed validation
        else {
          $('#register_form').submit();
          return true;
        }
 });
  
</script>
@endsection