@extends('layouts.manager')
@inject('arrays','App\Http\Utilities\Arrays')
@section('content')
<h3 class="pagetitle">Edit Player</h3>
<section class="box">
	<div class="box-header">
        <div class="row align-items-center">
            <div class="col-8">
            </div>
            
            <div class="col-4">
                <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
        </div>
    </div>
	<div class="box-body">
		{!! Form::model($player, ['route' => ['admin.players.update', $player->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('admin.players._form',['submitBtnText' => 'Update'])
		{!! Form::close() !!}
	</div>
</section>
@endsection