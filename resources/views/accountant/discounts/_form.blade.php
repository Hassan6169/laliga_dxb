<div class="box-body">
    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
        
        {!! Form::label('name', 'Name', ['class'=>'form-label',]) !!}
        
        {!! Form::text('name', null, ['class'=>'form-field','required' => 'required']) !!}
        
        @if ($errors->has('name'))
        <span class="error-msg">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
     <div class="control-group{{ $errors->has('amount') ? ' has-error' : '' }}">
        
        {!! Form::label('amount', 'Amount', ['class'=>'form-label',]) !!}
        
        {!! Form::text('amount', null, ['class'=>'form-field','required' => 'required']) !!}
        
        @if ($errors->has('amount'))
        <span class="error-msg">
            <strong>{{ $errors->first('amount') }}</strong>
        </span>
        @endif
    </div>
    <div class="control-group{{ $errors->has('type') ? ' has-error' : '' }}">
        
        {!! Form::label('type','Type', ['class'=>'form-label']) !!}
        
        {!! Form::select('type', array('1' => 'Percentage', '2' => 'Less','3' => 'Additional Amount'),null,['class' => 'form-field']); !!}
        @if ($errors->has('type'))
        <span class="error-msg">
            <strong>{{ $errors->first('type') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('category') ? ' has-error' : '' }}">
        
        {!! Form::label('category','Category', ['class'=>'form-label']) !!}
        
        {!! Form::select('category',$categories,null,['class' => 'form-field']); !!}
        @if ($errors->has('category'))
        <span class="error-msg">
            <strong>{{ $errors->first('category') }}</strong>
        </span>
        @endif
    </div>
   
    <div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
        <div class="checkbox">
            {!! Form::checkbox('is_active', 1, isset($discount) ? $discount->is_active : true) !!}
            <span class="label"></span>
            <span class="text">Activate</span>
        </div>
        @if ($errors->has('is_active'))
        <span class="error-msg">
            <strong>{{ $errors->first('is_active') }}</strong>
        </span>
        @endif
    </div>
    <div class="control-group">
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small --btn-secondary fc-white">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
    </div>