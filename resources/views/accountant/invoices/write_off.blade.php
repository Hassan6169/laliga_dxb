@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Write off</h3>



<section class="box">

	<div class="box-body">

		{!! Form::model($invoice, ['route' => ['admin.invoices.write.off.update', $invoice->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

			

		<div class="control-group{{ $errors->has('write_off_amount') ? ' has-error' : '' }}">



			{!! Form::label('write_off_amount', 'Write Off Amount', ['class'=>'form-label',]) !!}



			{!! Form::text('write_off_amount', null, ['class'=>'form-field','required' => 'required']) !!}



			@if ($errors->has('write_off_amount'))

			<span class="error-msg">

				<strong>{{ $errors->first('write_off_amount') }}</strong>

			</span>

			@endif



		</div>



		<div class="control-group{{ $errors->has('write_off_amount_reason') ? ' has-error' : '' }}">



			{!! Form::label('write_off_amount_reason', 'Write Off Amount Reason', ['class'=>'form-label',]) !!}



			{!! Form::textarea('write_off_amount_reason', null, ['class'=>'form-field']) !!}



			@if ($errors->has('write_off_amount_reason'))

			<span class="error-msg">

				<strong>{{ $errors->first('write_off_amount_reason') }}</strong>

			</span>

			@endif



		</div>



		<div class="control-group mb-0">



			<input type="submit" value="Update" class="btn --btn-small bg-secondary fc-white">

			<a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>

			

		</div>



		{!! Form::close() !!}

	</div>

</section>

@endsection