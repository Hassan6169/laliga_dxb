<div class="control-group{{ $errors->has('emirate_id') ? ' has-error' : '' }}">
    {!! Form::label('emirate_id', 'Emirate', ['class' => 'control-label']) !!}
    {!! Form::select('emirate_id', $emirates,$term->pivot->emirate_id ?? null,['class' => 'form-field','placeholder'=>'Select Emirate']); !!}
    @if ($errors->has('emirate_id'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('emirate_id') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('term_id') ? ' has-error' : '' }}">
    {!! Form::label('term_id', 'Player Term', ['class' => 'control-label']) !!}
   
      {!! Form::select('term_id',$terms,$term->pivot->term_id ?? null,['class' => 'form-field','placeholder'=>'Select Term']); !!}
    @if ($errors->has('term_id'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('term_id') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('team_id') ? ' has-error' : '' }}">
    
    {!! Form::label('team_id', 'Select Team', ['class' => 'control-label']) !!}
    
    {!! Form::select('team_id',$teams,$term->pivot->team_id ?? null,['class' => 'form-field','placeholder'=>'Select Team']); !!}
    
    @if ($errors->has('team_id'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('team_id') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('kit_size') ? ' has-error' : '' }}">
    {!! Form::label('kit_size', 'Kit Size', ['class' => 'control-label']) !!}
    {!! Form::select('kit_size',$kit_size,$term->pivot->kit_size ?? null,['class' => 'form-field','placeholder'=>'Select Kit Size']); !!}
    @if ($errors->has('kit_size'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('kit_size') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('kit_status') ? ' has-error' : '' }}">
    {!! Form::label('kit_status', 'Kit Status', ['class' => 'control-label']) !!}
     {!! Form::select('kit_status',$kit_status,$term->pivot->kit_status ?? null,['class' => 'form-field','placeholder'=>'Select Kit Status']); !!}
    @if ($errors->has('kit_status'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('kit_status') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('match_kit') ? ' has-error' : '' }}">
    {!! Form::label('match_kit', 'Match Kit', ['class' => 'control-label']) !!}
    {!! Form::select('match_kit', array('1' => 'Applicable', '0' => 'Not Applicable'),$term->pivot->match_kit ?? null,['class' => 'form-field','placeholder'=>'Select Match Kit']); !!}
    @if ($errors->has('match_kit'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('match_kit') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('is_sponsored') ? ' has-error' : '' }}">
    {!! Form::label('is_sponsored', 'Is Sponsored', ['class' => 'control-label']) !!}
    {!! Form::select('is_sponsored', array('1' => 'Yes', '0' => 'No'),$term->pivot->is_sponsored ?? null,['class' => 'form-field','placeholder'=>'Select Sponsored']); !!}
    @if ($errors->has('is_sponsored'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('is_sponsored') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('payment_status') ? ' has-error' : '' }}">
    {!! Form::label('payment_status', 'Payment Status', ['class' => 'control-label']) !!}
    {!! Form::select('payment_status',$payment_status,$term->pivot->payment_status ?? null,['class' => 'form-field','placeholder'=>'Select Payment Status']); !!}
    @if ($errors->has('payment_status'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('payment_status') }}</strong>
    </span>
    @endif
</div>
<div class="control-group">
    <a href="#">
        <button type="submit" class="btn --btn-small bg-secondary fc-white" id="submit">{{ $submitBtnText }}</button>
    </a>
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
</div>