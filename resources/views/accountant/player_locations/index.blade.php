@php
if(!empty($sessions)){
foreach($sessions as $session){
foreach($session->players as $player)
{
$playerdata[]=$player->pivot->training_session_id.'-'.$player->pivot->day_id.'-'.$player->pivot->location_id;
}
}
}
@endphp
@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Player: {{$player->name}}</h3>
@include('includes.message')

<section class="box">
    <div class="box-header">
        <div class="text-right">
            <a href="{{ url()->previous() }}" class="btn --btn-small bg-secondary fc-white mb-0">Back</a>
        </div>
    </div>

    <div class="box-body">
        {{ Form::open(['route'=>['admin.player.training.session.update',$player->id,$term,$emirate]]) }}
    
        <table class="table --bordered --hover">
            <tr>
                <th>Location</th>
                @foreach($days as $day)
                <th>{{$day->name}}</th>
                @endforeach
            </tr>
            
            @foreach($locations as $location)
            <tr>
                <td>
                    {{$location->name}}
                </td>

                @foreach($days as $day)          
                <td>
                    @if(!empty($sessions))
                    @foreach($sessions as $session)
                    @if($session->day_id==$day->id && $session->location_id==$location->id)
                    @php $data=$session->id.'-'.$day->id.'-'.$location->id; @endphp
                    <input type="checkbox" name="days[]" value="{{$session->id}}-{{$day->id}}-{{$location->id}}" {{!empty($playerdata) && in_array($data,$playerdata) ? 'checked':''}}>
            
                    {{date('H:i',strtotime($session->start_time))}}-{{date('H:i',strtotime($session->end_time))}}

                    @endif
                    @endforeach
                    @endif
                </td>
                @endforeach
            </tr>
            @endforeach
        </table>
        
        <button type="submit" class="btn --btn-small bg-secondary fc-white">Submit</button>
        {!! Form::close() !!}
    </div>
</section>
@endsection