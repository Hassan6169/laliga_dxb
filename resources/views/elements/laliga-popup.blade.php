<section class="x-modal --popup-form hidden show">
	<div class="modal-content">
		<a href="javascript:;" class="close-btn modal-close">
			<i class="icon xicon-close"></i>
		</a>
		<div class="inner-content">
			<h2 class="title">Laliga Academy</h2>
			<img src="/assets-web/images/popup-banner.webp" alt="">
			<p class="subtitle">
				Book your free trial now! <br> Get 20% off on term and kit fees.
			</p>
			{!! Form::open(['route' => 'sign.up.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
			<div class="control-group mb-10">
				{!! Form::label('name', 'Name', ['class'=>'form-label']) !!}
				{!! Form::text('name', null, ['class'=>'form-field','required' => 'required','maxlength'=>'50','placeholder'=>'Enter your Name']) !!}
				@if ($errors->has('name'))
				<span class="form-error">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group mb-10">
				{!! Form::label('email', 'Email', ['class'=>'form-label']) !!}
				{!! Form::text('email', null, ['class'=>'form-field','required' => 'required', 'placeholder'=>'Enter your Email']) !!}
				@if ($errors->has('email'))
				<span class="form-error">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group mb-10">
				{!! Form::label('mobile', 'Mobile', ['class'=>'form-label']) !!}
				{!! Form::text('mobile', null, ['class'=>'form-field','required' => 'required','maxlength'=>'10','patter'=>'\d*', 'placeholder'=>'Enter Phone Number']) !!}
				@if ($errors->has('mobile'))
				<span class="form-error">
					<strong>{{ $errors->first('mobile') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group mb-10">
				<label class="form-label">DOB</label>
				
				{!! Form::date('dob', null, ['class'=>'form-field','required' => 'required','id'=>'player_dob', 'placeholder'=>'Player DOB','autocomplete'=>'off']) !!}
				@if ($errors->has('dob'))
				<span class="form-error">
					<strong>{{ $errors->first('dob') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group mb-10">
				{!! Form::label('term', 'Select term', ['class' => 'form-label']) !!}
				{{ Form::select('term_id',$terms,null, ['class' => 'form-field','placeholder'=>"Select Option"]) }}
				@if ($errors->has('term'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('term') }}</strong>
				</span>
				@endif
			</div>
			<div class="control-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
				<div class="control-group mb-10">
					<div class="g-recaptcha" id="recaptcha" data-sitekey=" 6LfTmdUbAAAAAKQFZNIMSRDcl1xBLK4KP3n_tG4m" data-callback="recaptcha_callback"></div>
					@if ($errors->has('g-recaptcha-response'))
					<span class="help-block">
						<strong>{{ $errors->first('g-recaptcha-response') }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="control-group mb-0">
				
				<input type="submit" id="register-btn" class="btn --btn-primary" value="Submit">
			</div>
			{!! Form::hidden('form_type_id',TYPE_POPUP_SIGNUP, ['class'=>'form-field']) !!}
			{!! Form::close() !!}
		</div>
	</div>
 <script>
	var dateToday = new Date();
    $( "#player_dob" ).datepicker({
        
    });
</script>
<script type="text/javascript">
	
   function recaptcha_callback(){
   	var registerBtn=document.querySelector('#register-btn');
   	registerBtn.removeAttribute('disabled');
   	registerBtn.style.cursor='pointer';
   }           
</script>
</section>