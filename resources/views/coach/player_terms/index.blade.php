@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Player Terms: {{$player->name}}</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-md-6">
                <a href="{{route('admin.players.term.create',[$player->id])}}" class="btn --btn-small bg-primary fc-white mb-0">Add Term</a>
            </div>

            <div class="col-md-6">
                <div class="text-right">
                    <a href="{{ route('admin.players.index') }}" class="btn --btn-small bg-secondary fc-white mb-0">Back</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="box-body">
        <table class="table --bordered --hover">
            <tr>
                <th>S NO</th>
                <th>Player Details</th>
                <th>Kit Status</th>
                <th>Wallet Amount</th>
                <th>Emirates</th>
                <th>Tranining Term</th>
                <th>Payment Status</th>
                <th>Action</th>
            </tr>
            
            @foreach($terms as $term)
            <tr>
                <td>
                    {{ $no++ }}
                </td>
                
                <td>
                    <strong>Orginal Category:</strong>
                    @if(!empty($player->category_id))
                        {{$player->orginalCategory->name}}
                    @endif
                    <br />
                    <strong>Changed Category:</strong>
                    @if(!empty($player->admin_category_id))
                        {{$player->changeCategory->name}} @else {{"None"}}
                    @endif
                    <br />
                    <strong>Goal Keeping Training:</strong>
                    @if(!empty($player->is_goal_keeper==1))
                        {{"Yes"}}
                    @else
                        {{"No"}}
                    @endif
                    <br />
                    <strong>Is Sponsored :</strong>
                    @if(!empty($term->pivot->is_sponsored==1))
                        {{"Yes"}}
                    @else
                        {{"No"}}
                    @endif
                    <br />
                </td>
                
                <td>
                    <strong>kit Size:</strong>
                    @foreach($kit_size as $key => $size)
                    @if($term->pivot->kit_size==$key)
                        {{$size}}
                    @endif
                    @endforeach
                    <br />
                    <strong>kit Received:</strong>
                    @foreach($kit_status as $key => $status)
                    @if($term->pivot->kit_status==$key)
                        {{$status}}
                    @endif
                    @endforeach
                    <br />
                    <strong>Match Kit:</strong>
                    @if(!empty($term->pivot->match_kit==1))
                        {{"Applicable"}}
                    @else
                        {{"Not Applicable"}}
                    @endif
                </td>
                
                <td>
                    @if(!empty($player->user->wallet))
                    {{$player->user->wallet->sum('total')}}
                    @else
                    {{0}}
                    @endif
                </td>
                
                <td>
                    {{$term->getEmirate($term->pivot->emirate_id)}}
                </td>
                
                <td>
                    {{$term->name}}
                </td>

                <td>
                    @foreach($payment_status as $key => $status)
                    @if($term->pivot->payment_status==$key)
                        {{$status}}
                    @endif
                    @endforeach
                </td>

                <td>

                    <a href="{{ route('admin.players.term.edit',[$player->id,$term->pivot->id]) }}" class="btn --btn-small bg-secondary fc-white">Edit</a>
                    <br />

                    <a href="{{route('admin.players.term.location',[$player->id,$term->id,$term->pivot->emirate_id]) }}" class="btn --btn-small bg-primary fc-white">Location</a>
                    <br />
                    <a href="{{route('admin.players.discount.index',[$player->id,$term->id])}}" class="btn --btn-small bg-warning fc-white">Discount</a>
                    <br />
                    <!-- <a href="{{route('admin.players.term.destroy',[$player->id,$term->pivot->id])}}"> </a> -->

                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white btn-sm" data-delete-id="{{ $term->id }}" id="deleteItem">Delete</button>
            
                </td>          
            </tr>
        
        @endforeach
        </table>
      
    </div>
</section>
{!! Form::open(['method' => 'get', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}

<script>
    var deleteResourceUrl = '{{ url()->current() }}';

    $(document).ready(function () {
        $('.deleteItem').click(function (e) {
        e.preventDefault();
        var deleteId = parseInt($(this).data('deleteId'));
        $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
            if (confirm('Are you sure?')) {
                $('#deleteForm').submit();
            }
        });
    });

</script>
@endsection