@extends('layouts.admin')

@section('content')

<h3 class="pagetitle">Edit Term</h3>

<section class="box">

 <div class="box-body">

    {!! Form::model($term, ['route' => ['admin.players.term.update',[$players->id,$id]], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('admin.player_terms._form',['submitBtnText' => 'Update'])

    {!! Form::close() !!}

  </div>

  

</section>

@endsection