@extends('layouts.coach')

@section('content')
    <h3 class="pagetitle">Dashboard</h3>

    <section class="mb-30">
        <div class="row">
            <div class="col-md-3">
                <div class="box --info-box">
		        	<span class="box-icon bg-info">
		        		<i class="xxicon icon-user"></i>
		        	</span>

                    <div class="box-content">
                        <span class="box-text">TOTAL SIGNUP</span>
                        <span class="box-number fc-info">1,410</span>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box --info-box">
		        	<span class="box-icon bg-warning">
		        		<i class="xxicon icon-user"></i>
		        	</span>

                    <div class="box-content">
                        <span class="box-text">CONTACTED</span>
                        <span class="box-number fc-warning">1,410</span>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box --info-box">
		        	<span class="box-icon bg-success">
		        		<i class="xxicon icon-user"></i>
		        	</span>

                    <div class="box-content">
                        <span class="box-text">CONVERTED</span>
                        <span class="box-number fc-success">1,410</span>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box --info-box">
		        	<span class="box-icon bg-danger">
		        		<i class="xxicon icon-user"></i>
		        	</span>

                    <div class="box-content">
                        <span class="box-text">REJECTED</span>
                        <span class="box-number fc-danger">1,410</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <p class="box-title">Users</p>
                </div>

                <div class="col-md-6">
                    <div class="default-form">
                        <div class="control-group --search-group mb-0 float-right">
                            <input type="text" placeholder="Search" class="search-field">
                            <div class="search-button">
                                <button type="submit" class="form-button">
                                    <i class="xxicon icon-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-body">
    </div>
    </section>
@endsection
