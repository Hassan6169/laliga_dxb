@extends('layouts.coach')
@inject('arrays','App\Http\Utilities\Arrays')
@section('content')
<h3 class="pagetitle">Edit  Comments</h3>
<section class="box">
	<div class="box-header">
        <div class="row align-items-center">
            <div class="col-8">
            </div>
            
            <div class="col-4">
                <a href="{{route('coach.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
        </div>
    </div>
	<div class="box-body">
		{!! Form::model($attendance, ['route' => ['coach.attendance.update', $player->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('coach.attendance._form',['submitBtnText' => 'Update'])
		{!! Form::close() !!}
	</div>
</section>
@endsection