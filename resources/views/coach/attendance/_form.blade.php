
<div class="control-group{{ $errors->has('is_present') ? ' has-error' : '' }}">
    <div class="checkbox">
        {!! Form::checkbox('is_present', 1, isset($attendance) ? $attendance->is_present : true) !!}
        <span class="label"></span>
        <span class="text">
            {!! Form::label('is_present', 'Present', ['class' => 'form-label']) !!}
            @if ($errors->has('is_present'))
        </span>
        
        <span class="error-msg">
            <strong>{{ $errors->first('is_present') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="col-3">
<div class="control-group{{ $errors->has('date') ? ' has-error' : '' }}">
    
    {!! Form::label('date', 'Date', ['class'=>'form-label']) !!}
    {!! Form::date('date', null, ['class'=>'form-field dob','required' => 'required','id'=>'date']) !!}
    @if ($errors->has('date'))
    <span class="error-msg">
        <strong>{{ $errors->first('date') }}</strong>
    </span>
    @endif
</div>
</div>
<div class="control-group">
    
    @if(!empty($attendance->coach_comments))
    @php $comments=json_decode($attendance->coach_comments,true); @endphp
    
    
    @foreach($comments as $key => $comment)
    @if($comment['id']==$player->id)
    {{ $comment['comments']}}
    <b>Posted By:</b>{{ $comment['admin_name']}}<br>
    <b>Updated At:</b>{{$comment['posted_on']}}<br>
    @endif
    @endforeach
    
    @endif
    
</div>
<div class="control-group{{ $errors->has('coach_comments') ? ' has-error' : '' }}">
    
    {!! Form::label('coach_comments', 'Coach Comments', ['class'=>'form-label',]) !!}
    {!! Form::textarea('coach_comments', null, ['class'=>'form-field']) !!}
    @if ($errors->has('coach_comments'))
    <span class="error-msg">
        <strong>{{ $errors->first('coach_comments') }}</strong>
    </span>
    @endif
</div>
<div class="control-group mn-0">
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white" id="submit">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
</div>
