<?php



// USER TYPE

define('USER_TYPE_ADMIN', 1);

define('USER_TYPE_MANAGER', 2);

define('USER_TYPE_ACCOUNTANT', 3);

define('USER_TYPE_COACH',4);

define('USER_TYPE_PARENT', 9);

define('TAX',5); // 5% VAT



// GENDER

define('GENDER_MALE', 1);

define('GENDER_FEMALE', 2);



//HPC

define('HPC_PLAYER', 1);

define('NOT_HPC_PLAYER',0);



define('GOAL_KEEPER', 1);

define('NOT_GOAL_KEEPER',0);



// STATUS

define('STATUS_ENABLED', 1);

define('STATUS_DISABLED', 0);

define('STATUS_OVERALL_INVOICE', 1);
define('STATUS_SIBLING_INVOICE', 2);
define('STATUS_TERM_INVOICE', 3);

define('STATUS_NOT_PAID',1);
define('STATUS_PARTIAL_PAYMENT',2);
define('STATUS_PAID',3);
define('STATUS_READY_TO_PAY',4);
define('STATUS_INVOICE_ACCEPTED',5);
define('STATUS_REFUNDED',6);
define('STATUS_PARTIAL_PAYMENT_PENDING',7);
define('STATUS_PAYMENT_DEFAULTED',8);
define('STATUS_SPONSORED',9);
define('STATUS_MAKE_UP',10);
define('STATUS_PENDING',11);
define('STATUS_WAIVE_OFF',12);



define('WALLET_STATUS_USED', 1);

define('WALLET_STATUS_NOT_USED', 2);

define('WALLET_STATUS_CANCEL', 3);



define('AMOUNT_TYPE_DEBIT', 1);

define('AMOUNT_TYPE_CREDIT', 2);



define('SEASON_TERM', 1);

define('CAMP_WITHDAYS', 2);

define('CAMP_WITHOUTDAYS', 3);



define('TYPE_POPUP_SIGNUP', 1);
define('TYPE_ENQUIRY', 3);

define('CUSTOM_INVOICE_EMAIL', 1);
define('POPUP_SIGNUP_EMAIL', 2);