<?php

use Illuminate\Database\Seeder;
use App\Emirate;

class EmiratesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Emirate::create(['name' => 'Abu Dhabi']);
        Emirate::create(['name' => 'Al Ain']);
        Emirate::create(['name' => 'Northern Emireates']);
        Emirate::create(['name' => 'Dubai']);
        Emirate::create(['name' => 'Sharjah']);
    }
}
