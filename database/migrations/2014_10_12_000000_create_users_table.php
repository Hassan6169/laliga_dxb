<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('mobile')->nullable();
            $table->tinyInteger('type')->default(USER_TYPE_PARENT);
            $table->string('parent_1_relationship')->nullable();
            $table->string('parent_2_name')->nullable();
            $table->string('parent_2_email')->nullable();
            $table->string('parent_2_mobile')->nullable();
            $table->string('parent_2_relationship')->nullable();
            $table->text('admin_comments')->nullable();
            $table->string('nationality')->nullable();
            $table->string('hear_about_us')->nullable();
            $table->tinyInteger('is_active')->default(STATUS_ENABLED);
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
