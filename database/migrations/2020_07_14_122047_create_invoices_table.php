<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->integer('player_id');
            $table->integer('term_id');
            $table->integer('parent_id');
            $table->integer('category_id');
            $table->text('session_details')->nullable();
            $table->text('discount_details')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->float('amount')->nullable();
            $table->integer('installment_id')->nullable();
            $table->float('kit_amount')->nullable();
            $table->float('tax_amount')->nullable();
            $table->float('pay_amount')->nullable();
            $table->float('total_amount')->nullable();
            $table->float('write_off_amount')->nullable();
            $table->text('write_off_amount_reason')->nullable();
            $table->float('partial_amount')->nullable();
            $table->text('partial_amount_reason')->nullable();
            $table->integer('partial_payment_type')->nullable();
            $table->integer('payment_method')->nullable();
            $table->integer('payment_status')->default(STATUS_PENDING);
            $table->tinyinteger('is_wallet_used')->nullable();
            $table->integer('is_custom')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
