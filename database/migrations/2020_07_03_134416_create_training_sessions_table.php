<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('training_sessions', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->unsignedBigInteger('term_id');
        $table->unsignedBigInteger('category_id');
        $table->unsignedBigInteger('location_id');
        $table->unsignedBigInteger('day_id');
        $table->time('start_time');
        $table->time('end_time');
        $table->tinyInteger('for_goal_keeper');
        $table->integer('cap_a')->nullable();
        $table->integer('cap_b')->nullable();
        $table->integer('cap_c')->nullable();
        $table->tinyInteger('is_active')->default(STATUS_ENABLED);
        $table->tinyInteger('is_full')->nullable();
        $table->softDeletes();
        $table->timestamps();
        
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_sessions');
    }
}
