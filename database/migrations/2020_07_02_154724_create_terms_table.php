<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('type_id');
            $table->float('kit_amount')->nullable();
            $table->tinyInteger('yearly')->default(STATUS_DISABLED);
            $table->integer('total_sessions')->nullable();
            $table->integer('weeks')->nullable();
            $table->integer('level')->nullable();
            $table->unsignedBigInteger('season_id');
            $table->tinyInteger('is_show_to_admin')->default(STATUS_ENABLED);
            $table->tinyInteger('is_active')->default(STATUS_ENABLED);
            $table->softDeletes();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms');
    }
}
