<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->date('dob')->nullable();
            $table->tinyInteger('gender')->default(GENDER_MALE);
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('school')->nullable();
            $table->string('nationality')->nullable();
            $table->string('previous_football_academy')->nullable();
            $table->string('photo')->nullable();
            $table->string('medical_insurance')->nullable();
            $table->string('emirates_id_front')->nullable();
            $table->string('emirates_id_back')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('sibling_no');
            $table->integer('admin_category_id')->nullable();
            $table->text('admin_comments')->nullable();
            $table->tinyInteger('is_hpc_player')->default(STATUS_DISABLED);
            $table->tinyInteger('is_goal_keeper')->default(STATUS_DISABLED);
            $table->tinyInteger('is_active')->default(STATUS_ENABLED);
            $table->softDeletes();
            $table->timestamps();
           
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
