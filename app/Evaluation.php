<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    public function player()
    {
        return $this->belongsTo('App\Player');
    }

    public function term()
    {
        return $this->belongsTo('App\Term');
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function category()
    {
        return $this->belongsTo('App\Location');
    }
}
