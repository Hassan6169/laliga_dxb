<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $email_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_data)
    {
        $this->email_data = $email_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->email_data['email_type']==REGISTER_EMAIL) {
            return $this->subject('Registration Details')->view('emails.register')->with('data', $this->email_data);
        }else{
            return $this->subject('Popup Details')->view('emails.popup_details')->with('data', $this->email_data);
        }
    }
}
