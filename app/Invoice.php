<?php



namespace App;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Invoice extends Model

{
    use SoftDeletes;
    protected $dates = ['deleted_at'];  

   public function player()

    {

         return $this->belongsTo('App\Player');

    }
    

   public function term()

    {

         return $this->belongsTo('App\Term');

    }



    public function trainingSession($id)

    {

        return TrainingSession::where('id',$id)->first();

    }


    public function user()

    {

        return $this->belongsTo('App\User');

    }



    public function location()

    {

        return $this->belongsTo('App\Location');

    }



    public function day($id)

    {

    return Day::where('id',$id)->pluck('name')->first();   

    }


     public function getLocation($id){

      return Location::where('id',$id)->pluck('name')->first();   

    }

     public function getTerm($id){    
        // return Term::where('id',$id)->first();   
       $tmp = Term::where('id',$id)->first();
    //    print_r($tmp);
    //    exit(0);   
    }

    public function getPlayer($id){

      return Player::where('id',$id)->first();   

    }

    public function installment(){

    return $this->belongsTo('App\InvoiceInstallment');

    }

    public function getDayLiteralAttribute($id){
       return Day::where('id',$id)->pluck('name')->first();  
    }


}

