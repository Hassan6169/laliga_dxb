<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
   use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function terms()
    {
        return $this->hasMany('App\Term');
    } 
}
