<?php



namespace App;



use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model

{

     use SoftDeletes;



    public function players()

    {

        return $this->belongsToMany('App\Player','player_discount')->withTimestamps()->withPivot(['term_id']);

    }

}

