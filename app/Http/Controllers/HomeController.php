<?php



namespace App\Http\Controllers;


use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Location;
use App\Season;
use App\Term;



class HomeController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

//        $this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Contracts\Support\Renderable

     */

    public function index(Request $request){

    $terms=Term::where('is_active',STATUS_ENABLED)->pluck('name','id');
    return view('home',['terms'=>$terms]);

    }

    public function returnpage(Request $request)
    {
        //  return $request;
        $formdata=$request->input();
        //  return $formdata;
         
         $payment = new Payment;
         $payment->user_id=$request->merchant_extra;
         $payment->invoice_id=$request->merchant_extra2;
         $payment->payfort_id=$request->fort_id;
         $payment->merchant_reference=$request->merchant_reference;
         $payment->amount=$request->amount;
         $payment->response= json_encode($formdata);
         $payment->type="Online";
         $payment->save();

         $fort_id=$request->fort_id;
        
         if(!empty($fort_id)){
         $merchant_extra=$request->merchant_extra;
         $merchant_extra2=$request->merchant_extra2;
         $invoice22 =Invoice::where('id',$merchant_extra2)->first();
         $invoice22->payment_status=STATUS_PAID;
         $invoice22->save();
         }

         return redirect()->route('account.dashboard');

        // return view('pages.returnpage');
    }
    

    /** About LaLiga Section */

    public function about(Request $request)

    {

        return view('pages.about');

    }
     public function laligaTerms(Request $request)

    {

        return view('pages.terms_conditions');

    }


    public function coaches(Request $request)

    {

        return view('pages.coaches');

    }



    public function photos(Request $request)

    {

        return view('pages.photos');

    }



    public function videos(Request $request)

    {

        return view('pages.videos');

    }





    /** Programs Section */

    public function methodologyProgram(Request $request)

    {

        return view('pages.methodology_program');

    }



    public function developmentProgram(Request $request)

    {

        return view('pages.development_program');

    }



    public function advancedDevelopmentProgram(Request $request)

    {

        return view('pages.advanced_development_program');

    }



    public function laligaHpcProgram(Request $request)

    {

        return view('pages.laliga_hpc_program');

    }



    public function summerProgram(Request $request)

    {

        return view('pages.summer_program');

    }





    /** Scouting Section */

    public function laligaTrials(Request $request)

    {

        return view('pages.laliga_trials');

    }



    public function contractAwards(Request $request)

    {

        return view('pages.contract_awards');

    }



    public function soccerScholarship(Request $request)

    {

        return view('pages.soccer_scholarship');

    }





    /** Schedule Section */

    public function developmentSchedule(Request $request)

    {

        return view('pages.development_schedule');

    }



    public function advancedSchedule(Request $request)

    {

        return view('pages.advanced_schedule');

    }



    public function trainingLocations(Request $request)

    {
        $locations=Location::where('is_active',STATUS_ENABLED)->get();
        return view('pages.training_locations',['locations'=>$locations]);

    }





    /** Extra Section */

    public function fees(Request $request)

    {

        return view('pages.fees');

    }



    public function faq(Request $request)

    {

        return view('pages.faq');

    }



    public function policy(Request $request)

    {

        return view('pages.privacy_policy');

    }



    public function terms(Request $request)

    {

        return view('pages.terms_conditions');

    }



    public function registerTermsConditions(Request $request)

    {

        return view('pages.register_terms_conditions');

    }





    /* Dashboard Paged */



    public function adminDashboard(Request $request)

    {
        print_r('Hassan Tesitng on Home Controller');
        exit(0);
        
        $seasons=Season::orderByDesc('id')->get();
        return view('admin.dashboard',['seasons'=>$seasons]);

    }



    public function coachDashboard(Request $request)

    {

        return view('coach.dashboard');

    }



    public function parentDashboard(Request $request)

    {

        return view('account.dashboard');

    }



    public function parentProfileEdit(Request $request)

    {

        return view('account.profile.edit');

    }

    public function changePassword(){

        return view('account.profile.change_password');

    }

    public function updatePassword(Request $request){

        $request->validate([
         
          'password' => ['required','confirmed'],
         
        ]);
        $user = \App\User::getAuthenticatedUser();

        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('account.dashboard')->with('success','Password has been updated.');

    }



    public function parentProfileUpdate(Request $request)

    {
        $request->validate([
         
          'name' => ['required','max:50','regex:/^[\pL\s\-]+$/u'],
          'parent_2_email' => ['nullable','string', 'email'],
          'mobile' => ['required','digits:10'],
          'parent_2_mobile' => ['nullable','digits:10'],
         
        ]);
        $user = \App\User::getAuthenticatedUser();

        $user->name = $request->name;
        $user->parent_2_email = $request->parent_2_email;
        $user->parent_2_mobile = $request->parent_2_mobile;
        $user->mobile = $request->mobile;
        $user->nationality = $request->nationality;
        $user->save();

        return redirect()->route('account.dashboard')->with('success','Profile has been updated.');

    }

}

