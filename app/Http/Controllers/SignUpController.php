<?php



namespace App\Http\Controllers;



use App\SignUp;

use App\Term;

use App\AgeCategory;

use App\User;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Carbon\Carbon;

use Auth;



class SignUpController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index($id)

    {

       if($id==1)

       {

        $heading="Popup SignUp Form";

       }

       elseif($id==2){

        $heading="Trial SignUp Form";

       }

       elseif($id==3)

       {

       $heading="Enquiry Form";

       }

       $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();

       $sources=[1=>"Website-Incomming Call",2=>"Website-Pop ups",

       3=>"Website-Zendesk Chat"];

       $status=[1=>"New Enquiry",2=>"Contacted",3=>"Follow Up",4=>"Waiting List",5=>"Trial",6=>"Call Not Pick",7=>"Converted",8=>"Rejected",9=>"Parked"];

        $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();

        $users=User::where('is_active',STATUS_ENABLED)

        ->where('type',USER_TYPE_MANAGER)->get();

       $sign_ups=SignUp::where('form_type_id',$id)->get();

       return view('admin.sign_ups.index',['no'=>1,'sign_ups'=>$sign_ups,

        'heading'=>$heading,'id'=>$id,'status'=>$status,'sources'=>$sources,'terms'=>$terms,'users'=>$users,'categories'=>$categories,]);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create($id)

    {

        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();

        $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();

        $users=User::where('is_active',STATUS_ENABLED)->where('type',USER_TYPE_ADMIN)->get();

        $sources=[1=>"Website-Incomming Call",2=>"Website-Pop ups",3=>"Website-Zendesk Chat"];

        $status=[1=>"New Enquiry",2=>"Contacted",3=>"Follow Up",4=>"Waiting List",5=>"Trial",6=>"Call Not Pick",7=>"Converted",8=>"Rejected",9=>"Parked"];

        return view('admin.sign_ups.create',['categories'=>$categories,'terms'=>$terms,'sources'=>$sources,'users'=>$users,'status'=>$status,'id'=>$id]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

       $this->validateSignUp($request);

       // if(isset($request['g-recaptcha-response']) && !empty($request['g-recaptcha-response'])){
            
       //      $secret=env("NOCAPTCHA_SECRET");
           
       //      $verifyResponse =   file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$request['g-recaptcha-response']);
       //      $responseData = json_decode($verifyResponse);
       //      if($responseData->success){

        $sign_up = new SignUp();

        $sign_up->name = $request->name;

        $sign_up->email = $request->email;

        $sign_up->mobile = $request->mobile;

        $sign_up->source_id = $request->source_id;

        $sign_up->status_id = $request->status_id;

        $sign_up->term_id = $request->term_id;

        $sign_up->category_id = $request->category_id;

        $sign_up->user_id = $request->user_id;

        $sign_up->form_type_id = $request->form_type_id;

        $sign_up->comments = $request->comments;

        $sign_up->dob =Carbon::parse($request->dob)->format('Y-m-d');

        $sign_up->is_active = $request->input('is_active') !== null ? 1 : 0;;

        $sign_up->save();
        return redirect()->route('home')->with('success', 'Added Successfully');

    //     }
    //     else{
    //     return redirect()->route('home')->with('error', 'Failed');
    //     }
    // }

        

    }



    /**

     * Display the specified resource.

     *

     * @param  \App\SignUp  $signUp

     * @return \Illuminate\Http\Response

     */

    public function search($id,Request $request)

    {

        

        $keyword= $request->keyword;

        $track_no = $request->track_no;

        $created_at=$request->created_at;

        $created_to=$request->created_to;

        $user_ids = $request->user_id;

        $comments = $request->comments;

        $status_ids = $request->status_id;

        $category_ids = $request->category_id;

        $source_ids = $request->source_id;

        $term_ids = $request->term_id;


        $search_query = SignUp::where('is_active', STATUS_ENABLED);

        if (!empty($keyword)) {

            $search_query = $search_query->where('name', 'LIKE', '%'.$keyword.'%');

            }
   
        

        if (!empty($track_no )) {

            $search_query = $search_query->where('id', $track_no);

        }

        

        if(!empty($created_to) && !empty($created_at)) {

             $created_at=Carbon::createFromFormat('m/d/Y',$request->created_at)->format('Y-m-d');
             $created_to=Carbon::createFromFormat('m/d/Y',$request->created_to)->format('Y-m-d');
            $search_query = $search_query->where('created_at', '>=', $created_at)->where('created_at', '<=', $created_to);

        }

        

         if (!empty($created_to)) {
          $created_to=Carbon::createFromFormat('m/d/Y',$request->created_to)->format('Y-m-d');
          $search_query = $search_query->where('created_at',$created_to);

        }

        if (!empty($created_at)) {
            
            $created_at=Carbon::createFromFormat('m/d/Y',$request->created_at)->format('Y-m-d');
            $search_query = $search_query->where('created_at',$created_at);
        }



        if (!empty($user_ids)) {

        $search_query = $search_query->whereIn('user_id',[$user_ids]);

        }



         if(!empty($status_ids)) {

         $search_query = $search_query->whereIn('status_id',[$status_ids]);

        }



         if (!empty($source_ids)) {

        $search_query = $search_query->whereIn('source_id',[$source_ids]);

        }



        if (!empty($term_ids)) {

        $search_query = $search_query->whereIn('term_id',[$term_ids]);

        }



         if(!empty($category_ids)) {

         $search_query = $search_query->whereIn('category_id',[$category_ids]);

        }



        if($comments==1) {

           

         $search_query = $search_query->whereNotNull('comments');

        }

        if($comments==2){

          

           $search_query = $search_query->whereNull('comments'); 

        }

        

        $sign_ups = $search_query->where('form_type_id',$id)->get();



       if($id==1)

       {

        $heading="Popup SignUp Form";

       }

       elseif($id==2){

        $heading="Trial SignUp Form";

       }

       elseif($id==3)

       {

       $heading="Enquiry Form";

       }

       $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();

       $sources=[1=>"Website-Incomming Call",2=>"Website-Pop ups",

       3=>"Website-Zendesk Chat"];

       $status=[1=>"Ne Enquiry",2=>"Contacted",3=>"Follow Up",4=>"Waiting List",5=>"Trial",6=>"Call Not Pick",7=>"Converted",8=>"Rejected",9=>"Parked"];

        $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();

        $users=User::where('is_active',STATUS_ENABLED)

        ->where('type',USER_TYPE_ADMIN)->get();

       return view('admin.sign_ups.index',['no'=>1,'heading'=>$heading,'id'=>$id,'status'=>$status,'sources'=>$sources,'terms'=>$terms,'users'=>$users,'categories'=>$categories,'sign_ups'=>$sign_ups,'request'=>$request]);

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\SignUp  $signUp

     * @return \Illuminate\Http\Response

     */

    public function register(SignUp $signUp)

    {



        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();

        $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();

        $users=User::where('is_active',STATUS_ENABLED)->where('type',USER_TYPE_ADMIN)->get();

        $sources=[1=>"Website-Incomming Call",2=>"Website-Pop ups",3=>"Website-Zendesk Chat"];

        $status=[1=>"New Enquiry",2=>"Contacted",3=>"Follow Up",4=>"Waiting List",5=>"Trial",6=>"Call Not Pick",7=>"Converted",8=>"Rejected",9=>"Parked"];

        return view('admin.sign_ups.edit',['categories'=>$categories,'terms'=>$terms,'sources'=>$sources,'users'=>$users,'status'=>$status,'signUp'=>$signUp]); 

    }
    
    public function edit(SignUp $signUp){

        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();

        $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();

        $users=User::where('type',USER_TYPE_MANAGER)->pluck('name','id');
       
        $sources=[1=>"Website-Incomming Call",2=>"Website-Pop ups",3=>"Website-Zendesk Chat"];

        $status=[1=>"New Enquiry",2=>"Contacted",3=>"Follow Up",4=>"Waiting List",5=>"Trial",6=>"Call Not Pick",7=>"Converted",8=>"Rejected",9=>"Parked"];

        return view('admin.sign_ups.edit_details.edit',['categories'=>$categories,'terms'=>$terms,'sources'=>$sources,'users'=>$users,'status'=>$status,'signUp'=>$signUp]); 

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\SignUp  $signUp

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, SignUp $signUp)

    {



        $signUp->name = $request->name;

        $signUp->email = $request->email;

        $signUp->mobile = $request->mobile;

        $signUp->source_id = $request->source_id;

        $signUp->status_id = $request->status_id;

        $signUp->term_id = $request->term_id;

        $signUp->category_id = $request->category_id;

        $signUp->user_id = $request->user_id;

        $signUp->comments = $request->comments;

        $signUp->dob =Carbon::parse($request->dob)->format('Y-m-d');

        $signUp->is_active = $request->input('is_active') !== null ? 1 : 0;;

        $signUp->save();



        return redirect()->route('admin.signups.index',[$signUp->form_type_id])->with('success', 'Added Successfully');

    }
     

     public function updateSignup(Request $request, SignUp $signUp){

        
         $request->validate([
          'source_id' => ['required'],
          'status_id' => ['required'],
          'user_id' => ['required'],
        ]);
        $data=[];

        if(!empty($signUp->comments)){
         $comments=json_decode($signUp->comments,true);
         foreach($comments as $key=>$commentAdmin) {
            $data[]=['comments'=>$commentAdmin['comments'],'admin_name'=>$commentAdmin['admin_name'],'posted_on'=>$commentAdmin['posted_on'],'id'=>$commentAdmin['id']];
         }
        }
        if(!empty($request->comment)){
        $data[]=['comments'=>$request->comment,'admin_name'=>Auth::user()->name,'posted_on'=>date('Y-m-d'),'id'=>$signUp->id
        ]; 	
       }
       
    
        $signUp->source_id = $request->source_id;
        $signUp->status_id = $request->status_id;
        $signUp->user_id = $request->user_id;
        $signUp->comments = json_encode($data);
        $signUp->save();



        return redirect()->route('admin.signups.index',[$signUp->form_type_id])->with('success', 'Edit Successfully');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\SignUp  $signUp

     * @return \Illuminate\Http\Response

     */

    public function destroy($id,SignUp $signUp)

    {

       

       $signUp->delete();

        return redirect()->route('admin.signups.index',[$id])->with('success', 'Deleted Successfully');

    }



     private function validateSignUp(Request $request) {

       

        $request->validate([

            'name'  => 'required|max:100',

            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],

            'mobile'  => 'required|max:10',

            'dob'  => 'required',

            'term_id'  => 'required',
            'g-recaptcha-response'=>'required',

        ]);

    }

}

