<?php

namespace App\Http\Controllers;

use App\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news=News::all();
        $sessions=[1=>'2020/2021',2=>'2019/2020',3=>'2018/2019',4=>'2018/2019'];
        return view('admin.news.index',['news'=>$news,'sessions'=>$sessions,'no'=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    
         $new_type=[1=>'Video',2=>'Text'];
         $sessions=[1=>'2020/2021',2=>'2019/2020',3=>'2018/2019',4=>'2018/2019'];
         $news_events=[1=>'LaLiga Academy Launch'];
         return view('admin.news.create',['new_type'=>$new_type,'sessions'=>$sessions,'news_events'=>$news_events]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateNews($request);
        $news = new News();
        $news->name_eng = $request->name_eng;
        $news->news_type_id = $request->new_type;
        $news->slug = Str::slug($request->name_eng, '-');
        $news->news_source_eng = $request->news_source_eng;
        $news->session_id = $request->session_id;
        $news->news_event_id = $request->news_events_id;
        $news->details_eng = $request->details_eng;
        $news->name_ar = $request->name_ar;
        $news->details_ar = $request->details_ar;
        $news->news_source_ar = $request->news_source_ar;
        $news->news_source_ar = $request->news_source_ar;
        $news->external_link= $request->external_link;
        $image= $request->file('image');
        if(!empty($image)){
            $extension  = $image->getClientOriginalExtension();
            $news_image= $request->name_eng . mt_rand(11, 99) . mt_rand(11, 99). '.' . $extension;
            $file_path = $image->storeAs('public/news_images/',$news_image);
            $news->image = $news_image;
        }
        $news->is_active = $request->input('is_active') !== null ? 1 : 0;;
        $news->save();

        return redirect()->route('admin.news.index')->with('success', 'News Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
       $new_type=[1=>'Video',2=>'Text'];
       $sessions=[1=>'2020/2021',2=>'2019/2020',3=>'2018/2019',4=>'2018/2019'];
       $news_events=[1=>'LaLiga Academy Launch'];
         return view('admin.news.edit',['new_type'=>$new_type,'sessions'=>$sessions,'news_events'=>$news_events,'news'=>$news]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $this->validateNews($request);
        $news->name_eng = $request->name_eng;
        $news->news_type_id = $request->new_type;
        $news->slug = Str::slug($request->name_eng, '-');
        $news->news_source_eng = $request->news_source_eng;
        $news->session_id = $request->session_id;
        $news->news_event_id = $request->news_events_id;
        $news->details_eng = $request->details_eng;
        $news->name_ar = $request->name_ar;
        $news->details_ar = $request->details_ar;
        $news->news_source_ar = $request->news_source_ar;
        $news->news_source_ar = $request->news_source_ar;
        $news->external_link= $request->external_link;
        $image= $request->file('image');
        if(!empty($image)){
            $extension  = $image->getClientOriginalExtension();
            $news_image= $request->name_eng . mt_rand(11, 99) . mt_rand(11, 99). '.' . $extension;
            $file_path = $image->storeAs('public/news_images/',$news_image);
            $news->image = $news_image;
        }
        $news->is_active = $request->input('is_active') !== null ? 1 : 0;;
        $news->save();

        return redirect()->route('admin.news.index')->with('success', 'News Added Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return redirect()->route('admin.news.index')->with('success', 'News Deleted Successfully');
    }

    private function validateNews(Request $request) {
       
        $request->validate([
             'name_ar'  => 'required|max:100',
             'name_eng'  => 'max:100',
             'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    }
}
