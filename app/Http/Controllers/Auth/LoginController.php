<?php



namespace App\Http\Controllers\Auth;



use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Providers\RouteServiceProvider;

use Illuminate\Foundation\Auth\AuthenticatesUsers;



class LoginController extends Controller

{

    /*

    |--------------------------------------------------------------------------

    | Login Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles authenticating users for the application and

    | redirecting them to your home screen. The controller uses a trait

    | to conveniently provide its functionality to your applications.

    |

    */



    use AuthenticatesUsers;



    /**

     * Where to redirect users after login.

     *

     * @var string

     */

    protected $redirectTo = RouteServiceProvider::HOME;



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {
        
        $this->middleware('guest')->except(['redirectToUserHome', 'logout']);

    }



    public function redirectToUserHome(){
        if(Auth::check()){
            
            if(Auth::user()->type == USER_TYPE_ADMIN){

                $dashboard_route = 'admin.dashboard';

            } elseif(Auth::user()->type == USER_TYPE_PARENT){
            
                $dashboard_route = 'account.dashboard';

            }

            elseif(Auth::user()->type == USER_TYPE_MANAGER){
                
                $dashboard_route = 'manager.dashboard';
            }

             elseif(Auth::user()->type == USER_TYPE_ACCOUNTANT){

                $dashboard_route = 'accountant.dashboard';
            }

            elseif(Auth::user()->type == USER_TYPE_COACH){

                $dashboard_route = 'coach.dashboard';

            

        } else {

            $dashboard_route = 'login';

        }
        return redirect()->route($dashboard_route);
    }

  }

}

