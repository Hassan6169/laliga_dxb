<?php



namespace App\Http\Controllers;

use App\InvoiceRequest;
use App\User;
use App\TrainingSession;
use Illuminate\Http\Request;
use Auth;
use App\AgeCategory;
use App\Day;
use App\Location;
use App\Invoice;
use App\Term;
use App\InvoiceInstallment;
use App\Fee;
use App\Player;
use App\Http\Utilities\Payfort;
use App\Http\Utilities\Email;
use App\Wallet;
use Session;
use PDF;
use Carbon\Carbon;
use App\Team;
use Mail;
use App\Mail\CustomInvoice;
use DB;

class InvoiceController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

      $data=[];

      if(Auth::user()->type==USER_TYPE_ADMIN){
      
      $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();
      $locations=location::where('is_active',STATUS_ENABLED)->get();
      $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();
      
      // DB::enableQueryLog();
      $invoices =Invoice::where('parent_id',0)->get();
      // Your Eloquent query executed by using get()
      // $qu = DB::getQueryLog();
      // dd(end($qu)); // Show results of log
      

      $paymentMethods=[''=>"Select Method",1=>'Cash',2=> 'Cheques',3=>'Credit',4=>'Cardbank',5=>'Transfer',6=>'Transfer',7=>'Online'];
      $paymentStatus1=[''=>"Select Status",1=>'Not Paid',2=>'Partial Payment',3=>'Paid',4=>'Ready to Paid',5=> 'Invoice Accepted ',6=>'Refunded',7=>'Partial Payment Pending',8=> 'Payment Defaulted ',9=>'Sponsored',10=>'Makeup',11=>'Pending'];
       $paritalPaymentType=[''=>"Select Type",2=>'2 payments',3=>'3 payments',4=>'4 payments',5=>'5 payments'];

      //  print("<pre>".print_r($invoices,true)."</pre>");
      //  exit(0);

      // DB::enableQueryLog(); // Enable query log
      // // Your Eloquent query executed by using get()
      // dd(DB::getQueryLog()); // Show results of log

      return view('admin.invoices.index',['invoices'=>$invoices,'data'=>$data,'locations'=>$locations,
      'categories'=>$categories,'terms'=>$terms,'paymentMethods'=>$paymentMethods,'paymentStatus1'=>$paymentStatus1,'paritalPaymentType'=>$paritalPaymentType]);

      }

      else if(Auth::user()->type==USER_TYPE_PARENT){

      $days=Day::all();  

      $invoice_request =Invoice::where('user_id',Auth::user()->id)->where('parent_id',0)->get();




      return view('account.invoices.player_invoices',['invoice_request'=>$invoice_request,'no'=>1,'days'=>$days,'data'=>$data]);

      }

    }

     public function indexPlayerInvoice(Player $player){
      $data=[];
      $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();
      $invoices=Invoice::where('user_id',Auth::user()->id)->get();
      return view('admin.invoices.index',['invoices'=>$invoices,'data'=>$data,'categories'=>$categories]);
    }

    public function playerTermInvoice(Player $player,Term $term){
     

      $data=array();
      $date=date('Y-m-d');
      $invoices=Invoice::where('player_id',$player->id)->
      where('term_id',$term->id)->first();

      if(empty($invoices)){
        return redirect()->route('admin.players.term',[$player->id])->with('error',"Invoice not created yet");
      }
      
      $session_details=json_decode($invoices->session_details,true);
           foreach ($session_details  as $key => $detail) {
             
            if($invoices->player_id==null){
               $get_player=$detail['player_id'];
               $term_id=$detail['term_id'];
            }
            else{
               $get_player=$invoices->player_id;
               $term_id=$invoices->term_id;
            }
            
            $term =Term::find($term_id);
           
            $player=Player::where('id',$get_player)->first();
             if($term->type_id==CAMP_WITHOUTDAYS){
             
             $data[$player->id]['name']=$player->name;
             $data[$player->id]['guardian_name']=$player->user->name;
             $data[$player->id]['category']=$player->orginalCategory->name;
             $data[$player->id]['term_name']=$term->name;
             $data[$player->id]['season_name']=$term->season->name;
             $data[$player->id]['location']= "ALL DAYS";
             $data[$player->id]['no_of_trainings']= "0 DAYS";
             $data[$player->id]['start_end_time'][""]="";
             
            }
           elseif($term->type_id==SEASON_TERM || $term->type_id==CAMP_WITHDAYS){ 
            
            
            $data[$player->id]['name']=$player->name;
            $data[$player->id]['guardian_name']=$player->user->name;
            $data[$player->id]['category']=$player->orginalCategory->name;
            $data[$player->id]['season_name']=$term->season->name;
            $data[$player->id]['term_name']=$term->name;
            $data[$player->id]['training_id']=$detail['session_id'];
            $data[$player->id]['location']= $invoices->getLocation($detail['location_id']);
            $data[$player->id]['days']=$detail['day_id'];
            $data[$player->id]['no_of_trainings']= $detail['total_count']." "."Days";
            $data[$player->id]['start_end_time'][$detail['day_id']]=
            $detail['start_time']."-".$detail['end_time'];
          
              
           }
       }
      
      
      return view('admin.player_terms.invoice_report',['no'=>1,'data'=>$data,'invoices'=>$invoices]);
    }


  public function invoicesRequest(){

    $data=[];

    $session_days=Day::all();
    $players=Player::where('user_id',Auth::user()->id)->get();
    $invoices =Invoice::where('user_id',Auth::user()->id)->where('parent_id',STATUS_ENABLED)->get();
    if(count($invoices) > 0){
      foreach ($invoices  as $key => $invoice) {
        $session_details=json_decode($invoice->session_details,true);
            foreach ($session_details  as $key => $detail) {
            $checkInvoiceNo[]= $detail['serial_no'];
        }
      }
    }
    else{
       $checkInvoiceNo[]=array();
    }

     return view('account.invoices_request.index',['no'=>1,'players'=>$players,'session_days'=>$session_days,'data'=>$data,'checkInvoiceNo'=>$checkInvoiceNo]);

  }


  public function reviewInvoice(Request $request){
   
    $request->session()->forget('playerDiscount');
    $request->session()->forget('data');
    $request->session()->forget('calculation');
    $request->session()->forget('players');
    $request->session()->forget('terms');
    $request->session()->forget('is_wallet_used');
    $request->session()->forget('wallet_details');
    $request->session()->forget('start_date');
    $request->session()->forget('end_date');
       
    $user=User::find(Auth::user()->id);
    $data=[];
    $fee_sum=0;
    $count=0;
    $session_days=Day::all();
    $amountPer=0;
    $amountless=0;
    $amountAdd=0;
    $total=0;
    $kit_amount=0;
    $vat=0;
    $total_amount=0;
    $remove_kit_fee=$request->remove_kit_fee;
    $is_not_vat=$request->is_not_vat;
    $use_wallet=$request->use_wallet;
    $wallet_amount=$request->wallet_amount;
    $tax_amount=(TAX/100);
    $playerDiscount=array();
    $wallet_details=array();
    $players_request=$request->players;

    if(empty($players_request)){
     return redirect()->route('account.invoices.request')->with('error','Select Players'); 
    }
    
    foreach ($players_request as $pl_request) {
    $record = explode('-', $pl_request);
    $get_players[]    = $record[0];
    $terms[]   = $record[1];
    }

    $wallet=Wallet::where('user_id',Auth::user()->id)->orderByDesc('id')->first();
    if(empty($wallet->total)){
        $wallet_total=0;
    }else{
        $wallet_total=$wallet->total;
    }
    
    if(!empty($is_not_vat)){
     $tax_amount=0;
    }
    
    $players=Player::whereIn('id',$get_players)->orderBy('category_id','ASC')->get();

     foreach($players as $player){
      $category[]=$player->category->id;
     }

    foreach($players as $player){
    foreach($player->terms->whereIn('id',$terms) as $term){

     if($term->type_id==CAMP_WITHOUTDAYS){
            
            $data[$player->id]['name']=$player->name;
            $data[$player->id]['guardian_name']=$player->user->name;
            $data[$player->id]['category_id']=$player->category->id;
            $data[$player->id]['serial_no']=$term->pivot->serial_no;
            $data[$player->id]['category']=$player->category->name;
            $data[$player->id]['is_hpc_player']=$player->is_hpc_player;
            $data[$player->id]['is_goal_keeper']=$player->user->name;
            $data[$player->id]['term_id']=$term->id;
            $data[$player->id]['term_name']=$term->name;
            $data[$player->id]['training_id']=0;
            $data[$player->id]['location_id']=0;
            $data[$player->id]['location']="";
            $data[$player->id]['start_end_time'][0]="00:00:00-00:00:00";
            $data[$player->id]['player_remaining_training_session_count']="";
            $data[$player->id]['pro_rates']="";
            $data[$player->id]['player_fee']=$term->pivot->fee_amount; 
            $data[$player->id]['player_days_count']=0;
            $total=$term->pivot->fee_amount;
            $fee_sum+=$total;
      }

    elseif($term->type_id==CAMP_WITHDAYS ){ 
      foreach($player->trainingSessions->where('term_id',$term->id) as $session){
         if($session->pivot->is_active==0){
            
           
            $data[$player->id]['name']=$player->name;
            $data[$player->id]['guardian_name']=$player->user->name;
            $data[$player->id]['category_id']=$player->category->id;
            $data[$player->id]['category']=$player->category->name;
             $data[$player->id]['serial_no']=$session->pivot->serial_no;
            $data[$player->id]['is_hpc_player']=$player->is_hpc_player;
            $data[$player->id]['is_goal_keeper']=$player->user->name;
            $data[$player->id]['term_id']=$session->term->id;
            $data[$player->id]['term_name']=$session->term->name;
            $data[$player->id]['training_id']=$session->id;
            $data[$player->id]['location_id']=$session->pivot->location_id;
            $data[$player->id]['location']= $session->getLocation($session->pivot->location_id);
            $day=$session->getDayLiteralAttribute($session->pivot->day_id);
            $data[$player->id]['start_end_time'][$day]=$session->start_time."-".$session->end_time;
           $data[$player->id]['pro_rates']=$term->pivot->fee_amount;
           $data[$player->id]['player_fee']=$term->pivot->fee_amount;
           $data[$player->id]['player_remaining_training_session_count']="";
           $data[$player->id]['player_days_count']=$player->trainingSessions->where('term_id',$term->id)->count();
           
          } //end of if condition
        }//end of session loop
        $total=$term->pivot->fee_amount;
        $fee_sum+=$total;
        
    }


    elseif($term->type_id==SEASON_TERM){ 

      //get player remaining count 
       $total_counts=$this->getTrainingSessionCount($request->all(),$get_players,$terms);
        foreach ($total_counts as $key => $value) {
              if($key==$player->id){
              $player_remaining_training_session_count=$value;
              $data[$player->id]['player_remaining_training_session_count']=$value;
              }
       }
     foreach($player->trainingSessions->where('term_id',$term->id) as $session){
         if(!in_array($session->pivot->serial_no,$data) && $session->pivot->is_active==0){
            
           
            $data[$player->id]['name']=$player->name;
            $data[$player->id]['guardian_name']=$player->user->name;
            $data[$player->id]['category_id']=$player->category->id;
            $data[$player->id]['category']=$player->category->name;
            $data[$player->id]['serial_no']=$session->pivot->serial_no;
            $data[$player->id]['is_hpc_player']=$player->is_hpc_player;
            $data[$player->id]['is_goal_keeper']=$player->user->name;
            $data[$player->id]['term_id']=$session->term->id;
            $data[$player->id]['term_name']=$session->term->name;
            $data[$player->id]['training_id']=$session->id;
            $data[$player->id]['location_id']=$session->pivot->location_id;
            $data[$player->id]['location']= $session->getLocation($session->pivot->location_id);
            $day=$session->getDayLiteralAttribute($session->pivot->day_id);
            $data[$player->id]['start_end_time'][$day]=$session->start_time."-".$session->end_time;
            $data[$player->id]['pro_rates']=$term->pivot->fee_amount;
            $data[$player->id]['player_fee']=$player_remaining_training_session_count*$term->pivot->fee_amount;
            
             $data[$player->id]['player_days_count']=$player->trainingSessions->where('term_id',$term->id)->count();
            

          } //end of if condition
        }//end of session loop
         $total=$player_remaining_training_session_count*$term->pivot->fee_amount;
         $fee_sum+=$total;
      } //end of if condition
    
   
   
     
   
    if(!empty($remove_kit_fee)){
        
        $kit_amount=0;
        
    }
    elseif($term->pivot->match_kit==STATUS_ENABLED){
       $kit_amount+=$term->kit_amount;
     }

     $player_discount=$player->discounts()->wherePivot('term_id',$term->id)->get();
     if(!empty($player_discount)){
      
      foreach ($player_discount as $key => $discount) {
      if($discount->type==1){
          $amountPer+=$discount->amount;
          $data[$player->id]['discount'][$discount->id]=$discount->name;
          $total=$total-($total * $amountPer / 100);
          $data[$player->id]['discount_amount']=$total;
        }
       if($discount->type==2){
        $amountless+= $discount->amount;
        $data[$player->id]['discount'][$discount->id]=$discount->name;
        $total=$total-$amountless;
        $data[$player->id]['discount_amount']=$total;
    }
        if($discount->type==3){
         $amountAdd+= $discount->amount;
         $data[$player->id]['discount'][$discount->id]=$discount->name;
         $total=$total+$amountAdd;
         $data[$player->id]['discount_amount']=$total;
      }
      }
      
     }

    $data[$player->id]['player_amount']=$total;
  } //end of term
  
   if(count($players) == 2){
     $cat_seq = array_unique($category);
        sort($cat_seq);
         if($player->category->id==$cat_seq[0]){
            
             $per=15/100; 
             $data[$player->id]['discount'][1]="Sibling Discount Child 2 : (15%)";
             $playerDiscount[$player->id][1]="Sibling Discount Child 2 : (15%)";

         }  
    }

     elseif(count($players) >= 3){
     $cat_seq = array_unique($category);
        sort($cat_seq);

    
        if($player->category->id==$cat_seq[0]){
             $per=25/100;
             $data[$player->id]['discount'][2]="Sibling Discount Child 3 : (25%)";
             $playerDiscount[$player->id][2]="Sibling Discount Child 3 : (25%)";


            
         }
         elseif($player->category->id==$cat_seq[1])
         {
             $per=15/100; 
             $data[$player->id]['discount'][1]="Sibling Discount Child 2 : (15%)";
             $playerDiscount[$player->id][1]="Sibling Discount Child 2 : (15%)";

         }  
    }
    else{
        $per=0; 
    }
    $total_amount+=$total;

  }//end of player
  $calculation['total']=$total_amount; 
  $total_amount+=$kit_amount;

  if(count($players) >=2){
  $sibling_discount=ceil($total_amount-($total_amount)*$per);
  $vat=ceil($sibling_discount*$tax_amount);
  $total_sum=ceil($sibling_discount+$vat);  
  }
  else{
  $vat=ceil($total_amount*$tax_amount);
  $total_sum=ceil($total_amount+$vat); 
  }

  if(!empty($use_wallet) && $wallet_total < $wallet_amount){

     return redirect()->route('account.invoices.request')->with('error','In Sufficent Balance In Wallet');  
    }
    elseif(!empty($use_wallet)){
      $use=1;
      $remaining_amount=$wallet_total-$wallet_amount;
      $remarks=$request->remarks;
      $wallet_details=['remaining_amount'=>$remaining_amount,'remarks'=>$remarks,'wallet_amount'=>$wallet_amount];
     
    }
    else{
      $use=0;
        $remaining_amount="";
    }
   
    
    $calculation['total_sum']=$total_sum;
    $calculation['vat']=$vat; 
    $calculation['kit_amount']=$kit_amount; 
    
    $start_date=Carbon::createFromFormat('m/d/Y',$request->start_date)->format('Y-m-d');
    
    
    Session::put('playerDiscount',$playerDiscount); 
    Session::put('data',$data);
    Session::put('calculation',$calculation);
    Session::put('players',$request->players);
    Session::put('terms',$terms);
    Session::put('is_wallet_used',$use); 
    Session::put('wallet_details',$wallet_details);
    Session::put('start_date',$start_date); 
    // Session::put('end_date',$request->end_date); 

   
   return view('account.invoices_request.review_amount',['no'=>1,'data'=>$data,'total_sum'=>$total_sum,'tax_amount'=>$tax_amount,'kit_amount'=>$kit_amount,'remaining_amount'=>$remaining_amount,'vat'=>$vat]);
     } //function end

    public function getTrainingSessionCount($request,$get_players,$terms){
      
      $players=Player::whereIn('id',$get_players)->get();

      $start_date=Carbon::createFromFormat('m/d/Y',$request['start_date'])->format('Y-m-d');
      $getterms=$terms;
      $terms=Term::whereIn('id',$getterms)->get();
      
      foreach ($players as $key => $player) {
        $count=0;
       foreach ($player->trainingSessions->whereIn('term_id',$getterms) as $session) {
        
         $playerdatesRemaing=json_decode($session->date,true);
         foreach($playerdatesRemaing as $key => $remaining){
            
           if(empty($request['end_date'])){
            $end_date=Carbon::createFromFormat('Y-m-d H:i:s', $session->term->end_date)->format('Y-m-d');
            }
            else{
            $end_date=Carbon::createFromFormat('m/d/Y',$request['end_date'])->format('Y-m-d');
            }
         
           if($remaining['day_id']==$session->pivot->day_id){
             
             $plDate=Carbon::createFromFormat('Y-m-d',$remaining['date'])->format('Y-m-d');
           
             if (($plDate >= $start_date) && ( $plDate <= $end_date)){
                $count++;
              $playerSessionDay[$player->id]=$count;
               
             }
              
           }
        
       }
      }

      }

     return $playerSessionDay;
  
  }

    /**

     * Show the form for creating a new resource.

     *
0
     * @return \Illuminate\Http\Response

     */


    public function generatedReport(Invoice $invoice){

      $data=array();
      $discounts=array();
      $date=date('Y-m-d');
      $walletLess=0;
      $invoices=Invoice::where('id',$invoice->id)->first();
      
      if($invoices->is_wallet_used==1){
      $walletLess=Wallet::where('user_id',$invoices->user_id)->orderByDesc('id')->first();
      }

      $discounts=json_decode($invoice->discount_details,true);
      
      $session_details=json_decode($invoice->session_details,true);
           foreach ($session_details  as $key => $detail) {
             
            if($invoice->player_id==null){
               $get_player=$detail['player_id'];
               $term_id=$detail['term_id'];
            }
            else{
               $get_player=$invoice->player_id;
               $term_id=$invoice->term_id;
            }
            
            $term =Term::find($term_id);
           
            $player=Player::where('id',$get_player)->first();
            $player_amount=Invoice::where('player_id',$player->id)->where('term_id',$term_id)->first();
           

            

             if($term->type_id==CAMP_WITHOUTDAYS){
             
             $data[$player->id]['name']=$player->name;
             $data[$player->id]['guardian_name']=$player->user->name;
             $data[$player->id]['category']=$player->orginalCategory->name;
             $data[$player->id]['term_name']=$term->name;
             $data[$player->id]['season_name']=$term->season->name;
             $data[$player->id]['location']= "ALL DAYS";
             $data[$player->id]['no_of_trainings']= "0 DAYS";
             $data[$player->id]['start_end_time'][""]="";
             $data[$player->id]['amount']=$player_amount->amount;
            

             
            }
           elseif($term->type_id==SEASON_TERM || $term->type_id==CAMP_WITHDAYS){ 
            
            
            $data[$player->id]['name']=$player->name;
            $data[$player->id]['guardian_name']=$player->user->name;
            $data[$player->id]['category']=$player->orginalCategory->name;
            $data[$player->id]['season_name']=$term->season->name;
            $data[$player->id]['term_name']=$term->name;
            $data[$player->id]['training_id']=$detail['session_id'];
            $data[$player->id]['location']= $invoice->getLocation($detail['location_id']);
            $data[$player->id]['days']=$detail['day_id'];
            $data[$player->id]['no_of_trainings']= $detail['total_count']." "."Days";
            $data[$player->id]['start_end_time'][$detail['day_id']]=
            $detail['start_time']."-".$detail['end_time'];
            $data[$player->id]['amount']=$player_amount->amount;
          }
       }
      
      return view('account.invoices.invoice_report',['no'=>1,'data'=>$data,'invoices'=>$invoices,'walletLess'=>$walletLess,'discounts'=>$discounts]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function storeInvoice(Request $request){
     

     $wallet_details=Session::get('wallet_details');
     $is_wallet_used=Session::get('is_wallet_used');
     $start_date=Session::get('start_date');
     //$end_date=Session::get('end_date');

     $player_ids=Session::get('players');
     $terms=Session::get('terms');
     $details=Session::get('calculation');
     $playerDiscount=Session::get('playerDiscount');
     $data=Session::get('data');
     if(empty($playerDiscount)){
        $playerDiscount=array();
       }
    $last_insert=0;

    $wallet=Wallet::where('user_id',Auth::user()->id)->orderByDesc('id')->first();
      if(empty($wallet->total)){
        $wallet_total=0;
       }else{
       
       $wallet_total=$wallet->total;
    }
    foreach ($details as $key => $detail) {
         if($key=='vat'){
          $vat= $detail;
         }
         if($key=='total'){
          $total= $detail;
         }
          if($key=='total_sum'){
          $final_total= $detail;
         }

         if($key=='kit_amount'){
          $kit_amount= $detail;
         }
     }
    $players=Player::whereIn('id',$player_ids)->get();
     foreach ($players as $key => $player) {
        foreach ($player->terms as $key => $term){
          if($kit_amount==0){
           $player->terms()->wherePivot('term_id',$term->id)->update(['match_kit' => STATUS_DISABLED]);
      }
    }
}

    $invoices =Invoice::where('user_id',Auth::user()->id)->get();
    if(count($invoices)>0){
      foreach ($invoices  as $key => $invoice) {
        $session_details=json_decode($invoice->session_details,true);
            foreach ($session_details  as $key => $detail) {
            $checkInvoiceNo[]= $detail['serial_no'];
        }
      }
    }
    else{
       $checkInvoiceNo[]=array();
    }

    if(count($data)>1){
      foreach($data as $key=>$dt){
      foreach($dt['start_end_time'] as $dayKey=>$time){
              
             $record = explode('-', $time);{
              $start_time=$record[0];
              $end_time=$record[1];
        }
        $multi_players[]=['serial_no'=>$dt['serial_no'],'location_id'=>$dt['location_id'],'day_id'=>$dayKey,'player_id'=>$key,'session_id'=>$dt['training_id'],'category_id'=>$dt['category_id'],'term_id'=>$dt['term_id'],'start_time'=>$start_time,'end_time'=>$end_time,'total_count'=>$dt['player_days_count']];
      }
      
      
      }

          $invoice=new Invoice();
          $invoice->user_id=Auth::user()->id;
          $invoice->session_details=json_encode($multi_players);
          $invoice->amount = $total;
          $invoice->parent_id=$last_insert;
          $invoice->total_amount=$final_total;
          $invoice->tax_amount=$vat;
          $invoice->kit_amount= $kit_amount;
          $invoice->payment_status= STATUS_READY_TO_PAY;
          $invoice->discount_details=json_encode($playerDiscount);
          $invoice->is_wallet_used=$is_wallet_used;
          $invoice->start_date=$start_date;
          //$invoice->end_date= $end_date;
          $invoice->save();
          $last_insert=$invoice->id;


      foreach($data as $key=>$dt){
   
      foreach($dt['start_end_time'] as $dayKey=>$time){

        $record = explode('-', $time);{
              $start_time=$record[0];
              $end_time=$record[1];
        }
        $single_player[]=['serial_no'=>$dt['serial_no'],'location_id'=>$dt['location_id'],'day_id'=>$dayKey,'session_id'=>$dt['training_id'],'player_id'=>0,'category_id'=>0,'term_id'=>0,'start_time'=>$start_time,'end_time'=>$end_time,'total_count'=>$dt['player_days_count']];
      }
    
       $category_id=$dt['category_id'];
       $term_id=$dt['term_id'];
       $player=$key;  
       
          $invoice=new Invoice();
          $invoice->user_id=Auth::user()->id;
          $invoice->player_id=$player;
          $invoice->category_id=$category_id;
          $invoice->term_id=$term_id;
          $invoice->parent_id=$last_insert;
          $invoice->session_details=json_encode($single_player);
          $invoice->amount = $dt['player_amount'];
          $invoice->payment_status= STATUS_READY_TO_PAY;
          $invoice->discount_details=json_encode($playerDiscount);
          $invoice->start_date=$start_date;
          //$invoice->end_date= $end_date;
          $invoice->save();
      }
      

    }
    else{
    foreach($data as $key=>$dt){
   
      foreach($dt['start_end_time'] as $dayKey=>$time){

        $record = explode('-', $time);{
              $start_time=$record[0];
              $end_time=$record[1];
        }
       $single_player[]=['serial_no'=>$dt['serial_no'],'location_id'=>$dt['location_id'],'day_id'=>$dayKey,'session_id'=>$dt['training_id'],'player_id'=>0,'category_id'=>0,'term_id'=>0,'start_time'=>$start_time,'end_time'=>$end_time,'total_count'=>$dt['player_days_count']];
      }
    
       $category_id=$dt['category_id'];
       $term_id=$dt['term_id'];
       $player=$key;  
    
          $invoice=new Invoice();
          $invoice->user_id=Auth::user()->id;
          $invoice->player_id=$player;
          $invoice->category_id=$category_id;
          $invoice->term_id=$term_id;
          $invoice->parent_id=$last_insert;
          $invoice->session_details=json_encode($single_player);
          $invoice->amount = $total;
          $invoice->total_amount=$final_total;
          $invoice->tax_amount=$vat;
          $invoice->kit_amount= $kit_amount;
          $invoice->payment_status= STATUS_READY_TO_PAY;
          $invoice->discount_details=json_encode($playerDiscount);
          $invoice->is_wallet_used=$is_wallet_used;
          $invoice->start_date=$start_date;
          //$invoice->end_date= $end_date;
          $invoice->save();
      }
    }  

      if(!empty($wallet_details)){
        foreach ($wallet_details as $key => $value) {
            if($key=="remaining_amount"){
                $amount=$value;
            }
          if($key=="wallet_amount"){
            $wallet_amount=$value;
          }
            if($key=="remarks"){
             $remarks=$value;
            }
        }
        
        $walletInsert=new Wallet();
        $walletInsert->user_id=Auth::user()->id;
        $walletInsert->status=WALLET_STATUS_NOT_USED;
        $walletInsert->amount_type=AMOUNT_TYPE_CREDIT;
        $walletInsert->total=$amount;
        $walletInsert->debit_amount=$wallet_amount;
        $walletInsert->remarks=$remarks;
        $walletInsert->total=$amount;
        $walletInsert->save();
        
        }
       
        
        $request->session()->forget('playerDiscount');
        $request->session()->forget('data');
        $request->session()->forget('calculation');
        $request->session()->forget('players');
        $request->session()->forget('terms');
        $request->session()->forget('is_wallet_used');
        $request->session()->forget('wallet_details');
        $request->session()->forget('start_date');
        $request->session()->forget('end_date');
        

       return redirect()->route('account.request.invoices.index')->with('success','Request Have Been Submitted');

        

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */
    public function checkout($invoiceNo)

    {   
       $invoice=Invoice::where('id',$invoiceNo)->first();
       return view('account.invoices.checkout',['invoice'=>$invoice]);

    }

  



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function switchToUser($user)

    {  
        
        Session::put('admin_id',Auth::user()->id);
        Auth::loginUsingId($user);
        return redirect()->route('account.dashboard');

      // ****** Hamza Work *********

      //   Session::put('activeuser_id',$user);       
      //    $user22 = User::where('id','=',$user)->first();
      //    $user22->isadmin=1;
      //    $user22->save();
      //  Session::put('admin_id',Auth::user()->id);
      //    Auth::loginUsingId($user);
      //    return redirect()->route('account.dashboard');

      // ****** Hamza Work *********

    }

    public function switchToAdmin()

    {  
        $admin=Session::get('admin_id');

        if(!empty($admin)){

        Auth::loginUsingId($admin);

        Session::forget('admin_id');

      if(Auth::user()->type==USER_TYPE_MANAGER){
      $type="manager.";
      }
      elseif(Auth::user()->type==USER_TYPE_ADMIN){
      $type="admin.";
      }

        return redirect()->route($type.'dashboard');

    }


          // Hamza Work

    //       $activeuser_id=Session::get('activeuser_id');
    //       $user222 = User::where('id','=',$activeuser_id)->first();
    //       $user222->isadmin=0;
    //       $user222->save();

    //      $admin=Session::get('admin_id');

    //      if(!empty($admin)){

    //      Auth::loginUsingId($admin);

    //      Session::forget('admin_id');

    //    if(Auth::user()->type==USER_TYPE_MANAGER){
    //     $type="manager.";
    //    }
    //    elseif(Auth::user()->type==USER_TYPE_ADMIN){
    //     $type="admin.";
    //    }

    //      return redirect()->route($type.'dashboard');

    //  }

    // Hamza Work

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function updateInvoiceRequest(Request $request)

    {

    }

    
   public function downloadReport(Request $request)

    {
      
    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function writeOff(Invoice $invoice)

    {

       return view('admin.invoices.write_off',['invoice'=>$invoice]);

    }



    public function writeOffUpdate(Request $request,Invoice $invoice)

    {

      $invoice->write_off_amount=$request->write_off_amount;

      $invoice->write_off_amount_reason=$request->write_off_amount_reason;

      $invoice->save();

      return redirect()->route('admin.invoices.index')->with('Updated Successfully');

    }



    public function partialRefund(Invoice $invoice)

    {

       return view('admin.invoices.partial_refund',['invoice'=>$invoice]);

    }



    public function partialRefundUpdate(Request $request,Invoice $invoice)

    {

      $invoice->partial_amount=$request->partial_refund_amount;

      $invoice->partial_amount_reason=$request->partial_refund_amount_reason;

      $invoice->save();

      return redirect()->route('admin.invoices.index')->with('Updated Successfully');

    }



    public function editPaymentMethod(Invoice $invoice)

    {



     $paymentMethods=[''=>"Select Method",1=>'Cash',2=> 'Cheques',3=>'Credit',

     4=>'Cardbank',5=>'Transfer',6=>'Transfer',7=>'Online'];

    $paymentStatus=[''=>"Select Status",1=>'Not Paid',2=>'Partial Payment',3=>'Paid',4=>'Ready to Paid',5=> 'Invoice Accepted ',6=>'Refunded',7=>'Partial Payment Pending',8=> 'Payment Defaulted ',9=>'Sponsored',10=>'Makeup'];

     $paritalPaymentType=[''=>"Select Type",2=>'2 payments',3=>'3 payments',4=>'4 payments',

     5=>'5 payments'];

     

     return view('admin.invoices.payment_method',['paymentMethods'=>$paymentMethods,'paymentStatus'=>$paymentStatus,'paritalPaymentType'=>$paritalPaymentType,'invoice'=>$invoice

     ]);

    }

    public function installments()
    {

      $data=array();
      if(Auth::user()->type==USER_TYPE_PARENT){
      $days=Day::all();  
      $invoice_request =Invoice::where('user_id',Auth::user()->id)->where('is_installment',STATUS_ENABLED)->get();
      return view('account.invoices.player_invoices',['invoice_request'=>$invoice_request,'no'=>1,'days'=>$days,'data'=>$data]);
    }

    }

    public function updatePaymentMethod(Request $request,Invoice $invoice)

    {
      $partial_payment_type=$request->partial_payment_type;
      $invoice->payment_method=$request->payment_method;
      $invoice->payment_status=$request->payment_status;
      $invoice->partial_payment_type=$partial_payment_type;
      $invoice->save();
     
      if(!empty($invoice->total_amount)){

      $installments=$invoice->total_amount/$partial_payment_type;

      }else{

        $installments=$invoice->amount/$partial_payment_type;

      }

      for($i=1;$i<=$partial_payment_type;$i++){
       
        $installment=new InvoiceInstallment();

        $installment->invoice_id=$invoice->id;

        $installment->partial_payment_type=$partial_payment_type;

        $installment->amount=$installments;

        $installment->is_active = $request->input('is_active') !== null ? 1 : 0;

        $installment->save();

      }
     
      return redirect()->route('admin.invoices.index')->with('success','Updated Successfully');
      }



    public function paymentInstallment(Request $request,Invoice $invoice)

    {
    
    $list =InvoiceInstallment::where('invoice_id',$invoice->id)->get();
    $countInstallments = $list->count();    
    $installments=InvoiceInstallment::where('invoice_id',$invoice->id)->get();
    $paymentStatus=[STATUS_NOT_PAID=>'Not Paid',STATUS_PAID=>'Paid',STATUS_WAIVE_OFF=>'Waive off',STATUS_READY_TO_PAY=>'Ready to Pay'];

      return view('admin.invoices.installments',['invoice'=>$invoice,'countInstallments'=>$countInstallments,'installments'=>$installments,'paymentStatus'=>$paymentStatus]);
    }



    public function updateInstallments(Request $request,Invoice $invoice)

    {

      $total=0;
      $installmentIds=InvoiceInstallment::where('invoice_id',$invoice->id)->pluck('id')->toArray();
      $installmentNo=$request->installments;
      $status=$request->status;
      foreach ($installmentNo as $value) {

       $total+=$value;

      }
      $totalValue=floatval($total);
      
      if(!empty($invoice->total_amount))
       $totalAmount=$invoice->total_amount;
      else{
        $totalAmount=$invoice->amount;
      }
      
      if($totalValue!=$totalAmount)
      {
         return redirect()->route('admin.invoices.payments.installments',[$invoice->id])->with('error','Amount Miss Match');
      } 
    else{
      
        $results = array_map(function($installmentIds,$installmentNo,$status) {

        return array(
        'installmentIds'=>$installmentIds,
        'installmentNo'=>$installmentNo,
        'status'=> $status, 

        );

     }, 

     $installmentIds,$installmentNo,$status);
      foreach ($results as $result) {

       InvoiceInstallment::where('id',$result['installmentIds'])->update([

       'amount'=>$result['installmentNo'],
       'status' => $result['status'],
       'is_active' => STATUS_ENABLED,

      ]);

      if($result['status']==STATUS_PAID){
       
        $invoiceUpdate= new Invoice();
        $invoiceUpdate->user_id=$invoice->user_id;
        $invoiceUpdate->player_id=$invoice->player_id;
        $invoiceUpdate->term_id=$invoice->term_id;
        $invoiceUpdate->parent_id=$invoice->parent_id;
        $invoiceUpdate->category_id=$invoice->category_id;
        $invoiceUpdate->session_details=$invoice->session_details;
        $invoiceUpdate->total_amount=$result['installmentNo'];
        $invoiceUpdate->is_installment=STATUS_ENABLED;
        $invoiceUpdate->save();
     
       }
      
      }
         return redirect()->route('admin.invoices.index')
      ->with('success','Updated Successfully');

      }

    }
   
   public function destroy(Invoice $invoice)
    {
        $invoice->delete();
        return redirect()->route('admin.invoices.index')->with('success','Updated Successfully');
    }

    public function redirectRoutes($action){
     
        if(!isset($action)) {
            echo 'Page Not Found!';
            exit;
        }
        $objFort = new Payfort();
        if($action == 'getPaymentPage') {
            $objFort->processRequest(htmlspecialchars($_REQUEST['paymentMethod'], ENT_QUOTES, 'UTF-8'));
        }
        elseif($action == 'merchantPageReturn') {
            $objFort->processMerchantPageResponse();
        }
        elseif($action == 'processResponse') {
            $objFort->processResponse();
        }
        else{
            echo 'Page Not Found!';
            exit;
        }
    }


     public function invoiceSearch(Request $request)

    {

      // print("<pre>".print_r($request->all(), true)."</pre>");
      // exit(0);

        /*echo "saeed";
        echo ($request->created_at) ."<br>"; 
        echo ($request->created_at2) ."<br>"; 
        echo ($request->start_date); exit;*/

        
       if(Auth::user()->type==
       USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
        
        $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();
        $teams=Team::where('is_active',STATUS_ENABLED)->get();
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();
        $coaches=User::where('type',USER_TYPE_COACH)->get();
        $paymentMethods=[''=>"Select Method",1=>'Cash',2=> 'Cheques',3=>'Credit',4=>'Cardbank',5=>'Transfer',6=>'Transfer',7=>'Online'];
        $paymentStatus1=[''=>"Select Status",1=>'Not Paid',2=>'Partial Payment',3=>'Paid',4=>'Ready to Paid',5=> 'Invoice Accepted ',6=>'Refunded',7=>'Partial Payment Pending',8=> 'Payment Defaulted ',9=>'Sponsored',10=>'Makeup',11=>'Pending'];
        $locations=location::where('is_active',STATUS_ENABLED)->get();

        
       
        $keyword = $request->keyword;
        $user_id = $request->user_id;
        $invoice_id = $request->invoice_id;
        $player_id = $request->player_id;
        $term_id = $request->term_id;
        $parent_id = $request->parent_id;
        $payment_status = $request->payment_status;
        $payment_method = $request->payment_method;
        $category_id=$request->category_id;
        $location_id = $request->location_id;
        //$additional = $request->additional;
        $start_date=$request->start_date;
        $end_date=$request->end_date;
        $created_at=$request->created_at;
        $created_at2=$request->created_at2;
        $amount_from = $request->amount_from;
        $amount_upto = $request->amount_upto;
        $inv_installment = $request->inv_installment;
        //$show_all_inv_additional_inv = $request->show_all_inv_additional_inv;
        //$show_all_inv_for_custom_inv = $request->show_all_inv_for_custom_inv;
        $refund_amount = $request->refund_amount;
        $write_off_amount = $request->write_off_amount;
        $paid_installments = $request->paid_installments;
        $pending_installments = $request->pending_installments;
        
        //echo($request->paymentStatus); exit;
         
        $search_query = Invoice::where('is_custom', '=', 1);
        if(!empty($start_date) && !empty($end_date)){
         
          $start_date= Carbon::createFromFormat('m/d/Y', $start_date)->format('Y-m-d');
          $end_date = Carbon::createFromFormat('m/d/Y',$end_date)->format('Y-m-d');
          $search_query->where('start_date','>=', "'$start_date'")
          ->where('end_date','<=',"'$end_date'");
         }

         if(!empty($refund_amount)){

          if($refund_amount == 1){
            $search_query->where('payment_status', STATUS_REFUNDED);
          }else{
            $search_query->where('payment_status',"<>" ,STATUS_REFUNDED);
          }
                   
         }

         if(!empty($write_off_amount)){

          if($write_off_amount == 1){
            $search_query->where('write_off_amount', ">", "0.00");
          }else{
            $search_query->where('write_off_amount', "<=" ,"0.00");
          }
                   
         }

         if(!empty($amount_from) && !empty($amount_upto)){
         
          $search_query->where('amount','>=', $amount_from)
          ->where('amount','<=', $amount_upto);
         }

         if(!empty($show_all_inv_for_custom_inv)){
          $search_query->where('is_custom','=', $show_all_inv_for_custom_inv);
         }

         if(!empty($inv_installment)){

          $search_query->join('invoice_installments', 'invoice_installments.invoice_id', '=', 'invoices.id')
          ->where('is_installment',STATUS_ENABLED)
          ->where('invoice_installments.partial_payment_type','=', $inv_installment)->groupBy('invoice_installments.invoice_id');
         }  

         if(!empty($paid_installments)){

          $search_query->join('invoice_installments', 'invoice_installments.invoice_id', '=', 'invoices.id')
          ->where('is_installment',STATUS_ENABLED)
          ->where('payment_status',STATUS_PAID)
          ->where('invoice_installments.partial_payment_type','=', $paid_installments)->groupBy('invoice_installments.invoice_id');
         }
         
         if(!empty($pending_installments)){

          $search_query->join('invoice_installments', 'invoice_installments.invoice_id', '=', 'invoices.id')
          ->where('is_installment',STATUS_ENABLED)
          ->where('payment_status',STATUS_PARTIAL_PAYMENT_PENDING)
          ->where('invoice_installments.partial_payment_type','=', $pending_installments)->groupBy('invoice_installments.invoice_id');
         }

         if(!empty($created_at) && !empty($created_at2)){
         
          $created_at= Carbon::createFromFormat('m/d/Y', $created_at)->format('Y-m-d');
          $created_at2 = Carbon::createFromFormat('m/d/Y',$created_at2)->format('Y-m-d');
          $search_query->where('created_at','>=',"'$created_at'")
          ->where('created_at','<=',"'$created_at2'");

         }

         if (!empty($keyword)) {
          //$search_query = $search_query->where('name', 'LIKE', '%'.$keyword.'%');

          $search_query = $search_query->where(function ($query) use ($keyword) {
            $query->where('id', 'LIKE', '%'.$keyword.'%')
                ->orWhere('player_id', 'LIKE', '%'.$keyword.'%')
                ->orWhere('user_id', 'LIKE', '%'.$keyword.'%')
                ->orWhere('parent_id', 'LIKE', '%'.$keyword.'%')
                ->orWhere('category_id', 'LIKE', '%'.$keyword.'%')
                ->orWhere('is_custom', 'LIKE', '%'.$keyword.'%')
                ->orWhere('parent_id', 'LIKE', '%'.$keyword.'%')
                ;
            });

          }
        if(!empty($term_id)){

        //   $search_query->whereHas('term',function($search_query) use ($term_id) {
        //   $search_query->where('player_term.term_id',$term_id);
        //  });

          $search_query = $search_query->where('term_id',$term_id);

        }

        if (!empty($player_id)) {
        $search_query = $search_query->where('player_id',$player_id);
        }

        if (!empty($invoice_id)) {
          $search_query = $search_query->where('id',$invoice_id);
        }
        
        if(!empty($user_id)) {
        $search_query = $search_query->where('user_id',$user_id);
        }
        if(!empty($parent_id)) {
          $search_query = $search_query->where('parent_id',$parent_id);
          }
          if(!empty($category_id)) {
            $search_query = $search_query->where('category_id',$category_id);
            }
            if(!empty($location_id)) {

              //$search_query = $search_query->where('location_id',$location_id);
              $search_query = $search_query->where('session_details','LIKE', '%"location_id":'.$location_id.'%');

              }

              // if(!empty($additional)) {
              //   $search_query = $search_query->where('additional',$additional);
              //   }
               
          if(!empty($payment_status)) {
            $search_query = $search_query->where('payment_status',$payment_status);
            }
            if(!empty($payment_method)) {
              $search_query = $search_query->where('payment_method',$payment_method);
              } if(!empty($is_custom)) {
                $search_query = $search_query->where('is_custom',$is_custom);
                }
               

          //print_r($search_query);
          //exit(0);      


              
        $invoices = $search_query->paginate(15);

        // print_r($invoices);
        // exit(0);

        
         return view($type.'invoices.index', [
            'player_id'=>$player_id,'parent_id'=>$parent_id,'keyword'=>$keyword,
            'invoices'=>$invoices,'user_id'=>$user_id,'paymentStatus1'=>$paymentStatus1,'paymentMethods'=>$paymentMethods,
            'location_id'=>$location_id,'term_id'=>$term_id,'invoice_id'=>$invoice_id,'locations'=>$locations,
            'categories'=>$categories,'terms'=>$terms,'start_date'=>$start_date,'end_date'=>$end_date,'created_at'=>$created_at,'created_at2'=>$created_at2
            ,'payment_status'=>$payment_status
        ]);

    }

     public function customInvoice(Player $player,Term $term){
      
      $invoices=Invoice::where('player_id',$player->id)->where('term_id',$term->id)->where('is_custom',STATUS_ENABLED)->get();
      return view('admin.custom_invoices.index',['player'=>$player,'term'=>$term,
        'invoices'=>$invoices]);

    }

     public function customInvoiceStore(Request $request,Player $player,Term $term){

      $request->validate([
          'amount'  => ['required','regex:/^\d+(\.\d{1,2})?$/'],
         
        ]);
     
     $amount=$request->amount;
     $start_date=Carbon::createFromFormat('m/d/Y',$request->start_date)->format('Y-m-d');
     $is_wallet=0;
     $use_wallet=$request->use_wallet;
     $send_email=$request->send_email;
     $user_id=$player->user->id;
     if(!empty($request->add_vat)){
          $vat=(TAX/100);
      }
       else{
          $vat=0;
      }
      
      $vat_amount=$amount*$vat;
      $debit_amount=$amount+$vat_amount; 
     
     $wallet=Wallet::where('user_id',$user_id)->orderByDesc('id')->first();
    if(empty($wallet->total)){
      $wallet_total=0;
    }else{
      $wallet_total=$wallet->total;
    }
     if(!empty($use_wallet) && $amount > $wallet_total){
    
     return redirect()->route('admin.player.term.custom.invoice',[$player->id,$term->id])->with('error','In Sufficent Balance In Wallet');  
    }
    elseif(!empty($use_wallet)){
        $is_wallet=1;
        $total=$wallet_total-$debit_amount;
        $remarks=$request->remarks;
        $walletInsert=new Wallet();
        $walletInsert->user_id=$user_id;
        $walletInsert->status=WALLET_STATUS_NOT_USED;
        $walletInsert->amount_type=AMOUNT_TYPE_CREDIT;
        $walletInsert->total=$total;
        $walletInsert->debit_amount=$debit_amount;
        $walletInsert->remarks=$remarks;
        $walletInsert->save();
     
    }

   
      
      $invoice =new Invoice();
      $invoice->user_id=$user_id;
      $invoice->player_id=$player->id;
      $invoice->term_id=$term->id;
      $invoice->parent_id=0;
      $invoice->category_id=$player->category->id;
      $invoice->start_date=$start_date;
      $invoice->amount=$amount;
      $invoice->tax_amount=$vat_amount;
      $invoice->total_amount=$debit_amount;
      $invoice->is_custom=STATUS_ENABLED;
      $invoice->is_wallet_used=$is_wallet;
      $invoice->save();
      
      return redirect()->route('admin.player.term.custom.invoice',[$player->id,$term->id])->with('success',"Custom Invoice Generated Successfully");

    }

    public function sendCustomInvoiceEmail(Request $request,Player $player,Term $term){

    if(!empty($player)){
      $email_data=[
        'subject'=>$request->subject,
      ];
      $email = 'wardamajid115@gmail.com';
      Mail::to($email)->send(new CustomInvoice($email_data));
    }

    return redirect()->route('admin.player.term.custom.invoice',[$player->id,$term->id])->with('success',"Custom Invoice Email Send Successfully");

    }

    public function customInvoicedestroy(Request $request,Player $player,Term $term,Invoice $invoice){

     $invoice->delete();
      return redirect()->route('admin.player.term.custom.invoice',[$player->id,$term->id])->with('success',"Custom Invoice Deleted Successfully");

    }


    public function customInvoiceShow(Request $request,Player $player,Term $term,Invoice $invoice){

      $data=array();
      $discounts=array();
      $date=date('Y-m-d');
      $walletLess=0;
      $invoices=Invoice::where('id',$invoice->id)->where('is_custom',STATUS_ENABLED)->where('player_id',$player->id)->first();
      
      if($invoices->is_wallet_used==1){
      $walletLess=Wallet::where('user_id',$invoices->user_id)->orderByDesc('id')->first();
      }

      $discounts=json_decode($invoice->discount_details,true);
      
      $player=Player::where('id',$player->id)->first();
      
             $data[$player->id]['name']=$player->name;
             $data[$player->id]['guardian_name']=$player->user->name;
             $data[$player->id]['category']=$player->orginalCategory->name;
             $data[$player->id]['term_name']=$term->name;
             $data[$player->id]['season_name']=$term->season->name;
             $data[$player->id]['amount']=$request->amount;
            
       return view('admin.custom_invoices.invoice',['no'=>1,'data'=>$data,'invoices'=>$invoices,'walletLess'=>$walletLess]);

    }



    

}

