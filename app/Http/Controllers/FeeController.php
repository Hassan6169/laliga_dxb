<?php



namespace App\Http\Controllers;



use App\Fee;

use App\Term;

use App\Location;

use App\AgeCategory;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;





class FeeController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {
     if(Auth::user()->type==USER_TYPE_ADMIN){
      // if(Auth::user()->isadmin == 1){
        $type="admin.";
       }
   elseif(Auth::user()->type==USER_TYPE_MANAGER){
      //  elseif(Auth::user()->isadmin == 1){
       $type="manager.";
      }
        
         $locations=Location::where('is_active',STATUS_ENABLED)->get();
         $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();
         $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();

           $category_id=$request->category_id;
           $term_id=$request->term_id;
           $location_id=$request->location_id;


       if($request->is_submit == 1){
       $search_query = Fee::where('is_active', '=', STATUS_ENABLED);
       
       if(!empty($request->term_id)){
        
         $search_query = $search_query->where('term_id',$request->term_id);
    
       }
       if(!empty($request->category_id)) {
        $search_query = $search_query->where('fee_details', 'like', '%"'.$request->category_id.'"%');
        }

       if(!empty($request->location_id)){
        
         $search_query = $search_query->where('location_id', $request->location_id);
    
       }
        
        $fees = $search_query->paginate(15);
      }
      else{
        $fees=Fee::where('is_active',STATUS_ENABLED)->orderByDesc('created_at')->paginate(15); 
      }
     
        return view($type.'fees.index',['fees'=>$fees,'categories'=>$categories,'locations'=>$locations,'terms'=>$terms,'category_id'
            =>$category_id,'term_id'=>$term_id,'location_id'=>$location_id]);
  
    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

     public function create($type){
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
        $locations=Location::where('is_active',STATUS_ENABLED)->get();
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)
        ->orderByDesc('created_at')->get();

         if($type==SEASON_TERM){
         
          $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->where('type_id',SEASON_TERM)
          ->orderByDesc('created_at')->pluck('name','id');
         $feeSession=[1=>'1x/WEEKS',2=>'2x/WEEKS',3=>'3x/WEEKS',4=>'4x/WEEKS',
        5=>'Full season'];
         }
         //type Camp
         else if($type==CAMP_WITHDAYS){
          $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->where('type_id',CAMP_WITHDAYS)
          ->orderByDesc('created_at')->pluck('name','id');
         $feeSession=[1=>'Daily',2=>'1 WEEKS',3=>'2 WEEKS',4=>'3 WEEKS'];
         }

        //type Camp without days
        else if($type==CAMP_WITHOUTDAYS){
          $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->where('type_id',CAMP_WITHOUTDAYS)
          ->orderByDesc('created_at')->pluck('name','id');
         $feeSession=[7=>'All Days'];
         }
         
         return view($user_type.'fees.create',['categories'=>$categories,'terms'=>$terms,
            'locations'=>$locations,'feeSession'=>$feeSession,'type'=>$type]);

     }

   



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

     if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
      
     $this->validateFee($request);
     $locations=$request->location_id;
    

     $amounts=$request->feeAmount;
     $categories=$request->categories;


     $feeSessions=$request->feeSessionId;
      
     $term_id=$request->term;
     $term=Term::find($term_id);

       foreach ($categories as $key => $category) {
            foreach ($amounts as $feeSessionKey => $amount) {
           
              $data[]=['category_id'=>$category,'amount'=>$amount,'no_of_week'=>$feeSessionKey+1];  
           
            
        
       }
      }
      
    
    if(!empty($locations) && $term->type != CAMP_WITHOUTDAYS){
    foreach ($locations as $key => $location) {

        $fee= new Fee();
        $fee->term_id= $term_id;
        $fee->location_id= $location;
        $fee->fee_details= json_encode($data);
        $fee->save();
       }
    }
    else
    {
        $fee= new Fee();
        $fee->term_id= $term_id;
        $fee->location_id= 0;
        $fee->fee_details= json_encode($data);
        $fee->save();
    }


     

     return redirect()->route($user_type.'fee.sessions.index')->with('success', 'Training Fees Added Successfully');

     }



    /**

     * Display the specified resource.

     *

     * @param  \App\Fee  $fee

     * @return \Illuminate\Http\Response

     */
      public function destroy(Fee $fee){
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
      
      if (!empty($fee)) {

           $fee->delete();

           return redirect()->route($user_type.'fee.sessions.index')->with('success', 'Fee Deleted Successfully');

        }

    }


  



    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Fee  $fee

     * @return \Illuminate\Http\Response

     */



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Fee  $fee

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request,Fee $fee)

    {
    if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
      
    $this->validateFee($request);
     
     $amounts=$request->feeAmount;
     $categories=$request->categories;
     $location_id=$request->location_id;
      
     $term_id=$request->term;
      foreach ($categories as $key => $category) {
            foreach ($amounts as $feeSessionKey => $amount) {
           
          $data[]=['location_id'=>$location_id,'category_id'=>$category,'amount'=>$amount,'no_of_week'=>$feeSessionKey+1];  
           
            
        
       }
      }
      
    Fee::where('id',$fee->id)->update(['fee_details' => json_encode($data)]);
        
    
    return redirect()->route($user_type.'fee.sessions.index')->with('success', 'Training session Updated Successfully');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Fee  $fee

     * @return \Illuminate\Http\Response

     */

    public function edit(Fee $fee,$term)

    {
    if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
      
        $locations=Location::where('is_active',STATUS_ENABLED)->pluck('name','id');
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)
        ->orderByDesc('created_at')->get();

         if($fee->term->type_id==SEASON_TERM){
         
          $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->where('type_id',SEASON_TERM)
          ->orderByDesc('created_at')->pluck('name','id');
         $feeSession=[1=>'1x/WEEKS',2=>'2x/WEEKS',3=>'3x/WEEKS',4=>'4x/WEEKS',
        5=>'Full season'];
         }
         //type Camp with days
         else if($fee->term->type_id==CAMP_WITHDAYS){
          $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->where('type_id',CAMP_WITHDAYS)
          ->orderByDesc('created_at')->pluck('name','id');
         $feeSession=[1=>'Daily',2=>'1 WEEKS',3=>'2 WEEKS',4=>'3 WEEKS'];
         }
        //type Camp with days
        else if($fee->term->type_id==CAMP_WITHOUTDAYS){
          $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->where('type_id',CAMP_WITHOUTDAYS)
          ->orderByDesc('created_at')->pluck('name','id');
         $feeSession=[7=>'All Days'];
         }
         
          $detailsFee=json_decode($fee->fee_details,true);
          foreach($detailsFee as $key => $detail){
           
            $cateIds[]=$detail['category_id'];
            $amount[]=$detail['amount'];
            $no_of_week[]=$detail['no_of_week']; 
        }

            $details=array_combine($no_of_week, $amount);


            $locIds=Fee::where('term_id',$term)->pluck('location_id')->toArray();
            
            
            return view($user_type.'fees.edit',['categories'=>$categories,'terms'=>$terms,
            'locations'=>$locations,'feeSession'=>$feeSession,'fee'=>$fee,
            'locIds'=>$locIds,'cateIds'=>$cateIds,'details'=>$details]);

    }

     private function validateFee(Request $request) {

      $request->validate([

            'term'  => 'required',
            'categories'  => 'required',
            'location_id'  => 'required',
            'feeAmount.*'    => 'nullable|regex:/^\d*(\.\d{2})?$/|',
        ]);

    }



}

