<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\AgeCategory;
use App\Team;
use App\SignUp;
use App\Location;
use Auth;
use Mail;
use App\Mail\PopupRegister;
use Illuminate\Support\Facades\DB;
use Log;

class UserController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
      if(Auth::user()->type==USER_TYPE_ADMIN){
      //  if(Auth::user()->isadmin == 1){
          $usertype="admin.";
         }
      elseif(Auth::user()->type==USER_TYPE_MANAGER){
      //  elseif(Auth::user()->isadmin == 1){  
        $usertype="manager.";
       }
       
       $parents=User::where('type',USER_TYPE_PARENT)->orderByDesc('id')->paginate(15);
        return view($usertype.'users.index',['parents'=>$parents]);

    }

     public function accounts(){

        $users=User::whereIn('type',[USER_TYPE_COACH,USER_TYPE_MANAGER])->paginate(15);
        $user_types=[USER_TYPE_MANAGER=>'Manager',USER_TYPE_COACH=>'Coach'];
        return view('admin.accounts.index',['users'=>$users,'user_types'=>$user_types]);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function createAccount()

    {
       $user_types=[USER_TYPE_MANAGER=>'Manager',USER_TYPE_COACH=>'Coach'];
       return view('admin.accounts.create',['user_types'=>$user_types]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request,SignUp $signUp){
         
         $this->validateUser($request);
         $user = new User();
         $user->name = $request->name;
         $user->email = $request->email;
         $user->mobile = $request->mobile;
         $user->parent_1_relationship= $request->parent_1_relationship;
         $user->parent_2_name = $request->parent_2_name;
         $user->parent_2_email = $request->parent_2_email;
         $user->parent_2_mobile = $request->parent_2_mobile;
         $user->nationality = $request->nationality;
         $user->parent_2_relationship = $request->parent_2_relationship;
         $user->password=Hash::make($request->password);
         $user->hear_about_us = $request->hear_about_us;
         $user->type= USER_TYPE_PARENT;
         $user->save();

         SignUp::where('id',$signUp->id)->update(['is_registered' => STATUS_ENABLED]);

         if(!empty($user->id)){
          if(!empty($request->send_email)){
            
            $email_data=[
              'name' => $request->name,
              'email' => $request->email,
              'password'=>$request->password,
            ];

            Mail::to($request->email)->send(new PopupRegister($email_data));
          }
         }

        return redirect()->route('admin.signups.index',[$signUp->form_type_id])->with('success',"Added Succcessfully");
         
     }

    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function editAccount(User $user)

    {
       $user_types=[USER_TYPE_MANAGER=>'Manager',USER_TYPE_COACH=>'Coach'];
       return view('admin.accounts.edit',['user'=> $user,'user_types'=>$user_types]);

    }

    public function storeAccount(Request $request){

          $request->validate([
          'name' => ['required','max:50','regex:/^[\pL\s\-]+$/u'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
          'mobile' => ['nullable','max:10'],
        ]);

         $user = new User();
         
         $user->name=$request->name;
         $user->email=$request->email;
         $user->mobile=$request->mobile;
         $user->type=$request->type;
         if(!empty($request->password)){
         $user->password=Hash::make($request->password);
         }
         $user->save();

         return redirect()->route('admin.accounts.index')->with('success',"Coach Updated Succcessfully");



    }


    public function updateAccount(Request $request,User $user){

          $request->validate([
          'name' => ['required','max:50','regex:/^[\pL\s\-]+$/u'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id],
          'mobile' => ['nullable','digits:10'],
        ]);
         
         $user->name=$request->name;
         $user->email=$request->email;
         $user->mobile=$request->mobile;
         $user->type=$request->type;
         if(!empty($request->password)){
         $user->password=Hash::make($request->password);
         }
         $user->save();

         return redirect()->route('admin.accounts.index')->with('success',"Coach Updated Succcessfully");



    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit(User $user)
    {
       if(Auth::user()->type==USER_TYPE_MANAGER){
        $usertype="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $usertype="admin.";
       }
      $hear_about_us=[1=>"TV/Radio/Cinema",2=>"Social Media(Facebook,Twitter,Instagram,etc.)",3=>"Online Search",4=>"Family or Friend",
      5=>"School",6=>"du Email or SMS",7=>"others"];
      return view($usertype.'users.edit',['user'=>$user,'hear_about_us'=>$hear_about_us]);
    }




    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */
    public function update(Request $request, User $user)
    {

       $this->validateUserUpdate($request,$user);
     
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $usertype="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $usertype="admin.";
       }

      
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->parent_1_relationship= $request->parent_1_relationship;
        $user->parent_2_name = $request->parent_2_name;
        $user->parent_2_email = $request->parent_2_email;
        $user->parent_2_mobile = $request->parent_2_mobile;
        $user->nationality = $request->nationality;
        $user->parent_2_relationship = $request->parent_2_relationship;
        $user->password=Hash::make($request->password);
        $user->hear_about_us = $request->hear_about_us;
        $user->save();

        return redirect()->route($usertype.'parents.index')->with('success',"User Added Succcessfully");

      
    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroyCoach(User $user){
        
        
        $user->delete();
        return redirect()->route('admin.accounts.index')->with('success',"Account Deleted Succcessfully");
;

    }


    public function destroy(User $user){
    
    if(Auth::user()->type==USER_TYPE_MANAGER){
        $usertype="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $usertype="admin.";
       }
       
       $user->players()->delete();
        $user->delete();
        return redirect()->route($usertype.'parents.index')->with('success',"Coach Deleted Succcessfully");

    }



    public function coachAssignment()

    {

        

        $users = User::where('type',USER_TYPE_COACH)->with('teams')->get();

        return view('admin.coach_assignments.index',['users'=>$users,'no'=>1]);

        

    }



    public function coachAssignmentCreate()

    {

        $categories= AgeCategory::where('is_active',STATUS_ENABLED)->orderByDesc('created_at')->get();

        $locations =Location::where('is_active',STATUS_ENABLED)->orderByDesc('created_at')->get();

        $users = User::where('is_active',STATUS_ENABLED)->where('type',USER_TYPE_COACH)->get();

        $teams=Team::where('is_active',STATUS_ENABLED)->get();

        return view('admin.coach_assignments.create',['categories'=>$categories,'locations'=>$locations,'users'=>$users,'teams'=>$teams]);

    }



    public function coachAssignmentStore(Request $request)

    {

      $user = User::find($request->user_id);

      $team_id = $request->team_id;

      $location_id=$request->location_id;

      $category_id=$request->category_id;

      $user->teams()->attach($team_id,['location_id'=>$location_id,'category_id'=>$category_id]);

      return redirect()->route('admin.coach.assignments.index'); 

    }



    public function coachAssignmentEdit($userId,$id)

    {

        $user=User::find($userId);

        $data=$user->teams()->wherePivot('id',$id)->first();

        $categories= AgeCategory::where('is_active',STATUS_ENABLED)->orderByDesc('created_at')->get();

        $locations =Location::where('is_active',STATUS_ENABLED)->orderByDesc('created_at')->get();

        $users = User::where('is_active',STATUS_ENABLED)->where('type',USER_TYPE_COACH)->get();

        $teams=Team::where('is_active',STATUS_ENABLED)->get();

        return view('admin.coach_assignments.edit',['categories'=>$categories,'locations'=>$locations,'users'=>$users,'teams'=>$teams,'data'=>$data,'id'=>$id,]);

      

    }



    public function coachAssignmentUpdate(Request $request,$id)

    {

      $user = User::find($request->user_id);

      $team_id = $request->team_id;

      $location_id=$request->location_id;

      $category_id=$request->category_id;

      $user->teams()->wherePivot('id', '=', $id)->update([

        'user_id'=>$user->id,'team_id'=>$team_id,

        'location_id' =>$location_id,'category_id'=>$category_id]);

      return redirect()->route('admin.coach.assignments.index'); 

      

    }



    public function coachAssignmentDelete(Request $request,$userId,$id)

    {

    $user = User::find($userId);

      $team_id = $request->team_id;

      $location_id=$request->location_id;

      $category_id=$request->category_id;

      $user->teams()->wherePivot('id',$id)->update([

        'coach_teams.is_active'=>0]);

      return redirect()->route('admin.coach.assignments.index'); 

      

    }

    public function searchUser(Request $request)
    {

      

       if(Auth::user()->type==USER_TYPE_MANAGER){
        $usertype="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $usertype="admin.";
       }
       
        $keyword= $request->keyword;
        $pr= $request->pr;
        $gender = $request->gender;
        $address=$request->address;
        $email=$request->email;
        $mobile = $request->mobile;
        $nationality = $request->nationality;
        $name = $request->name;

        //DB::enableQueryLog(); // Enable query log
       
        $search_query = User::where('type','!=',USER_TYPE_ADMIN);
        
        if (!empty($keyword)) {

          $search_query = $search_query->where(function ($query) use ($keyword) {
            $query->where('name', 'LIKE', '%'.$keyword.'%')
                ->orWhere('email', 'LIKE', '%'.$keyword.'%')
                ->orWhere('mobile', 'LIKE', '%'.$keyword.'%')
                ->orWhere('nationality', 'LIKE', '%'.$keyword.'%');
            });

            // var_dump($search_query->toSql());
            // exit(0);
            
        }
        
          if (!empty($email)) {
          $search_query = $search_query->where('email',$email);
          }

          if (!empty($pr)) {
          $search_query = $search_query->where('id',$pr);
          }

          if(!empty($mobile)) {
          $search_query = $search_query->where('mobile',$mobile);
          }

          if (!empty($nationality)) {
          $search_query = $search_query->where('nationality',$nationality);
          }

          if (!empty($name)) {
          
          $search_query = $search_query->where('name',$name);

          }
         
        $parents = $search_query->paginate(15);
        
        return view($usertype.'users.index',['parents'=>$parents,
            'keyword'=>$keyword,'name'=>$name,
            'nationality'=>$nationality,'mobile'=>$mobile,'email'=>$email,'pr'=>$pr]);
    }
    
    
    private function validateUser(Request $request) {

        $request->validate([
          'name'  => ['required','max:50','regex:/^[\pL\s\-]+$/u'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
          'password' => ['required', 'string'],
          'mobile' => ['required','digits:10'],
          'parent_1_relationship' =>['required','max:50'],
          'parent_2_name' =>['nullable','max:50'],
          'parent_2_email' =>['nullable','max:50'],
          'parent_2_mobile' =>['nullable','max:50'],
          'parent_2_relationship'=>['nullable','max:50'],
        ]);

    }

     private function validateUserUpdate(Request $request,$user) {

        $request->validate([
          'name'  => ['required','max:50','regex:/^[\pL\s\-]+$/u'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id],
          'mobile' => ['required','digits:10'],
          'parent_1_relationship' =>['required','max:50'],
          'parent_2_name' =>['nullable','max:50'],
          'parent_2_email' =>['nullable','max:50'],
          'parent_2_mobile' =>['nullable','max:50'],
          'parent_2_relationship'=>['nullable','max:50'],
        ]);

    }


}

