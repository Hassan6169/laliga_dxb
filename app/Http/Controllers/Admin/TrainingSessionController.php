<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\TrainingSession;

use App\FeeSession;

use App\Location;

use App\AgeCategory;

use App\Term;
use App\TermBreak;

use App\Day;
use App\Season;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Auth;
use DB;

class TrainingSessionController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

 public function index(Request $request){
    if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
        
        
        $seasons=Season::pluck('name','id');
         $search_query =  Term::where('is_active', '=', STATUS_ENABLED);
        
        if(!empty($request->season_id) && empty($request->term_id)){
         $season_id = $request->season_id;
         $search_terms=Term::where('season_id',$season_id)->pluck('name','id');
         $search_query = $search_query->where('season_id',$season_id);
         $terms = $search_query->paginate(15);
        }
        elseif($request->is_submit==1){
        
        $term_id = $request->term_id;
        $season_id = $request->season_id;
        $search_terms=Term::where('season_id',$season_id)->pluck('name','id');
        if(!empty($term_id)){
          $search_query = $search_query->where('id',$term_id)->where('season_id',$season_id);
        }
        if(!empty($season_id)){
          $search_query = $search_query->where('season_id',$season_id);
        }

        $terms = $search_query->paginate(15);
    
       }
       else{
        $search_terms=[];
        $terms = Term::where('is_show_to_admin',STATUS_ENABLED)->orderByDesc('created_at')
         ->paginate(15);

       }
       
         return view($user_type.'training_session.index',['terms'=>$terms,'seasons'=>$seasons,
         'search_terms'=>$search_terms,'request'=>$request,
         ]);

    }






    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create(Term $term)

    {
        if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }

        $days=Day::all();

        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();

      

        $locations=Location::where('is_active',STATUS_ENABLED)->get();

        return view($user_type.'training_session.create',['days'=>$days,'categories'=>$categories,'locations'=>$locations,'term'=>$term]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request,$term)

    {
        $this->validateTrainingSession($request);
        if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
        $terms=Term::find($term);

        $from_date = Carbon::createFromFormat('Y-m-d H:i:s', $terms->start_date)->format('Y-m-d');
        $to_date=Carbon::createFromFormat('Y-m-d H:i:s', $terms->end_date)->format('Y-m-d');
        $from_date=Carbon::createFromFormat('Y-m-d',$from_date);
        $to_date=Carbon::createFromFormat('Y-m-d',$to_date);

       
        $breaks=TermBreak::where('term_id',$term)->get();
         if(!$breaks->isEmpty()){

        foreach ($breaks as $key => $break) {
        
        $break_start=date('Y-m-d',strtotime($break->start_date));
        $break_end=date('Y-m-d',strtotime($break->end_date)); 
         
        //break dates
         $period = CarbonPeriod::create($break_start, $break_end);
         foreach ($period as $date) {
              $dates[]=$date->format('Y-m-d');
            
            }
        
         }

         //find days betwwen two dates
         for ($date = $from_date; $date <= $to_date; $date->modify('+1 day')) {
          $day_name=$date->format('l');
          $day_id=Day::where('name',$day_name)->pluck('id')->first();
          $training_date=$date->format('Y-m-d');
          
          if(!in_array($training_date, $dates)){
          $days[]=['date'=>$training_date,'day_id'=>$day_id];
        }
      }
  }
     else{
        
        //find days betwwen two dates
         for ($date = $from_date; $date <= $to_date; $date->modify('+1 day')) {
          $day_name=$date->format('l');
          $day_id=Day::where('name',$day_name)->pluck('id')->first();
          $training_date=$date->format('Y-m-d');
         
          $days[]=['date'=>$training_date,'day_id'=>$day_id];
        }



     }
          
        $start_time=TrainingSession::getStartTimeFormattedAttribute($request->start_time);
        $end_time=TrainingSession::getStartTimeFormattedAttribute($request->end_time);

      


         $days_no = $request->days;

         

         $locations=$request->locations;

         $categories=$request->categories;


        foreach ($days_no as $key => $day) {
        
        foreach ($categories as $key => $category) {
            foreach ($locations as $key => $location) {
             
             $data[]=['term_id'=>$term,'location_id' => $location,
             'day_id' => $day,'category_id' => $category,'start_time' => $start_time,
             'end_time' =>$end_time,'date'=>json_encode($days),'for_goal_keeper' => $request->for_goal_keeper,'is_active' => $request->is_active !== null ? 1 : 0 ,'created_at'=>Carbon::now()->timestamp,'updated_at'=>Carbon::now()->timestamp];
         }

        }
        }
         
         
          TrainingSession::insert($data);

          return redirect()->route($user_type.'training.sessions.index',[$term])->with('success', 'Training session Added Successfully');

    }



    /**

     * Display the specified resource.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function editDetails(TrainingSession $session)

    {
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
        $days=Day::all();
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->pluck('name','id');
        $locations=Location::where('is_active',STATUS_ENABLED)->pluck('name','id');
        return view($user_type.'training_session.edit_details',['session'=>$session,'days'=>$days,
            'categories'=>$categories,'locations'=>$locations]);

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Term $term, Request $request)

    {
        if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }

        $days=Day::pluck('name','id');
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->pluck('name','id');
        $locations=Location::where('is_active',STATUS_ENABLED)->pluck('name','id');

        if($request->is_submit==1){
        
        $category_id = $request->category_id;
        $location_id = $request->location_id;
        $day_id = $request->day_id;
        $search_query = TrainingSession::where('is_active', '=', STATUS_ENABLED);
        
        if(!empty($category_id)){
          $search_query = $search_query->where('category_id',$category_id);
        }
        if(!empty($location_id)){
          $search_query = $search_query->where('location_id',$location_id);
        }
        if(!empty($day_id)){
          $search_query = $search_query->where('day_id',$day_id);
        }

        $sessions = $search_query->paginate(15);
    
       }
       else{

        $sessions=TrainingSession::where('term_id',$term->id)->paginate(15);
        }
          return view($user_type.'training_session.edit',['term'=>$term,'days'=>$days,
            'categories'=>$categories,'locations'=>$locations,'sessions'=>$sessions,'term'=>$term,'request'=>$request]);

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request $request

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request)

    {
        $this->validateEditTrainingSession($request);
       if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
        
         $session=TrainingSession::find($request->session);

        $start_time=TrainingSession::getStartTimeFormattedAttribute($request->start_time);
        $end_time=TrainingSession::getStartTimeFormattedAttribute($request->end_time);
        
      
        $session->start_time = $start_time;

        $session->end_time = $end_time;

        $session->location_id = $request->location_id;
        $session->category_id = $request->category_id;

        $session->cap_a = $request->cap_a;
        $session->cap_b = $request->cap_b;
        $session->cap_c = $request->cap_c;

        $session->is_active = $request->is_active !== null ? 1 : 0;

        $session->for_goal_keeper = $request->for_goal_keeper !== null ? 1 : 0;

        $session->save();

        return redirect()->route($user_type.'training.sessions.edit',[$session->term_id])->with('success', 'Traning session Updated Successfully');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function destroy(Term $term,TrainingSession $session)

    {
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }

        if (!empty($session)) {

            $session->delete();
         
         return redirect()->route($user_type.'training.sessions.edit',[$term->id])->with('success', 'Traning session Deleted Successfully');

        }



    }



     private function validateTrainingSession(Request $request) {

       

        $request->validate([

            

            'start_time'  => 'required',

            'end_time'  => 'required|after:start_time',

            'locations'  => 'required',

            'categories' => 'required',

            'days' => 'required',

        ]);

    }

     private function validateEditTrainingSession(Request $request) {

       

        $request->validate([

            'name'  => 'max:100',
            'start_time'  => 'required',
            'end_time'  => 'required|after:start_time',
            'location_id' => 'required',
            'category_id' => 'required',
            

        ]);

    }

}

