<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Location;

use App\Emirate;

use App\Term;

use Auth;





class LocationController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

     $locations = location::orderByDesc('id')->paginate(15);

            if(Auth::user()->type==USER_TYPE_ADMIN){
            // if(Auth::user()->isadmin == 1){
              return view('admin.locations.index',['locations'=>$locations]);
              }
              elseif(Auth::user()->type==USER_TYPE_MANAGER){
            //  elseif(Auth::user()->isadmin == 1){
              return view('manager.locations.index',['locations'=>$locations]);
                 }     

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        $emirates=Emirate::where('is_active', STATUS_ENABLED)->get();
        if(Auth::user()->type==USER_TYPE_MANAGER){
        return view('manager.locations.create',['emirates'=>$emirates]);
         }
         elseif(Auth::user()->type==USER_TYPE_ADMIN){
         return view('admin.locations.create',['emirates'=>$emirates]);
         }
        

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

         $this->validateLocation($request);

        $name = $request->input('name');

        if (Location::where('name','=',$name)->first()) {

        return redirect()->route('admin.locations.index')->with('error', 'Data is Already Present');

        }

        $location = new location();

        $location->name = $name;

        $location->emirate_id = $request->input('emirate_id');

        $location->is_active = $request->input('is_active') !== null ? 1 : 0;

        $location->save();


        if(Auth::user()->type==USER_TYPE_MANAGER){
        return redirect()->route('manager.locations.index')->with('success', 'location Added Successfully');
         }
         elseif(Auth::user()->type==USER_TYPE_ADMIN){
         return redirect()->route('admin.locations.index')->with('success', 'location Added Successfully');
         }
        

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Location $location)

    {

      $emirates=Emirate::where('is_active', STATUS_ENABLED)->get();
       if(Auth::user()->type==USER_TYPE_MANAGER){
        return view('manager.locations.edit',['location'=>$location, 'emirates'=>$emirates]);
         }
        elseif(Auth::user()->type==USER_TYPE_ADMIN){
        return view('admin.locations.edit',['location'=>$location, 'emirates'=>$emirates]);
         }
      

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request $request

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request,Location $location)

    {

        $this->validateLocation($request);

        $name = $request->input('name');

        if (Location::where('name','=',$name)->where('id','!=',$location->id)->first()) {
        
        if(Auth::user()->type==USER_TYPE_MANAGER){
        return redirect()->route('manager.locations.index')->with('error', 'Data is Already Present');
         }
        elseif(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.locations.index')->with('error', 'Data is Already Present');
         }
        

        }

        

        $location->name = $name;

        $location->emirate_id = $request->input('emirate_id');

        $location->is_active = $request->input('is_active') !== null ? 1 : 0;

        $location->save();

        
        if(Auth::user()->type==USER_TYPE_MANAGER){
          return redirect()->route('manager.locations.index')->with('success', 'location Updated Successfully');
         }
        elseif(Auth::user()->type==USER_TYPE_ADMIN){
          return redirect()->route('admin.locations.index')->with('success', 'location Updated Successfully');
         }
        

      

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

  



    public function destroy(Location $location){

       
        $location->delete();
        if(Auth::user()->type==USER_TYPE_MANAGER){
         return redirect()->route('manager.locations.index')->with('success', 'location Deleted Successfully');
         }
        elseif(Auth::user()->type==USER_TYPE_ADMIN){
          return redirect()->route('admin.locations.index')->with('success', 'location Deleted Successfully');
         
      }
    }

    private function validateLocation(Request $request) {

       

        $request->validate([

            'name'  => 'required|max:100',

        ]);

    }

}

