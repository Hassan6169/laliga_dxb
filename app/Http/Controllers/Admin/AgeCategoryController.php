<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\AgeCategory;

use App\TrainingSession;

use App\Player;

use Carbon\Carbon;
use Auth;




class AgeCategoryController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function __construct()

    {

        // $this->middleware('auth');

    }



    public function index()

    {

        $age_categories = AgeCategory::orderByDesc('created_at')

        ->paginate(15);

         if(Auth::user()->type==USER_TYPE_ADMIN){
        // if(Auth::user()->isadmin == 1){
         return view('admin.age_category.index',['age_categories'=>$age_categories]);
         }
         elseif(Auth::user()->type==USER_TYPE_MANAGER){
        // elseif(Auth::user()->isadmin == 1){
            return view('manager.age_category.index',['age_categories'=>$age_categories]);
            }
        

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
         return view($type.'age_category.create');
    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }

        $this->validateCategory($request);

        $category = new AgeCategory();

        $category->name = $request->input('name');

        $category->gender = $request->input('gender');

        $category->start_date =Carbon::parse($request->input('start_date'))->format('Y-m-d');

        $category->end_date = Carbon::parse($request->input('end_date'))->format('Y-m-d');

        if (AgeCategory::where('name', '=', $category->name)->first()) {

        
         return redirect()->route($type.'age.categories.index')->with('error', 'Data is Already Present');
         }

        $category->is_active = $request->input('is_active') !== null ? 1 : 0;

        $category->save();


       
         return redirect()->route($type.'age.categories.index')->with('success', 'Age Categories Added Successfully');
         
        

    }



    /**

     * Display the specified resource.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function edit(AgeCategory $category)

    {

    if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
      return view($type.'age_category.edit',['category'=>$category]);
       

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request $request

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request,AgeCategory $category)

    {
       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }

        $this->validateCategory($request);

        $category->name = $request->input('name');

        $category->gender = $request->input('gender');

        $category->start_date =Carbon::parse($request->input('start_date'))->format('Y-m-d');

        $category->end_date = Carbon::parse($request->input('end_date'))->format('Y-m-d');

        $category->is_active = $request->input('is_active') !== null ? 1 : 0;



        $category->save();

         return redirect()->route($type.'age.categories.index')->with('success', 'Age Category Updated Successfully');
         
        

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

   

    public function destroy(AgeCategory $category)

    {
       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
     
        $session = TrainingSession::where('category_id', $category->id)->where('deleted_at','=',NULL)->first();

             $player = Player::where('category_id',$category->id)->first();

        if (empty($session) || empty($player)) {
             $category->delete();

        return redirect()->route($type.'age.categories.index')->with('success', 'Age Category Deleted Successfully');
         }
        else{

        return redirect()->route($type.'age.categories.index')->with('error', 'This Age Category Is In Used');
         }
      
      }

      private function validateCategory(Request $request) {

       

        $request->validate([

            'name'  =>  ['required','max:100'],
            'start_date' =>  ['required','before_or_equal:end_date'],
            'end_date' =>  ['required','after_or_equal:date_start'],

            

        ]);

    }

}

