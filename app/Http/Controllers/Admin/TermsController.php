<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Term;
use App\TermBreak;
use App\TrainingSession;
use App\AgeCategory;
use App\Location;
use Carbon\Carbon;
use Auth;

class TermsController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index($season)

    {

     $types=['1'=>"Season Term",'2'=>"Camp-with Days Selection",'3'=>"Camp-without Days Selection"];

     $levels=['1'=>"Development",'2'=>"Advanced",'3'=>"HPC"];

      $terms = Term::where('season_id', $season)
      ->orderBy('id','desc')->paginate(15);

       if(Auth::user()->type==USER_TYPE_MANAGER){
        return view('manager.terms.index',['season'=>$season, 'terms'=>$terms, 'no'=>1,
        'types'=>$types,'levels'=>$levels]);
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
         return view('admin.terms.index',['season'=>$season, 'terms'=>$terms,
        'types'=>$types,'levels'=>$levels]);
      }
        

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create($season)

    {

    $locations=Location::all();

    $types=['1'=>"Season Term",'2'=>"Camp-with Days Selection",'3'=>"Camp-without Days Selection"];

    $levels=['1'=>"Development",'2'=>"Advanced",'3'=>"HPC"];
   
    
    if(Auth::user()->type==USER_TYPE_MANAGER){
        return view('manager.terms.create',['season'=>$season,'locations'=>$locations,'types'=>$types,'levels'=>$levels]);
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return view('admin.terms.create',['season'=>$season,'locations'=>$locations,'types'=>$types,'levels'=>$levels]);
      }
    

    }

    public function createTermbreak($season,Term $term)

    {
   
    $term_break=TermBreak::where('term_id',$term->id)->paginate(15);
    if(Auth::user()->type==USER_TYPE_MANAGER){
        return view('manager.terms.break',['term_break'=>$term_break,'term'=>$term,
        'season'=>$season]);
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return view('admin.terms.break',['term_break'=>$term_break,'term'=>$term,
        'season'=>$season]);
      }
    

    }

    public function storeTermbreak(Request $request,$season,$term)

    {

    $request->validate([
            'start_date' =>  ['required','before_or_equal:end_date'],
            'end_date' =>  ['required','after_or_equal:date_start'],
            
        ]);
   
    $start_date=$request->start_date;
    $end_date=$request->end_date;
    
    $term_break= new TermBreak();
    $term_break->term_id=$term;
    $term_break->start_date=date('Y-m-d',strtotime($start_date));
    $term_break->end_date=date('Y-m-d',strtotime($end_date));
    $term_break->save();

    if(Auth::user()->type==USER_TYPE_MANAGER){
        return redirect()->route('manager.season.terms.break',[$season,$term])->with('success', 'Term Break Added'); 
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.season.terms.break',[$season,$term])->with('success', 'Term Break Added'); 
      }
    
      

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $this->validateTerm($request);

        $season_id = $request->input('season_id');

        $name = $request->input('name');

        $check_exist = Term::where('name',$name)->first();

        if(!empty($check_exist->season_id) && $check_exist->season_id==$season_id)

         {
         
         if(Auth::user()->type==USER_TYPE_MANAGER){
        return redirect()->route('manager.season.terms.index', [$season_id])->with('error', 'Term Already Exist'); 
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.season.terms.index', [$season_id])->with('error', 'Term Already Exist');
      }

             

        }

        if (!empty($request->start_date)) {

        $startDate=date('Y-m-d',strtotime($request->start_date));  

        }else{

           $startDate=NULL; 

        }

        if (!empty($request->end_date)) {

        $endDate=date('Y-m-d',strtotime($request->end_date)); 

        }else{

           $endDate=NULL; 

        }
        
        $level=$request->level;
        

       

        $terms = new Term();

        $terms->name = $name;

        $terms->start_date = $startDate;

        $terms->end_date = $endDate;
        
        $terms->type_id = $request->type_id;

        $terms->level = $level;

        $terms->season_id = $season_id;

        $terms->kit_amount = $request->kit_amount; 
        $terms->total_sessions = $request->total_sessions;
        $terms->weeks = $request->weeks; 

        $terms->is_active = $request->input('is_active') !== null ? 1 : 0;

        $terms->yearly = $request->input('yearly') !== null ? 1 : 0;

        $terms->is_show_to_admin = $request->is_show_to_admin !== null ? 1 : 0;

        $terms->save();


        if(Auth::user()->type==USER_TYPE_MANAGER){
       return redirect()->route('manager.season.terms.index', [$season_id])->with('success', 'Terms Added Successfully');
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.season.terms.index', [$season_id])->with('success', 'Terms Added Successfully');
      }

        

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Request $request, $season, Term $term)

    {

        $types=['1'=>"Season Term",'2'=>"Camp-with Days Selection",'3'=>"Camp-without Days Selection"];

        $levels=['1'=>"Development",'2'=>"Advanced",'3'=>"HPC"];
        $locations=Location::withoutGlobalScope('active')->orderByDesc('created_at')->get();
        
        
       if(Auth::user()->type==USER_TYPE_MANAGER){
       return view('manager.terms.edit',['season'=>$season,'locations'=>$locations,'types'=>$types,'term'=> $term,'levels'=>$levels]);
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return view('admin.terms.edit',['season'=>$season,'locations'=>$locations,'types'=>$types,'term'=> $term,'levels'=>$levels]);
      }
        

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request $request

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Term $term)

    {
    
        $this->validateTerm($request);

        $season_id = $request->input('season_id');

        $name = $request->input('name');
        $check_exist = Term::where('name',$name)->where('id','!=',$term->id)->first();
    
        if(!empty($check_exist->season_id) && $check_exist->season_id==$season_id)

         {

        if(Auth::user()->type==USER_TYPE_MANAGER){
        return redirect()->route('manager.season.terms.index', [$season_id])->with('error', 'Term Already Exist'); 
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.season.terms.index', [$season_id])->with('error', 'Term Already Exist'); 
      }

            

        }

        if (!empty($request->start_date)) {

        $startDate=date('Y-m-d',strtotime($request->start_date));  

        }else{

           $startDate=NULL; 

        }

        

        if (!empty($request->end_date)) {

        $endDate=date('Y-m-d',strtotime($request->end_date)); 

        }else{

           $endDate=NULL; 

        }
        
        $level=$request->level;

        $term->name = $name;

        $term->start_date = $startDate;

        $term->end_date = $endDate;

        $term->type_id = $request->type_id;

        $term->level = $level;
        $term->weeks = $request->weeks; 

    

        $term->kit_amount = $request->kit_amount; 
        $term->total_sessions = $request->total_sessions; 

        $term->yearly = $request->yearly !== null ? 1 : 0;

        $term->is_active = $request->is_active !== null ? 1 : 0;

        $term->is_show_to_admin = $request->is_show_to_admin !== null ? 1 : 0;

        $term->save();
        
        if(Auth::user()->type==USER_TYPE_MANAGER){
       return redirect()->route('manager.season.terms.index', [$season_id])->with('success', 'Terms Updated Successfully'); 
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
       return redirect()->route('admin.season.terms.index', [$season_id])->with('success', 'Terms Updated Successfully');
      }
        

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($season,Term $term)

    {
       
        if (!empty($term)) {
            
            $term->trainingSession()->delete();
            $term->delete();

        if(Auth::user()->type==USER_TYPE_MANAGER){
        return redirect()->route('manager.season.terms.index', [$season])->with('success', 'Term Deleted Successfully');
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.season.terms.index', [$season])->with('success', 'Term Deleted Successfully');
      }

           

        }

    }


     public function destroyTermBreak($season,Term $term,TermBreak $break){
        
        $break->delete();
        if(Auth::user()->type==USER_TYPE_MANAGER){
        return redirect()->route('manager.season.terms.break',[$season,$term])->with('success', 'Term Break Deleted');  
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.season.terms.break',[$season,$term])->with('success', 'Term Break Deleted');  
      }
         
           

    }

   


    private function validateTerm(Request $request) {

       

        $request->validate([

            'name'  =>  ['required','max:100'],
            'start_date' =>  ['required','before_or_equal:end_date'],
            'end_date' =>  ['required','after_or_equal:date_start'],
            'kit_amount' =>  ['required','regex:/^\d+(\.\d{1,2})?$/'],
            'type_id' =>  ['required'],
            'weeks' => ['required','max:4'],
            'total_sessions' => ['required','max:4'],

            

        ]);

    }

    

   



}

