<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryCategory;
use App\Emirate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries=Gallery::all();
        $emirates=Emirate::where('is_active', STATUS_ENABLED)->get();
        $sessions=['' => 'Select Session',1=>'2020/2021',2=>'2019/2020',3=>'2018/2019',4=>'2018/2019'];
        return view('admin.galleries.index',['galleries'=>$galleries,
            'sessions'=>$sessions,'no'=>1,'emirates'=>$emirates]);
    }

    public function indexGalleryCategories()
    {
       
        $galleries=GalleryCategory::all();
        $sessions=['' => 'Select Session',1=>'2020/2021',2=>'2019/2020',3=>'2018/2019',4=>'2018/2019'];
        return view('admin.gallery_categories.index',['galleries'=>$galleries,'sessions'=>$sessions,'no'=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type=['Select Type',1=>'image',2=>'video'];
        $emirates=Emirate::where('is_active', STATUS_ENABLED)->pluck('name','id');
        $gallery_categories=GalleryCategory::where('is_active', STATUS_ENABLED)->pluck('name_eng','id');
         return view('admin.galleries.create',['gallery_categories'=>$gallery_categories,'emirates'=>$emirates,'type'=>$type]);
    }

    public function createGalleryCategories()
    {
        $sessions=['' => 'Select Session',1=>'2020/2021',2=>'2019/2020',3=>'2018/2019',4=>'2018/2019'];
        return view('admin.gallery_categories.create',['sessions'=>$sessions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateGallery($request);
        $gallery = new Gallery();
        $gallery->name_eng = $request->name_eng;
        $gallery->name_ar = $request->name_ar;
        $gallery->type= $request->type;
        $gallery->video_url= $request->video_url;
        $gallery->emirate_id = $request->emirate_id;
        $gallery->gallery_category_id = $request->gallery_category_id;
        $image= $request->file('image');
        if(!empty($image)){
            $extension  = $image->getClientOriginalExtension();
            $gallery_image= $request->name_eng . mt_rand(11, 99) . mt_rand(11, 99). '.' . $extension;
            $file_path = $image->storeAs('public/galleries/',$gallery_image);
            $gallery->image = $gallery_image;
        }
        $gallery->is_active = $request->input('is_active') !== null ? 1 : 0;;
        $gallery->save();

        return redirect()->route('admin.galleries.index')->with('success', 'News Added Successfully');
    }

     public function storeGalleryCategories(Request $request)
    {
        $this->validateGalleryCategories($request);
        $gallery_category = new GalleryCategory();
        $gallery_category->name_eng = $request->name_eng;
        $gallery_category->name_ar = $request->name_ar;
        $gallery_category->session_id = $request->session_id;
        $gallery_category->is_active = $request->input('is_active') !== null ? 1 : 0;;
        $gallery_category->save();

        return redirect()->route('admin.gallery.categories.index')->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
       $type=['' => 'Select Type',1=>'image',2=>'video'];
       $emirates=Emirate::where('is_active', STATUS_ENABLED)->pluck('name','id');
        $gallery_categories=GalleryCategory::where('is_active', STATUS_ENABLED)->pluck('name_eng','id');
         return view('admin.galleries.edit',['gallery_categories'=>$gallery_categories,'emirates'=>$emirates,'gallery'=>$gallery,'type'=>$type]);
    }

    public function editGalleryCategories(GalleryCategory $gallery_category)
    {

       $sessions=['' => 'Select Session',1=>'2020/2021',2=>'2019/2020',3=>'2018/2019',4=>'2018/2019'];
        return view('admin.gallery_categories.edit',['sessions'=>$sessions,'gallery_category'=>$gallery_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        $this->validateGallery($request);
        $gallery->name_eng = $request->name_eng;
        $gallery->name_ar = $request->name_ar;
        $gallery->emirate_id = $request->emirate_id;
        $gallery->type= $request->type;
        $gallery->video_url= $request->video_url;
        $gallery->gallery_category_id = $request->gallery_category_id;
        $image= $request->file('image');
        if(!empty($image)){
            $extension  = $image->getClientOriginalExtension();
            $gallery_image= $request->name_eng . mt_rand(11, 99) . mt_rand(11, 99). '.' . $extension;
            $file_path = $image->storeAs('public/galleries/',$gallery_image);
            $gallery->image = $gallery_image;
        }
        $gallery->is_active = $request->input('is_active') !== null ? 1 : 0;;
        $gallery->save();

        return redirect()->route('admin.galleries.index')->with('success', 'News Added Successfully');
    }

     public function updateGalleryCategories(Request $request,GalleryCategory 
        $gallery_category)
    {
        $this->validateGalleryCategories($request);
        $gallery_category->name_eng = $request->name_eng;
        $gallery_category->name_ar = $request->name_ar;
        $gallery_category->session_id = $request->session_id;
        $gallery_category->is_active = $request->input('is_active') !== null ? 1 : 0;;
        $gallery_category->save();

        return redirect()->route('admin.gallery.categories.index')->with('success', 'Added Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
       $gallery->delete();
        return redirect()->route('admin.galleries.index')->with('success', 
            'Deleted Successfully');
    }

     public function destroyGalleryCategories(GalleryCategory $gallery_category)
    {
        $gallery_category->delete();
        return redirect()->route('admin.gallery.categories.index')->with('success', 
            'Deleted Successfully');
    }

     private function validateGalleryCategories(Request $request) {
       
        $request->validate([
             'name_ar'  => 'required|max:100',
             'name_eng'  => 'max:100',
        ]);
    }

    private function validateGallery(Request $request) {
       
        $request->validate([
             'name_ar'  => 'required|max:100',
             'name_eng'  => 'max:100',
             'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    }
}
