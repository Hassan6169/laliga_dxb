<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Player;
use App\Location;
use App\AgeCategory;
use App\Evaluation;
use App\Term;
use App\User;
use App\Team;
use Auth;
use PDF;
use Mail;

class EvaluationController extends Controller
{
    protected $categories = ['technical_evaluation' => ['label' => "Technical Evaluation",
        'sub_categories' => [
            "ball_skills" => ['label' => "Ball Skills", 'score' => 0],
            "pass_control" => ['label' => "Pass Control", 'score' => 0],
            "carrying_the_ball" => ['label' => "Carrying The Ball", 'score' => 0],
            "shooting" => ['label' => "Shooting", 'score' => 0],
            "defensive_skills" => ['label' => "Defensive Skills", 'score' => 0],
            "right" => ['label' => "Right", 'score' => 0],
            "lefts" => ['label' => "Lefts", 'score' => 0],

        ]],
        'tactical_evaluation' => ['label' => "Tactical Evaluation", 'sub_categories' => [
            "decision_making" => ['label' => "Decision Making", 'score' => 0],
            "spatial_awareness" => ['label' => "Spatial Awareness (‘Where I have to be’)", 'score' => 0],
            "timing_positional_play" => ['label' => "Timing - Positional & Play (‘When I have to be there’)", 'score' => 0],
            "creativity_adaptation" => ['label' => "Creativity/Adaptation", 'score' => 0]
        ]],
        'physical_and_psychomotor_evaluation' => ['label' => "Physical And Psychomotor Evaluation", 'sub_categories' => [
            "strength" => ['label' => "Strength", 'score' => 0],
            "speed" => ['label' => "Speed", 'score' => 0],
            "endurance" => ['label' => "Endurance", 'score' => 0],
            "game_application_of_physical_tools" => ['label' => "Game application of physical tools", 'score' => 0],
            "coordination" => ['label' => "Coordination", 'score' => 0],
            "laterality" => ['label' => "Laterality", 'score' => 0],
        ]],
        'psychological_evaluation' => ['label' => "Psychological Evaluation", 'sub_categories' => [
            "concentration" => ['label' => "Concentration", 'score' => 0],
            "competitiveness" => ['label' => "Competitiveness", 'score' => 0],
            "self_confidence" => ['label' => "Self Confidence", 'score' => 0],
            "sacrifice_and_overcoming" => ['label' => "Sacrifice & Overcoming", 'score' => 0],
            "emotional_control_and_management " => ['label' => "Emotional Control & Management ", 'score' => 0],

        ]],
        'attitude_evaluation' => ['label' => "Attitude Evaluation", 'sub_categories' => [
            "solidarity_comradeship" => ['label' => "Solidarity/Comradeship", 'score' => 0],
            "discipline" => ['label' => "Discipline", 'score' => 0],
            "manners_and_respect" => ['label' => "Manners And Respect", 'score' => 0],
            "relationship_with_coach_and_group" => ['label' => "Relationship with Coach And Group", 'score' => 0],
            "understanding_of_Individual_and_team_roles " => ['label' => "Understanding Of Individual & Team Roles", 'score' => 0],
        ]]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Player $player)
    {
    return view('admin.evaluations.index',['player'=>$player,'no'=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Player $player)
    {

        $locations = Location::where('is_active',STATUS_ENABLED)->pluck('name', 'id');
        $age_categories = AgeCategory::where('is_active',STATUS_ENABLED)->pluck('name', 'id');
        $categories = $this->categories;
        $coaches=User::where('type',USER_TYPE_COACH)
        ->where('is_active',STATUS_ENABLED)->get();
        $temp = Term::withTrashed()->get();
        $terms = Term::withTrashed()->pluck('name', 'id');

        $team=Team::find($player->team->id);
        foreach($team->users as $user){
        $coach_id= $user->id;
        }
        
        return view('admin.evaluations.evaluation_details',['player'=>$player, 'locations'=>$locations, 'categories'=>$categories, 'age_categories'=>$age_categories, 'terms'=>$terms,'coaches'=>$coaches,'coach_id'=>$coach_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $player, $evaluation)
    {
        
        $player_id = $player;
        $location_id = $request->location_id;
        $term_id = $request->term_id;
        $category_id = $request->category_id;
        $coach_id = $request->coach_id;
        $position_1 = $request->position_1;
        $position_2 = $request->position_2;
        $observation = $request->observation;
        $is_final = $request->is_final !== null ? 1 : 0;

        $scores = $request->result;

        $categories = $this->categories;
        if(!empty($scores)) {
            foreach ($scores as $cat => $sub_categories) {
                foreach ($sub_categories as $sub_cat => $score) {
//                dump($cat." ".$sub_cat." ".$score);
                    if (isset($categories[$cat]['sub_categories'][$sub_cat]['score'])) {
                        $categories[$cat]['sub_categories'][$sub_cat]['score'] = $score;
                    }
                }
            }
        }
        $evaluation = Evaluation::where('id', $evaluation)->first();
        if (!$evaluation) {
            $evaluation = new Evaluation();
        }
        $evaluation->player_id = $player_id;
        $evaluation->location_id = $location_id;
        $evaluation->term_id = $term_id;
        $evaluation->category_id = $category_id;
        $evaluation->coach_id = $coach_id;
        $evaluation->evaluation_details = json_encode($categories);
        $evaluation->position_1 = $position_1;
        $evaluation->position_2 = $position_2;
        $evaluation->observation = $observation;
        $evaluation->is_final = $is_final;
        $evaluation->save();

        if (Auth::user()->type == USER_TYPE_ADMIN) {
            $user_type = 'admin';
        } else {
            $user_type = 'coach';
        }

        if($is_final==1){

            $player = Player::where('id',$player_id)->first();

            $data = array(
                'email' => $player->user->email,
                'subject' => 'Laliga Academy Canada Evaluation Report',
                'player' => $player,
                'evaluation' => $evaluation
            );

            $pdf_path = $this->generateEvaluationPdfReport($player->id, $evaluation);

            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                return redirect()->route($user_type . '.player.evaluations', [$player_id])->with('error', 'Email address is not valid to send evaluation');
            }

            Mail::send(['view' => 'emails.evaluation_report'], $data, function ($message) use ($pdf_path, $data) {
                $message->to($data['email'])
                    ->subject($data['subject'])
                    ->attach($pdf_path, [
                        'as' => 'LA-Evaluation'.$data['player']->id.'_'.$data['evaluation']->id.'.pdf',
                        'mime' => 'application/pdf',
                    ]);
            });

        }

        return redirect()->route($user_type . '.player.evaluations.index', [$player_id])->with('success', 'Player Evaluation is Submitted Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generateEvaluationPdfReport($player, Evaluation $evaluation)
    {

         $pdf = PDF::loadView('admin.evaluations.evaluation_details',$evaluation);
        // if (Auth::user()->type == USER_TYPE_ADMIN) {
        //     $user_type = 'admin';
        // } else {
        //     $user_type = 'coach';
        // }
        // $player = Player::find($player);

        //  $wkhtmltopdf = public_path().'/generatepdf';
        //  $fileDestination = storage_path() . '/app/public/evaluation_report/';
        
        // $fileName = 'evaluation_' . $player->id . '_'.$evaluation->id.'.pdf';

        // $evaluationURL = route($user_type.'.player.evaluations.report', [$player->id, $evaluation->id]);
        // $cmd = "xvfb-run -- " . $wkhtmltopdf . " --page-size A2 --lowquality -L 15mm -R 15mm -T 20mm -B 20mm '" . $evaluationURL . "' '" . $fileDestination . $fileName . "'";

        // if (exec($cmd)){
        //     echo $fileDestination.$fileName;
        //     die();
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Player $player, Evaluation $evaluation)
    {
       $locations = Location::where('is_active',STATUS_ENABLED)->pluck('name', 'id');
        $age_categories = AgeCategory::where('is_active',STATUS_ENABLED)->pluck('name', 'id');
        $categories = $this->categories;

        $coaches=User::where('type',USER_TYPE_COACH)
        ->where('is_active',STATUS_ENABLED)->get();
        $temp = Term::withTrashed()->get();
        $terms = Term::withTrashed()->pluck('name', 'id');

        $team=Team::find($player->team->id);
        foreach($team->users as $user){
        $coach_id= $user->id;
        }

        if (!empty($evaluation)) {
            $record = $evaluation->evaluation_details;
            $categories = json_decode($record, true);
        }
    
        return view('admin.evaluations.evaluation_details',['player'=>$player, 'locations'=>$locations, 'categories'=>$categories, 'age_categories'=>$age_categories, 'terms'=>$terms,'coaches'=>$coaches,'coach_id'=>$coach_id,'evaluation'=>$evaluation,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getEvaluationReport(Request $request, $player, Evaluation $evaluation)
    {
        $locations = array_merge(['' => 'Select Location'], Location::withTrashed()->pluck('name', 'name')->toArray());
        $age_categories = array_merge(['' => 'Select Age'], AgesCategory::withTrashed()->pluck('name', 'name')->toArray());
        $categories = $this->categories;

        $temp = Term::withTrashed()->get();
        $terms = ['' => 'Select Term'];
        foreach($temp as $term){
            $str = $term->season->name . ' - ' . $term->name;
            $terms[$str] = $str;
        }

        $player = Player::find($player);

        if (!empty($evaluation)) {
            $record = $evaluation->evaluation_details;
            $categories = json_decode($record, true);
        }
        return view('admin.evaluation.evaluation_report', compact('evaluation', 'player','locations', 'categories', 'age_categories', 'terms'));
    }

    public function sendPlayerEvaluation(Request $request, $player, Evaluation $evaluation)
    {
        $player = Player::where('id',$player)->first();
        $data = array(
            'email' => $player->user->email,
            'subject' => 'Laliga Academy UAE Evaluation Report',
            'player' => $player,
            'evaluation' => $evaluation
        );

        if (Auth::user()->user_type == USER_TYPE_ADMIN) {
            $user_type = 'admin';
        } else {
            $user_type = 'coach';
        }

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return redirect()->route($user_type . '.player.evaluations', [$player->id])->with('error', 'Email address is not valid to send evaluation');
        }

        $pdf_path = url('/storage/evaluation_report/evaluation_'.$player->id.'_'.$evaluation->id.'.pdf');
        Mail::send(['view' => 'emails.evaluation_report'], $data, function ($message) use ($pdf_path, $data) {
            $message->to($data['email'])
                ->subject($data['subject'])
                ->attach($pdf_path, [
                    'as' => 'LA-Evaluation'.$data['player']->id.'_'.$data['evaluation']->id.'.pdf',
                    'mime' => 'application/pdf',
                ]);
        });

        return redirect()->route($user_type . '.player.evaluations', [$player->id])->with('success', 'Player Evaluation Email Sent Successfully');
    }


     
}
