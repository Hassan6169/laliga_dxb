<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Player;
use App\Team;
use App\Day;
use App\Term;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TrainingSession;
use Carbon\Carbon;
use Auth;
class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->type==USER_TYPE_ADMIN){
        // if(Auth::user()->isadmin == 1){
                $type="admin.";
               }
       elseif(Auth::user()->type==USER_TYPE_COACH){
    //    elseif(Auth::user()->isadmin == 1){
        $type="coach.";
       }
       $teams=Team::where('is_active',STATUS_ENABLED)->get();
       $players=[];
       return view($type.'attendance.index',['teams'=>$teams,'players'=>$players]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show($team)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance){
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        //
    }

    public function search(Request $request){
     
      if(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
       elseif(Auth::user()->type==USER_TYPE_COACH){
        $type="coach.";
       }
     $teams=Team::where('is_active',STATUS_ENABLED)->get();
     $team_id=$request->team_id;
     $player_ids=[];
     $present=[];
     $date= Carbon::createFromFormat('m/d/Y', $request->date)->format('Y-m-d');
        
        $attendance=Attendance::where('team_id',$team_id)->where('date',$date)->get();
        if(count($attendance) != 0){
         foreach ($attendance as $key => $attend) {
         	$playerids[]=$attend->player_id;
         }
         $search_query = Player::where('is_active', '=', STATUS_ENABLED)->whereNotIn('id',$playerids);
        }
        else{
        	 $search_query = Player::where('is_active', '=', STATUS_ENABLED);
        }
      
        $search_query->whereHas('terms',function($search_query) use ($team_id) {
            $search_query->where('player_term.team_id',$team_id);
        });
        $players=$search_query->get();


     if($request->submit==1){
      return view($type.'attendance.index',['players'=>$players,'teams'=>$teams,'request'=>$request]);
    }
    elseif($request->submit==2){
      
      $is_present=$request->is_present;
            
          foreach ($players as $key => $value) {
                if(in_array($value->id, $is_present))
                {
                   $player_ids[]=$value->id;
                   $present[]=1;
                }
            }
           
            $coach_comments=$request->coach_comments;
           
            $results = array_map(function($player_ids,$present,$coach_comments) {
                 
                return array(
                'player_ids'=>$player_ids,
                'present'=>$present,
                'coach_comments'=> $coach_comments, 
                  
                );
        
             }, 
           
             $player_ids,$present,$coach_comments);
         
              foreach ($results as $result) {
              	if(!empty($result['player_ids'])){
                   
                if(empty($result['coach_comments'])){
                    $result['coach_comments']="";

                }
               
                $attendance= new attendance();
                $attendance->player_id=$result['player_ids'];
                $attendance->team_id=$request->team_id;
                $attendance->date=$date ;
                $attendance->is_present =$result['present'];
                $attendance->coach_comments =$result['coach_comments'];
                $attendance->save();
           }
       }
            return redirect()->route($type.'attendance.index')->with('success',"Data inserted Successfully");
        }
        elseif($request->submit==3){
        $attendance=Attendance::where('team_id',$team_id)->where('date',$date)->get();

        if(empty($attendance)){
            
            return redirect()->route($type.'attendance.index')->with('Error'," No Data Found");

        }else{

            return view($type.'attendance.show',['attendance'=>$attendance]);
           
        }
      
       }
    }
}