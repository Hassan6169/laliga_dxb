<?php



namespace App\Http\Controllers;



use App\Discount;

use App\Player;

use App\Term;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;


class DiscountController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
        if(Auth::user()->type==USER_TYPE_ADMIN){
        // if(Auth::user()->isadmin == 1){
                $user_type="admin.";
               }
        elseif(Auth::user()->type==USER_TYPE_MANAGER){
        // elseif(Auth::user()->isadmin == 1){
            $user_type="manager.";
           }
        $discounts = Discount::orderByDesc('created_at')->paginate(15);
        $categories=[1=>'Over All Discount',2=>'Sibling Discount',3=>'Term Discount'];
        $types=[1=>'Percentage',2=>'Less',3=>'Additional'];
        return view($user_type.'discounts.index',['discounts'=>$discounts,'categories'=>$categories,'types'=>$types]);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
       $categories=[1=>'Over All Discount',2=>'Sibling Discount',3=>'Term Discount'];
       return view($user_type.'discounts.create',['categories'=>$categories]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
        if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }

        $this->validateDiscount($request);

       if (Discount::where('name', '=', $request->name)->first()) {

            return redirect()->route('admin.discounts.index')->with('error', 'Data is Already Present');

        }
         $discount = new Discount();

        $discount->name = $request->name;
        $discount->amount = $request->amount;
        $discount->type = $request->type;
        $discount->category = $request->category;


        $discount->is_active = $request->input('is_active') !== null ? 1 : 0;;

        $discount->save();



        return redirect()->route($user_type.'discounts.index')->with('success', 'discount Categories Added Successfully');

    }



    /**

     * Display the specified resource.

     *

     * @param  \App\Discount  $discount

     * @return \Illuminate\Http\Response

     */

    public function show(Discount $discount)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Discount  $discount

     * @return \Illuminate\Http\Response

     */

    public function edit(Discount $discount)

    {

     if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
        $categories=[1=>'Over All Discount',2=>'Sibling Discount',3=>'Term Discount'];
         return view($user_type.'discounts.edit', ['discount'=>$discount,'categories'=>$categories]);

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Discount  $discount

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Discount $discount)

    {

       if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }

        $this->validateDiscount($request);

        if (Discount::where('name', '=',  $request->name)->where('id','!=',$discount->id)->first()) {

            return redirect()->route('admin.discounts.index')->with('error', 'Data is Already Present');

        }
        $discount->name = $request->name;
        $discount->amount = $request->amount;
        $discount->type = $request->type;
        $discount->category = $request->category;

        $discount->is_active = $request->input('is_active') !== null ? 1 : 0;;

        $discount->save();



        return redirect()->route($user_type.'discounts.index')->with('success', 'discount Categories Updated Successfully');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Discount  $discount

     * @return \Illuminate\Http\Response

     */

    public function destroy(Discount $discount)

    {
    if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }

        $discount->delete();

        return redirect()->route($user_type.'discounts.index')->with('success', 'Discount Deleted Successfully');

    }



    private function validateDiscount(Request $request) {

       

        $request->validate([

            'name'  => 'required|max:100',
            'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'type'  => 'required',
            'category'  => 'required',

            

        ]);

    }



    public function playerDiscount(Player $player,Term $term)

    {

    if(Auth::user()->type==USER_TYPE_MANAGER){
        $user_type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $user_type="admin.";
       }
      elseif(Auth::user()->type==USER_TYPE_PARENT){
        $user_type="account.";
       }

        $player_discounts = $player->discounts()->wherePivot('term_id',$term->id)->get();

        $discounts = Discount::orderByDesc('created_at')->where('is_active',STATUS_ENABLED)->where('category',STATUS_TERM_INVOICE)->get();

        return view($user_type.'discounts.discount_player',['discounts'=>$discounts,'player'=>$player,'term'=>$term,'player_discounts'=>$player_discounts]);

    }



    public function playerDiscountUpdate(Request $request)

    {

       $player_id = \Request::segment(4);

       $player=Player::find($player_id);
      
       $term_id = \Request::segment(6);
       
        if($request->submit=="update")

          {

        $discount_ids = $request->discounts;
        $player_discounts = $player->discounts()->wherePivot('term_id',$term_id)->get();
        if(count($player_discounts) > 0){
           $player->discounts()->wherePivot('term_id',$term_id)->detach();  
        }
       

       foreach ($discount_ids as $key => $id) {

        $player->discounts()->attach($id,['term_id'=>$term_id]);

       }



     }

     elseif($request->submit=="delete")

     {

      $player->discounts()->detach();

     }



       return redirect()->route('account.invoices.request')->with('success', 'Discount Updated Successfully');

        

    }

}

