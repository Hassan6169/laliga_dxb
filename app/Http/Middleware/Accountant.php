<?php

namespace App\Http\Middleware;

use Closure;

class Accountant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (Auth::check() && Auth::user()->type==USER_TYPE_ACCOUNTANT){

            return $next($request);

        }

        return $next($request);
    }
}
