<?php



namespace App;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Auth;



class User extends Authenticatable

{
    use SoftDeletes;
    use Notifiable;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'name', 'email','isadmin','password', 'gender', 'parent_2_email', 'mobile', 'parent_1_relationship', 'nationality', 'parent_2_mobile', 'hear_about_us'

    ];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password', 'remember_token',

    ];



    /**

     * The attributes that should be cast to native types.

     *

     * @var array

     */

    protected $casts = [

        'email_verified_at' => 'datetime',

    ];



    public static function getAuthenticatedUser()

    {

        if(Auth::check()) { // Check is User LoggedIn

            if (Auth::user()->type == USER_TYPE_ADMIN) { // Check is User an Admin

                if(!empty(session('parent_id'))){

                    // Return Parent User Object

                    return User::find(session('parent_id'))->first();     

                } else {

                    // Return Admin User Object

                    return Auth::user();

                }

            } else {

                // Return User Object

                return Auth::user();



            }

        } else {

            // Return false if User not LoggedIn

            return false;

        }

    }

    public function players()

    {

        return $this->hasMany(Player::class, 'user_id', 'id');

    }



    public function wallet()

    {

        return $this->hasOne('App\Wallet');

    } 



    public function teams()

    {

        return $this->belongsToMany('App\Team','coach_teams')->withTimestamps()->withPivot(['location_id','category_id','id','is_active']);

    }



    public function getCategory($id)

    {

        return AgeCategory::where('id',$id)->pluck('name')->first(); 

    }



    public function getLocation($id)

    {

      return Location::where('id',$id)->pluck('name')->first(); 

    }

}

