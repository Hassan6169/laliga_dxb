<?php
namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;



class Location extends Model
{

    use SoftDeletes;

   public function terms()

    {

        return $this->belongsToMany('App\Term')->withTimestamps();

    }



   public function emirate()

    {

        return $this->belongsTo('App\Emirate');

    }



    public function Fees()

    {

        return $this->hasMany('App\Fee');

    }

}

