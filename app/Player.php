<?php



namespace App;



use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

use Collective\Html\Eloquent\FormAccessible;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Emirate;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;



class Player extends Model implements Auditable

{

    use SoftDeletes;

    use FormAccessible;

    use AuditableTrait;

    public function category()

    {

        if(!empty($this->admin_category_id)){

             return $this->belongsTo('App\AgeCategory','admin_category_id');

        } else if(!empty($this->category_id)) {

            return $this->belongsTo('App\AgeCategory');

        }

        return false;

    }



    public function orginalCategory()

    {

       return $this->belongsTo('App\AgeCategory','category_id');

    

    }



    public function changeCategory()

    {

       return $this->belongsTo('App\AgeCategory','admin_category_id');

      



    }

    

    public function getDobAttribute($value)

    {

        if(!empty($value)){

            return Carbon::parse($value)->format('d/m/Y');

        }

        return '-';

    }



    public function formDobAttribute($value)

    {

        return Carbon::parse($value)->format('Y-m-d');

    }



    public function terms()

    {

    return $this->belongsToMany('App\Term')->withTimestamps()->withPivot(['emirate_id','is_kit_amount_paid','kit_size','is_active','kit_status','is_sponsored','match_kit','payment_status','id','term_id','team_id','deleted_at','serial_no','fee_amount']);

    }

   

   public function getEmirate($id)

    {

       return Emirate::where('id',$id)->pluck('name')->first();   

    }



    public function trainingSessions()

    {

        return $this->belongsToMany('App\TrainingSession')->withTimestamps()->withPivot(['location_id','day_id','serial_no','weeks','is_full_week']);

    }



    public function user()

    {

        return $this->belongsTo('App\User','user_id');

    }

    public function admin(){
      return $this->belongsTo('App\User','admin_id');
    }

 

     public function discounts()

    {

        return $this->belongsToMany('App\Discount','player_discount')->withTimestamps()->withPivot(['term_id']);

    }



    public function evaluations()

    {

        return $this->hasMany('App\Evaluation', 'player_id');

    }

    public function invoices(){

        return $this->belongsTo('App\Invoice','player_id');

    }



    public function team(){

        return $this->belongsTo('App\Team');

    }
}

