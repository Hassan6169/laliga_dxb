<?php



namespace App;



use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;



class Term extends Model

{

  use SoftDeletes;

  protected $dates = ['deleted_at','start_date', 'end_date'];

  public function locations()

    {

        return $this->belongsToMany('App\Location')->withTimestamps();

    }



  public function getStartDateFormattedAttribute()

    {

        return Carbon::parse($this->start_date)->format('d/m/Y');

    }



    public function getEndDateFormattedAttribute()

    {

        return Carbon::parse($this->end_date)->format('d/m/Y');

    } 



    public function trainingSession()

    {

        return $this->hasMany('App\TrainingSession');

    } 



    public function players()

    {

        return $this->belongsToMany('App\Player')->withTimestamps()->withPivot(['emirate_id','is_kit_amount_paid','kit_size','is_active','kit_status','is_sponsored','match_kit','payment_status','id','term_id','team_id','serial_no','fee_amount']);

    }



    public function getEmirate($id)

    {

        $emirate=Emirate::find($id);

        return $emirate->name;

    }



    public function season()

    {

       return $this->belongsTo('App\Season');

    

    }





}

