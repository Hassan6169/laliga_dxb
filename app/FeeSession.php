<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeSession extends Model
{
   
    public function trainingSession()
    {
        return $this->belongsTo('App\TrainingSession');
    }


    public function category()
    {
        return $this->belongsTo('App\AgeCategor');
    } 


    public function location()
    {
        return $this->belongsTo('App\Location');
    }  
}
